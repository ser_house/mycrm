<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.07.2020
 * Time: 10:19
 */

use App\Model\Client\Client;
use App\Model\Task\Task;
use App\Model\User;
use Application\Task\State;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskSeeder extends Seeder {
  public function run() {
    $user = User::first();
    $client = Client::first();

    DB::table('task')->insert([
      'user_id' => $user->id,
      'title' => 'Task 1',
      'description' => '',
      'client_id' => $client->id,
      'currency' => 'RUB',
      'launched_at' => date('Y-m-d'),
      'state' => State::New->value,
    ]);

    $task = Task::where('title', '=', 'Task 1')->first();

    DB::table('sub_task')->insert([
      'title' => 'Основная',
      'task_id' => $task->id,
      'budget' => '3000',
      'hours' => '',
    ]);

    DB::table('task_state_journal')->insert([
      'task_id' => $task->id,
      'state_to' => State::New->value,
    ]);
  }
}
