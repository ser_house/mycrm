<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.07.2020
 * Time: 10:19
 */

use App\Model\AccountType;
use App\Model\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountSeeder extends Seeder {
  public function run() {
    $user = User::first();

    DB::table('account_type')->insert([
      'name' => 'Type 1',
    ]);

    $account_type = AccountType::first();

    DB::table('account')->insert([
      'user_id' => $user->id,
      'type_id' => $account_type->id,
      'name' => 'Account 1 rub',
      'note' => '',
      'currency' => 'RUB',
      'is_active' => 1,
    ]);
  }
}
