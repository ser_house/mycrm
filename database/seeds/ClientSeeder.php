<?php

use App\Model\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ClientSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $user = User::first('id');

    DB::table('client')->insert([
      'user_id' => $user->id,
      'name' => 'Client name',
      'note' => '',
      'is_selectable' => true,
    ]);
  }
}
