<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.07.2020
 * Time: 10:19
 */

use App\Model\Client\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskTemplateSeeder extends Seeder {
  public function run() {
    $client = Client::first();

    DB::table('task_template')->insert([
      'title' => 'Task template',
      'description' => 'Task template description',
      'client_id' => $client->id,
      'currency' => 'RUB',
      'budget' => 100.00,
      'hours' => 12,
    ]);
  }
}
