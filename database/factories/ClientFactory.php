<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Model\Client\Client::class, function (Faker $faker) {
  return [
    'name' => 'Client name',
    'note' => '',
    'contacts' => [
      ['name' => 'contact_name1', 'value' => 'contact_value1'],
    ],
    'sites' => [
      ['url' => 'http://example.com'],
    ],
  ];
});
