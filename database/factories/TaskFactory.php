<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.07.2020
 * Time: 10:17
 */

use Application\Task\State;
use Faker\Generator as Faker;

$factory->define(App\Model\Client\Client::class, function (Faker $faker) {
  return [
    'title' => '',
    'description' => '',
    'client_site_id' => '',
    'currency' => 'RUB',
    'launched_at' => date('Y-m-d'),
    'completed_at' => '',
    'state' => State::New->value,
    'sub_tasks' => [
      [
        'id' => '',
        'title' => 'Основная',
        'task_id' => '',
        'budget' => '',
        'launched_at' => '',
        'completed_at' => '',
        'hours' => '',
      ],
    ],
  ];
});
