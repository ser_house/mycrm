<?php

use Application\Task\State;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Create extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   * @throws Exception
   */
  public function up() {

    $this->client();
    $this->account();
    $this->document();
    $this->task();
  }

  private function client() {
    Schema::create('client', function (Blueprint $table) {
//      $geodata_db = DB::connection('geodata')->getDatabaseName();

      $table->increments('id');
      $table->timestamp('created_at');
      $table->unsignedInteger('user_id');
      $table->string('name', 255);
      $table->text('note')->nullable();
      $table->unsignedInteger('city_id')->nullable();
      $table->boolean('is_selectable');
      $table->index('is_selectable');
      $table->binary('settings')->nullable();

      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('restrict');
//      $table->foreign('city_id')->references('id')->on(new \Illuminate\Database\Query\Expression($geodata_db . '.city'))->onDelete('cascade')->onUpdate('restrict');
    });

    Schema::create('client_contact', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('client_id');
      $table->string('name', 64);
      $table->string('value', 64);
      $table->string('note', 255)->nullable();

      $table->foreign('client_id')->references('id')->on('client')->onDelete('cascade')->onUpdate('restrict');
    });

    Schema::create('client_site', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('client_id');
      $table->string('url', 255);

      $table->foreign('client_id')->references('id')->on('client')->onDelete('cascade')->onUpdate('restrict');
    });
  }

  private function account() {
    Schema::create('account_type', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name', 64)->unique();
    });

    Schema::create('account', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamp('created_at');
      $table->unsignedInteger('user_id');
      $table->unsignedInteger('type_id');
      $table->string('name', 64);
      $table->string('note', 64)->nullable();
      $table->char('currency', 3)->default('RUB');
      $table->boolean('is_active');
      $table->index('is_active');

      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('restrict');
      $table->foreign('type_id')->references('id')->on('account_type')->onDelete('cascade')->onUpdate('restrict');
    });
  }

  private function document() {
    Schema::create('file', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('user_id');
      $table->timestamps();
      $table->string('path', 1024)->comment = 'Внутренний, файловый, путь к файлу.';
      $table->string('name', 255)->comment = 'Название файла вместе с расширением.';
      $table->string('orig_name')->default('');
      $table->unsignedBigInteger('size')->comment = 'Размер файла в байтах.';
      $table->string('mime', 255);
      $table->string('uri', 255)->nullable()->comment = 'uri, по которому файл может быть доступен публично. Может и не быть, если, например, файл не должен быть доступен публично. Это не то же самое, что дает asset для сохраненного файла.';

      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('restrict');
    });

    Schema::create('document_type', function (Blueprint $table) {
      $table->increments('id');
      $table->string('machine_name', 64)->unique();
      $table->string('title', 64)->unique();
    });

    Schema::create('document_template', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('client_id');
      $table->unsignedInteger('type_id');
      $table->unsignedInteger('file_id');
      $table->string('note', 64)->nullable();
      $table->binary('options')->nullable();
      $table->unsignedTinyInteger('weight')->default(0);

      $table->foreign('client_id')->references('id')->on('client')->onDelete('cascade')->onUpdate('restrict');
      $table->foreign('type_id')->references('id')->on('document_type')->onDelete('cascade')->onUpdate('restrict');
      $table->foreign('file_id')->references('id')->on('file')->onDelete('cascade')->onUpdate('restrict');
    });

    Schema::create('document', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamps();
      $table->unsignedInteger('client_id');
      $table->unsignedInteger('type_id');
      $table->unsignedInteger('file_id');
      $table->date('date')->nullable();
      $table->string('number', 50)->change();
      $table->tinyInteger('page_num', false, true)->default(1);
      $table->string('title', 64)->nullable();
      $table->unsignedTinyInteger('weight')->default(0);

      $table->foreign('client_id')->references('id')->on('client')->onDelete('cascade')->onUpdate('restrict');
      $table->foreign('type_id')->references('id')->on('document_type')->onDelete('cascade')->onUpdate('restrict');
      $table->foreign('file_id')->references('id')->on('file')->onDelete('cascade')->onUpdate('restrict');
    });
  }

  private function task() {
    Schema::create('task', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('user_id');
      $table->string('title');
      $table->text('description');
      $table->unsignedInteger('client_id');
      $table->unsignedInteger('client_site_id')->nullable();
      $table->char('currency', 3);
      $table->date('launched_at')->nullable()->comment('Реальная дата начала, для внутренних отчетов');
      $table->date('completed_at')->nullable()->comment('Реальная дата завершения, для внутренних отчетов');
      // Ссылка на шаблон, по которому была создана задача.
      // Заполняется только для задач, созданных по шаблону.
      // Пока таки задачи буду создавать только автоматически.
      $table->unsignedInteger('task_template_id')->nullable();
      $table->string('state')->default(State::New->value);

      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('restrict');
      $table->foreign('client_id')->references('id')->on('client')->onDelete('cascade')->onUpdate('restrict');

      $table->foreign('client_site_id')->references('id')->on('client_site')->onDelete('cascade')->onUpdate('restrict');
    });

    Schema::create('task_template', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamp('created_at');
      $table->boolean('is_active')->default(1);
      $table->string('title');
      $table->text('description')->nullable();
      $table->unsignedInteger('client_id');
      $table->unsignedInteger('client_site_id')->nullable();
      $table->char('currency', 3);
      $table->decimal('budget');
      $table->decimal('hours')->nullable();

      $table->foreign('client_id')->references('id')->on('client')->onDelete('cascade')->onUpdate('restrict');

      $table->foreign('client_site_id')->references('id')->on('client_site')->onDelete('cascade')->onUpdate('restrict');
    });

    Schema::create('task_state_journal', function (Blueprint $table) {
      $table->increments('id');
      $table->dateTime('inserted_at');
      $table->unsignedInteger('task_id');
      $table->string('state_to')->default(State::New->value);

      $table->foreign('task_id')->references('id')->on('task')->onDelete('cascade')->onUpdate('restrict');
    });

    DB::statement("ALTER TABLE task_state_journal CHANGE inserted_at inserted_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;");

    Schema::create('sub_task', function (Blueprint $table) {
      $table->increments('id');
      $table->text('title');
      $table->text('note')->nullable();
      $table->unsignedInteger('task_id');
      $table->decimal('budget');
      $table->decimal('hours')->nullable()->comment('Кол-во часов для актов-счетов');

      $table->foreign('task_id')->references('id')->on('task')->onDelete('cascade')->onUpdate('restrict');
    });

    Schema::create('payment', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('task_id');
      $table->date('created_at');
      $table->unsignedInteger('account_id');
      $table->string('note')->nullable();
      $table->decimal('amount');
      $table->decimal('rate')->default(1.00);

      $table->foreign('task_id')->references('id')->on('task')->onDelete('cascade')->onUpdate('restrict');
      $table->foreign('account_id')->references('id')->on('account')->onDelete('cascade')->onUpdate('restrict');
    });
// Это для возможных сущностей Todo, которые можно было бы одним движением перeводить в задачи.
//			Schema::create('task_file', function(Blueprint $table) {
//				$table->unsignedInteger('task_id');
//				$table->unsignedInteger('file_id')->comment('Связанные файлы форматов doc,docx,xls,xlsx,jpg,png.');
//
//				$table->foreign('task_id')->references('id')->on('task')->onDelete('cascade')->onUpdate('restrict');
//				$table->foreign('file_id')->references('id')->on('file')->onDelete('cascade')->onUpdate('restrict');
//			});
//
//			Schema::create('task_note', function(Blueprint $table) {
//				$table->unsignedInteger('task_id');
//				$table->text('content')->comment('Связанные куски текста, включающие ссылки (цитаты).');
//
//				$table->foreign('task_id')->references('id')->on('task')->onDelete('cascade')->onUpdate('restrict');
//			});
  }

  private function holiday() {
    Schema::create('holiday', function (Blueprint $table) {
      // Конечно, возможно стоило бы добавить поля месяца и индексы по нему
      // (чтобы искать в пределах конкретного не только года, но и месяца),
      // но как-то это глуповато на таких объемах, которые создают празники в году.

      $table->unsignedInteger('client_id')->nullable();
      $table->unsignedMediumInteger('year');
      $table->date('date');

      $table->index('year');

      $table->foreign('client_id')->references('id')->on('client')->onDelete('cascade')->onUpdate('restrict');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('payment');
    Schema::dropIfExists('sub_task');
    Schema::dropIfExists('task_state_journal');
    Schema::dropIfExists('task_template');
    Schema::dropIfExists('task');

    Schema::dropIfExists('document');
    Schema::dropIfExists('document_template');
    Schema::dropIfExists('document_type');
    Schema::dropIfExists('file');

    Schema::dropIfExists('account');
    Schema::dropIfExists('account_type');

    Schema::dropIfExists('holiday');
    Schema::dropIfExists('client_site');
    Schema::dropIfExists('client_contact');
    Schema::dropIfExists('client');
  }
}
