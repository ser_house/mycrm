<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TemplatesCorrecting extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    $this->correctOptions();

    Schema::create('task_document', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('task_id');
      $table->unsignedInteger('document_id');

      $table->foreign('task_id')->references('id')->on('task')->onDelete('cascade')->onUpdate('restrict');
      $table->foreign('document_id')->references('id')->on('document')->onDelete('cascade')->onUpdate('restrict');
      $table->unique(['task_id', 'document_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('task_document');
  }

  private function correctOptions() {
    $templates = \App\Model\DocumentTemplate::all();
    Schema::table('document_template', function (Blueprint $table) {
      $table->dropColumn('options');
    });
    Schema::table('document_template', function (Blueprint $table) {
      $table->text('options');
    });
    $any_correct_options_json = null;
    foreach ($templates as $template) {
      if (6 === $template->id) {
        continue;
      }

      $options = unserialize($template->options, ['allowed_classes' => false]);
      if (isset($options['options'])) {
        $options = $options['options'];
      }

      if (array_key_exists('id', $options)) {
        unset($options['id']);
      }
      foreach (['append_hours_suffix', 'human_amount_upfirst', 'human_amount_append_units_suffix'] as $key) {
        $options[$key] = (bool)$options[$key];
      }

      $options_json = json_encode($options);
      if (null === $any_correct_options_json) {
        $any_correct_options_json = $options_json;
      }
      \Illuminate\Support\Facades\DB::table('document_template')
        ->where('id', $template->id)
        ->update(['options' => $options_json])
      ;
    }
    \Illuminate\Support\Facades\DB::table('document_template')
      ->where('id', 6)
      ->update(['options' => $any_correct_options_json])
    ;
  }
}
