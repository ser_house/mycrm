export class Selection {
	constructor(input) {
		this.start = this.end = this.length = 0;
		this.input = input;
		this.update();
	}

	update() {
		if (this.input.selectionStart === undefined) {
			this.input.focus();
			let select = document.selection.createRange();
			this.length = select.text.length;
			select.moveStart('character', -this.input.value.length);
			this.end = select.text.length;
			this.start = this.end - this.length;
		}
		else {
			this.start = this.input.selectionStart;
			this.end = this.input.selectionEnd;
			this.length = this.end - this.start;
		}
		return this;
	};

	set(start, end) {
		this.start = start;
		this.end = end;
		this.length = start - end;
		return this;
	};

	apply() {
		if (this.input.selectionStart === undefined) {
			this.input.focus();
			let range = this.input.createTextRange();
			range.collapse(true);
			range.moveEnd('character', this.end);
			range.moveStart('character', this.start);
			range.select();
		}
		else {
			this.input.selectionStart = this.start;
			this.input.selectionEnd = this.end;
		}
		return this;
	};

	moveForward(count) {
		if (this.start === 0 && count === -1) {
			return this;
		}
		this.start += count;
		this.end += count;

		return this;
	};

	moveBack(count) {
		this.start -= count;
		this.end -= count;

		return this;
	};
}
