export class Notify {

  constructor(options = {timeout: 0}) {
    this.timeout = options.timeout;
  }

  success(html, title = '', timeout = 3) {
    this.push(html, 'success', title, timeout);
  }

  warning(html, title = '', timeout = 0) {
    this.push(html, 'warning', title, timeout);
  }

  error(html, title = '', timeout = 0) {
    this.push(html, 'error', title, timeout);
  }

  push(html, type, title = '', timeout = 0) {
    let wrapper = this.getWrapper();

    let item = document.createElement('section');
    item.className = `notify ${type} fade-in`;
    let itemHtml = '<span class="close">x</span>';
    if (title) {
      itemHtml += `<header>${title}</header>`;
    }
    itemHtml += `<main>${html}</main>`;
    item.innerHTML = itemHtml;

    wrapper.appendChild(item);
    this.addOnClick(item);

    setTimeout(() => {
      item.classList.remove('fade-in');
      if (timeout || this.timeout) {
        // if (wrapper.firstChild !== item) {
        //   return;
        // }
        setTimeout(() => {
          item.classList.add('hide');
          'webkitAnimationEnd,animationend'.split(',').forEach(function(e){
            item.addEventListener('animationend', () => {
              item.remove();
              if (!parseInt(wrapper.childElementCount)) {
                wrapper.remove();
              }
            });
          });
        }, timeout ? timeout * 1000 : this.timeout * 1000);
      }
    }, 100);
  }

  getWrapper = () => {
    let wrapper = document.getElementById('notify-wrapper');
    if (!wrapper) {
      wrapper = document.createElement('div');
      wrapper.id = 'notify-wrapper';
      wrapper.className = 'notify-wrapper';
      document.body.insertBefore(wrapper, document.body.firstChild);
    }
    return wrapper;
  };

  addOnClick = (el) => {
    el.querySelector('.close').onclick = (e) => {
      this.close(el);
    };
  };

  close = (el) => {
    el.classList.add('closed');
    el.addEventListener('transitionend', () => {
      let wrapper = document.getElementById('notify-wrapper');
      if (wrapper) {
        wrapper.removeChild(el);
        if (!parseInt(wrapper.childElementCount)) {
          wrapper.remove();
        }
      }
    });
  };
}

// https://vuejs.org/v2/guide/plugins.html
Notify.install = function (Vue, options) {
  let notify = new Notify(options);

  Vue.prototype.success = function (html, title = '', timeout = 3) {
    notify.success(html, title, timeout);
  };

  Vue.prototype.warning = function (html, title = '', timeout = 0) {
    notify.warning(html, title, timeout);
  };

  Vue.prototype.error = function (html, title = '', timeout = 0) {
    notify.error(html, title, timeout);
  };
};
