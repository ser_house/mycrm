export const CURRENCIES = {
  'RUB': {
    title: 'Российский рубль',
    thousandsSeparator: ' ',
    decimalSeparator: ',',
    decimalsCount: 2,
    prefix: '',
    suffix: ' ₽'
  },
  'USD': {
    title: 'Доллар США',
    thousandsSeparator: ',',
    decimalSeparator: '.',
    decimalsCount: 2,
    prefix: '$',
    suffix: ''
  },
  'EUR': {
    title: 'Евро',
    thousandsSeparator: ' ',
    decimalSeparator: ',',
    decimalsCount: 2,
    prefix: '',
    suffix: '€'
  }
};



