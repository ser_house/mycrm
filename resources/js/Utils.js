let Utils = {
  dateToStr(date) {
    let year = date.getFullYear();
    let month = (date.getMonth() + 1);
    if (1 === month.toString().length) {
      month = '0' + month;
    }
    let day = date.getDate();
    if (1 === day.toString().length) {
      day = '0' + day;
    }
    return `${year}-${month}-${day}`;
  },

  firstDayNextMonthAsStr(date) {
    let firstDayDate = new Date(date.getFullYear(), date.getMonth() + 1, 1);
    return this.dateToStr(firstDayDate);
  },

  lastDayMonthAsStr(date) {
    let lastDayDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    return this.dateToStr(lastDayDate);
  },
};
export {Utils};
