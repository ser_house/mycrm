// https://blog.logrocket.com/how-to-manipulate-css-colors-with-javascript-fb547113a1b8/
const rgbToLightness = (r,g,b) => 1/2 * (Math.max(r,g,b) + Math.min(r,g,b));
const rgbToSaturation = (r,g,b) => {
  const L = rgbToLightness(r,g,b);
  const max = Math.max(r,g,b);
  const min = Math.min(r,g,b);
  return (L === 0 || L === 1)
    ? 0
    : (max - min)/(1 - Math.abs(2 * L - 1));
};
const rgbToHue = (r,g,b) => Math.round(
  Math.atan2(
    Math.sqrt(3) * (g - b),
    2 * r - g - b,
  ) * 180 / Math.PI
);
const rgbToHsl = (r,g,b) => {
  const lightness = rgbToLightness(r,g,b);
  const saturation = rgbToSaturation(r,g,b);
  const hue = rgbToHue(r,g,b);
  return [hue, saturation, lightness];
};
const rgbToObject = (red,green,blue) => {
  const [hue, saturation, lightness] = rgbToHsl(red, green, blue);
  return {red, green, blue, hue, saturation, lightness};
};
const compareLightness = (a,b) => a.lightness - b.lightness;
const compareSaturation = (a,b) => a.saturation - b.saturation;
const compareHue = (a,b) => a.hue - b.hue;
const compareAttribute = attribute =>
  (a,b) => a[attribute] - b[attribute];
// const compareLightness = compareAttribute('lightness');
// const compareSaturation = compareAttribute('saturation');
// const compareHue = compareAttribute('hue');

const toSum = (a,b) => a + b;
const toAttribute = attribute => element => element[attribute];
const averageOfAttribute = attribute => array =>
  array.map(toAttribute(attribute)).reduce(toSum) / array.length;
const normalizeAttribute = attribute => array => {
  const averageValue = averageOfAttribute(attribute)(array);
  const normalize = overwriteAttribute(attribute)(averageValue);
  return normalize(array);
};
const normalizeSaturation = normalizeAttribute('saturation');
const normalizeLightness = normalizeAttribute('lightness');
const normalizeHue = normalizeAttribute('hue');

window.addEventListener('DOMContentLoaded', function() {
  const styles = document.styleSheets;

  let cssArr = [...styles[0].cssRules].map(function(x) {
    if ('undefined' === typeof x.selectorText) {
      return false;
    }

    if ('undefined' === typeof x.style) {
      return false;
    }
    if ('undefined' === typeof x.style.color && 'undefined' === typeof x.style.backgroundColor) {
      return false;
    }
    if (!x.style.color.length && !x.style.backgroundColor.length) {
      return false;
    }

    return {
      class: x.selectorText,
      color: x.style.color,
      background: x.style.backgroundColor,
    };
  });
  let div = document.createElement("div");
  cssArr = cssArr.filter(item => item);

  cssArr = cssArr.sort((item1, item2) => {
    div.style.backgroundColor = item1.background;
    document.body.appendChild(div);
    let bg1 = window.getComputedStyle(div).backgroundColor;
    let bg1arr = bg1.match(/\((.*)\)/)[1].split(',');

    div.style.backgroundColor = item2.background;
    document.body.appendChild(div);
    let bg2 = window.getComputedStyle(div).backgroundColor;
    let bg2arr = bg2.match(/\((.*)\)/)[1].split(',');

    bg1 = rgbToObject(bg1arr[0], bg1arr[1], bg1arr[2]);
    bg2 = rgbToObject(bg2arr[0], bg2arr[1], bg2arr[2]);

    return bg1.hue - bg2.hue;
  });

  div.remove();

  let main = document.getElementById('colors-table');
  let tbody = main.getElementsByTagName('tbody')[0];

  for(let item of cssArr) {
    let tr = document.createElement('tr');
    tbody.appendChild(tr);

    let td = document.createElement('td');
    td.textContent = item.class;
    tr.appendChild(td);

    td = document.createElement('td');
    td.style.color = item.color;
    td.style.backgroundColor = item.background;
    td.textContent = 'text';
    tr.appendChild(td);
  }
});
