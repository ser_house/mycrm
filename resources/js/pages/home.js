import {BarChart, FormatterMoney} from 'chart';

let formatter = new FormatterMoney('RUB');

window.addEventListener('DOMContentLoaded', function () {
  let elChart1 = document.getElementById('by-years-char');
  let data = JSON.parse(elChart1.getAttribute('data-data'));

  new BarChart(elChart1, {
    title: data.title,
    labels_x: data.data.labels_x,
    values: data.data.values,
    valuesFormatter: formatter,
    fill_color: ['#7fc3ff'],
    border_color: ['#0057a3'],
  });

  let elChart3 = document.getElementById('current-year-chart');
  data = JSON.parse(elChart3.getAttribute('data-data'));
  new BarChart(elChart3, {
    title: data.title,
    labels_x: data.data.labels_x,
    values: data.data.values,
    valuesFormatter: formatter,
    fill_color: ['#7fc3ff'],
    border_color: ['#0057a3'],
  });
});
