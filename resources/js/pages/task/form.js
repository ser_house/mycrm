require('../../app');

Vue.component('site', require('../../vue/Site').default);
Vue.component('sub-tasks', require('../../vue/task/SubTasks').default);
Vue.component('payments', require('../../vue/task/Payments').default);
Vue.component('task-form', require('../../vue/task/TaskForm').default);

