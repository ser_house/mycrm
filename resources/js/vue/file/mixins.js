import {Formatter} from "../../mixins";

export let dragDrop = {
  mixins: [
    Formatter,
  ],
  data() {
    return {
      toUpload: [],
      files: [],
      uploadedItems: [],
      dragAndDropCapable: false,
      isUploading: false,
      uploadPercentage: 0,
      errors: new Map(),
    };
  },
  props: {
    client: Object,
    rules: Object,
  },
  methods: {
    determineDragAndDropCapable() {
      var div = document.createElement('div');
      return (('draggable' in div)
        || ('ondragstart' in div && 'ondrop' in div))
        && 'FormData' in window
        && 'FileReader' in window;
    },
    initDragDrop() {
      this.dragAndDropCapable = this.determineDragAndDropCapable();
      if (this.dragAndDropCapable) {
        ['drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop'].forEach((evt) => {
          this.$refs.drop_zone.$el.addEventListener(evt, (e) => {
            e.preventDefault();
            e.stopPropagation();
          });
        });

        this.$refs.drop_zone.$el.addEventListener('drop', (e) => {
          this._addFiles(e.dataTransfer.files);
        });
      }
    },
    onDropZoneClicked() {
      this.$refs.input.click();
    },
    handleFilesUpload() {
      this._addFiles(this.$refs.input.files);
    },
    _addFiles(files) {
      for (let i = 0; i < files.length; i++) {
        let file = files[i];

        if (!this.validateFileType(file)) {
          let fileErrors = {};
          if (this.errors.has('files')) {
            fileErrors = this.errors.get('files');
          }

          if (!file.type.length) {
            fileErrors[i] = `Тип файла ${file.name} неизвестен.`;
          }
          else {
            fileErrors[i] = `Тип файла ${file.name} (<strong>${file.type}</strong>) не допускается.`;
          }

          this.errors.set('files', fileErrors);

          this.errors = new Map(this.errors);
        }
        else if (!this.validateFileSize(file)) {
          let fileErrors = {};
          if (this.errors.has('files')) {
            fileErrors = this.errors.get('files');
          }

          let file_size = this.formatFileSize(file.size);
          let max_size = this.formatFileSize(this.rules.max_filesize);
          fileErrors[i] = `Размер файла ${file.name} (<strong>${file_size}</strong>) больше допустимого (<strong>${max_size}</strong>).`;
          this.errors.set('files', fileErrors);

          this.errors = new Map(this.errors);
        }

        if (!this.errors.size) {
          this._addFile(file);
        }
      }
    },
    removeToUploadFile(index) {
      this.toUpload.splice(index, 1);
      this.files.splice(index, 1);
    },
    removeUploadedItem(index) {
      let url = this.uploadedItems[index].file.delete_url;
      axios.delete(url).then((response) => {
        if ('success' === response.data.status) {
          this.success(response.data.msg);
          this.uploadedItems.splice(index, 1);
        }
        else {
          this.error(response.data.msg);
        }
      });
    },
    validateFileSize(file) {
      return file.size <= this.rules.max_filesize;
    },
    validateFileType(file) {
      return 'undefined' !== typeof this.rules.types[file.type];
    },
    upload(url, formData) {
      this.isUploading = true;
      axios.post(url, formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          onUploadProgress: (progressEvent) => {
            this.uploadPercentage = parseInt(Math.round((progressEvent.loaded * 100) / progressEvent.total));
          }
        }
      ).then((response) => {
        this.processResponse(response);
      });
    },
    processResponse(response) {
      this.isUploading = false;
      response.data.files.forEach((file) => {
        if (!file.error) {
          this.uploadedItems.unshift(file);
          this.toUpload = [];
          this.files = [];
        }
        else {
          this.error(file.error, file.name);
        }
      });
    }
  },
  mounted() {
    this.initDragDrop();
  },
};
