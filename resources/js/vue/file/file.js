var fileTypes = {};

fileTypes['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'] = {
	text: 'xls',
	class: 'file-type-xls',
	extensions: 'xls, xlsx'
};

fileTypes['application/vnd.openxmlformats-officedocument.wordprocessingml.document'] = {
	text: 'doc',
	class: 'file-type-doc',
	extensions: 'doc, docx'
};

fileTypes['image/jpeg'] = {
	text: 'pic',
	class: 'file-type-pic',
	extensions: 'jpg, jpeg'
};

fileTypes['image/png'] = {
	text: 'pic',
	class: 'file-type-pic',
	extensions: 'png'
};

fileTypes['application/pdf'] = {
	text: 'pdf',
	class: 'file-type-pdf',
	extensions: 'pdf'
};
