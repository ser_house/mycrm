<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.10.2019
 * Time: 16:40
 */

return [
	'Anadyr' => 'Анадырь',
	'Barnaul' => 'Барнаул',
	'Chita' => 'Чита',
	'Irkutsk' => 'Иркутск',
	'Kamchatka' => 'Камчатка',
	'Khandyga' => 'Хандыга',
	'Krasnoyarsk' => 'Красноярск',
	'Magadan' => 'Магадан',
	'Novokuznetsk' => 'Новокузнецк',
	'Novosibirsk' => 'Новосибирск',
	'Omsk' => 'Омск',
	'Sakhalin' => 'Сахалин',
	'Srednekolymsk' => 'Среднеколымск',
	'Tomsk' => 'Томск',
	'Ust-Nera' => 'Усть-Нера',
	'Vladivostok' => 'Владивосток',
	'Yakutsk' => 'Якутск',
	'Yekaterinburg' => 'Екатеринбург',
	'Astrakhan' => 'Астрахань',
	'Kaliningrad' => 'Калининград',
	'Kirov' => 'Киров',
	'Moscow' => 'Москва',
	'Samara' => 'Самара',
	'Saratov' => 'Саратов',
	'Simferopol' => 'Симферополь',
	'Ulyanovsk' => 'Ульяновск',
	'Volgograd' => 'Волгоград',
];
