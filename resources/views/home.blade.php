<?php
$by_years_chart = $by_years ? [
  'title' => 'Доход по годам',
  'data' => $by_years,
] : null;
$current_year_chart = $current_year ? [
  'title' => 'Доход в текущем году',
  'data' => $current_year,
] : null;
?>

@extends('layouts.app')

@push('styles')
  <link href="{{ mix('css/chart.css') }}" rel="stylesheet">
@endpush

@push('scripts')
  <script src="{{ mix('js/pages/task/index.js') }}"></script>
  <script src="{{ mix('js/pages/home.js') }}"></script>
@endpush

@section('content')
  <section class="dashboard">
    @if (session('status'))
      <div class="alert alert-success">
        {{ session('status') }}
      </div>
    @endif
    <section class="active-tasks">
      <header><h2>Активные задачи</h2></header>
      <main>
        @if(!empty($active_tasks['tasks']))
          <tasks :init-tasks='@json($active_tasks['tasks'])'></tasks>
          {!! $active_tasks['pager'] !!}
        @else
          Нет активных задач.
        @endif
      </main>
    </section>
    <section class="dashboard-incoming">
      <header><h2>Доходы</h2></header>
      <main>
        @if(!$is_incoming_empty)
          @if($by_years)
            <div id="by-years-char" class="bar-chart chart" data-data='@json($by_years_chart)'></div>
          @endif
          @if($current_year_chart)
            <div id="current-year-chart" class="line-chart chart" data-data='@json($current_year_chart)'></div>
          @endif
        @else
          Нет доходов.
        @endif
      </main>
    </section>
  </section>
@endsection
