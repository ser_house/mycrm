<?php
/**
 * @var $client \App\Model\Client\Client
 * @var $active string|null
 * @var $is_active_clients bool|null
 */
$route_params = ['client' => $client->id];
$active = !empty($active) ? $active : '';
$is_active_clients = $is_active_clients ?? false;
?>
<ul class="client-actions actions menu-vert">
  <li>
    <a @if('client.show' === $active)class="active" @endif href="{{route('client.show', $route_params)}}"><span class="icon icon-client"></span>просмотр</a>
  </li>
  <li>
    <a @if('client.edit' === $active)class="active" @endif href="{{route('client.edit', $route_params)}}"><span class="icon icon-edit"></span>изменить</a>
  </li>
  {{--	<li>--}}
  {{--		<a href="{{route('client.remove', $route_params)}}"><span class="icon icon-delete"></span>удалить</a>--}}
  {{--	</li>--}}
  @if($is_active_clients)
    <li class="collapsible">
      <label class="toggle-link"><span class="icon icon-task"></span>задачи<span class="icon icon-right toggle-icon"></span></label>
      <ul class="toggle-content collapsed">
        <li>
          <a @if('client.tasks' === $active)class="active" @endif href="{{route('client.tasks', $route_params)}}"><span class="icon icon-task"></span>задачи</a>
        </li>
        <li>
          <a @if('client.task.create' === $active)class="active" @endif href="{{route('client.task.create', $route_params)}}"><span class="icon icon-add"></span>добавить</a>
        </li>
        <li>
          <a @if('client.task_templates' === $active)class="active" @endif href="{{route('client.task_templates', $route_params)}}"><span class="icon icon-template"></span>шаблоны</a>
        </li>
      </ul>
    </li>
    <li class="collapsible">
      <label class="toggle-link"><span class="icon icon-documents"></span>документы<span class="icon icon-right toggle-icon"></span></label>
      <ul class="toggle-content collapsed">
        <li>
          <a @if('client.documents' === $active)class="active" @endif href="{{route('client.documents', $route_params)}}"><span class="icon icon-documents"></span>документы</a>
        </li>
        <li>
          <a @if('client.documents.templates' === $active)class="active" @endif href="{{route('client.documents.templates', $route_params)}}"><span class="icon icon-template"></span>шаблоны</a>
        </li>
        <li>
          <a @if('client.documents.settings' === $active)class="active" @endif href="{{route('client.documents.settings', $route_params)}}"><span class="icon icon-settings"></span>настройки</a>
        </li>
        <li>
          <a @if('client.documents.generator' === $active)class="active" @endif href="{{route('client.documents.generator', $route_params)}}"><span class="icon icon-generator"></span>генератор</a>
        </li>
      </ul>
    </li>
  @else
    <li>
      <a @if('client.tasks' === $active)class="active" @endif href="{{route('client.tasks', $route_params)}}"><span class="icon icon-task"></span>задачи</a>
    </li>
    <li>
      <ul>
        <li>
          <a @if('client.task.create' === $active)class="active" @endif href="{{route('client.task.create', $route_params)}}"><span class="icon icon-add"></span>добавить</a>
        </li>
        <li>
          <a @if('client.task_templates' === $active)class="active" @endif href="{{route('client.task_templates', $route_params)}}"><span class="icon icon-template"></span>шаблоны</a>
        </li>
      </ul>
    </li>
    <li>
      <a @if('client.documents' === $active)class="active" @endif href="{{route('client.documents', $route_params)}}"><span class="icon icon-documents"></span>документы</a>
    </li>
    <li>
      <ul>
        <li>
          <a @if('client.documents.templates' === $active)class="active" @endif href="{{route('client.documents.templates', $route_params)}}"><span class="icon icon-template"></span>шаблоны</a>
        </li>
        <li>
          <a @if('client.documents.settings' === $active)class="active" @endif href="{{route('client.documents.settings', $route_params)}}"><span class="icon icon-settings"></span>настройки</a>
        </li>
        <li>
          <a @if('client.documents.generator' === $active)class="active" @endif href="{{route('client.documents.generator', $route_params)}}"><span class="icon icon-generator"></span>генератор</a>
        </li>
      </ul>
    </li>
  @endif
  <li>
    <a @if('client.calendar' === $active)class="active" @endif href="{{route('client.calendar', $route_params)}}"><span class="icon icon-calendar"></span>календарь</a>
  </li>
  <li>
    <a @if('client.holidays' === $active)class="active" @endif href="{{route('client.holidays', $route_params)}}"><span class="icon icon-calendar"></span>выходные</a>
  </li>
</ul>
