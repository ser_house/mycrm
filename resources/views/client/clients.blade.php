<?php
/**
 * @var App\Model\Client\Client[] $clients
 */
?>
@extends('layouts.app')
@section('page_title', 'Клиенты')

@push('scripts')
  <script src="{{ mix('js/pages/client/index.js') }}"></script>
@endpush

@section('content')
  @spaceless
  <header>
    <h1>Клиенты</h1>
    <div class="actions">
      <a class="btn btn-primary" href="{{ route('client.create') }}">Добавить</a>
    </div>
  </header>
  <ul class="tabs">
    @php
      $active_route = route('clients.index', ['active' => 'active']);
      $inactive_route = route('clients.index', ['active' => 'inactive']);
    @endphp
    <li class="tab-button @if('active' === $active) active @endif">
      <a href="{{ $active_route }}">Активные</a>
    </li>
    <li class="tab-button @if('inactive' === $active) active @endif">
      <a href="{{ $inactive_route }}">Неактивные</a>
    </li>
  </ul>
  <div class="clients tab-content">
    @if(count($clients) > 0)
      <clients :init-clients='@json($clients)'></clients>
    @else
      <div>Нет клиентов.</div>
    @endif
  </div>
  @endspaceless
@endsection
