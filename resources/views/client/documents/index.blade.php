<?php
/**
 * @var $client \App\Model\Client\Client
 */
?>
@extends('layouts.app')
@section('page_title', "$client->name: документы")

@push('scripts')
  <script src="{{ mix('js/pages/client/documents.js') }}"></script>
@endpush

@push('styles')
  <link href="{{ mix('css/file.css') }}" rel="stylesheet">
@endpush
@section('content')
  @php ($route_params = ['client' => $client->id])
  @component('client.documents.tabs', ['route_params' => $route_params, 'active' => 'documents'])@endcomponent
  <section class="tab-content">
    <header><h1>Документы клиента "{{$client->name}}"</h1></header>
    <documents
      :client='@json($client)'
      :document-types='@json($doc_types)'
      :rules='@json($document_rules)'
      :init-documents='@json($documents)'
    ></documents>
  </section>
@endsection
