@spaceless
<ul class="tabs">
  <li class="tab-button @if('documents' === $active) active @endif">
    <a href="{{ route('client.documents', $route_params) }}"><span class="icon icon-documents"></span>Документы</a>
  </li>
  <li class="tab-button @if('templates' === $active) active @endif">
    <a href="{{ route('client.documents.templates', $route_params) }}"><span class="icon icon-template"></span>Шаблоны</a>
  </li>
  <li class="tab-button @if('settings' === $active) active @endif">
    <a href="{{ route('client.documents.settings', $route_params) }}"><span class="icon icon-settings"></span>Настройки</a>
  </li>
  <li class="tab-button @if('generator' === $active) active @endif">
    <a href="{{ route('client.documents.generator', $route_params) }}"><span class="icon icon-generator"></span>Генератор</a>
  </li>
</ul>
@endspaceless
