<?php
/**
 * @var $client \App\Model\Client\Client
 */
?>
@extends('layouts.app')
@section('page_title', "$client->name: шаблоны")

@push('scripts')
  <script src="{{ mix('js/pages/client/document_templates.js') }}"></script>
@endpush
@push('styles')
  <link href="{{ mix('css/file.css') }}" rel="stylesheet">
@endpush
@section('content')
  @php ($route_params = ['client' => $client->id])
  @component('client.documents.tabs', ['route_params' => $route_params, 'active' => 'templates'])@endcomponent
  <section class="tab-content">
    <header><h1>Шаблоны клиента "{{$client->name}}"</h1></header>
    <document-templates
      :client='@json($client)'
      :template-types='@json($doc_types)'
      :rules='@json($template_rules)'
      :init-templates='@json($templates)'
      :vars='@json($variables)'
      :option-defs='@json($options_definitions)'
      :date-formats='@json($date_formats)'
    ></document-templates>
  </section>
@endsection
