<?php
/**
 * @var $client \App\Model\Client\Client
 */
?>
@extends('layouts.app')
@section('page_title', "$client->name: настройки")

@push('scripts')

@endpush

@section('content')
  @php ($route_params = ['client' => $client->id])
  @component('client.documents.tabs', ['route_params' => $route_params, 'active' => 'settings'])@endcomponent
  <section class="tab-content">
  <h1>Настройки генерации документов клиента "{{$client->name}}"</h1>

  <form method="post" autocomplete="off" action="{{route('client.documents.settings.save', $route_params)}}">
    {{csrf_field()}}
    <fieldset>
      <legend>Опции</legend>
      @foreach($data['template_options'] as $key => $item)
        @if(isset($item['options']))
          <div class="form-inline">
            <div class="input-group input-group-sm col-xs-12 col-sm-5">
              <span class="input-group-addon">{!! $item['title'] !!}</span>
              <select class="form-control date-format" name="{!! $key !!}">
                @foreach($item['options'] as $value => $label)
                  <option value="{!! $value !!}" @if($value == $item['default_value']) selected @endif>{!! $label !!}</option>
                @endforeach
              </select>
            </div>
          </div>
        @elseif(isset($item['type']) && 'string' === $item['type'])
          <div class="form-group{{ $errors->has($key) ? ' has-error' : '' }}">
            <label for="{!! $key !!}">{!! $item['title'] !!}:</label>
            <input id="{!! $key !!}" type="text" class="form-control col-note" name="{!! $key !!}" value="{!! $item['default_value'] !!}">
            <span class="help-block">Допустимые переменные: <strong>{doc_type}</strong>, <strong>{doc_num}</strong></span>
            @if($errors->has($key))
              {{ $errors->first($key) }}
            @endif
          </div>
        @else
          <div class="checkbox">
            <label>
              <input type="checkbox" value="1"
                name="{!! $key !!}" @if($item['default_value']) checked @endif>{!! $item['title'] !!}
            </label>
          </div>
        @endif

      @endforeach
    </fieldset>
    <fieldset>
      <legend>Используемые переменные</legend>
      @foreach($data['variables'] as $type => $variables)
        <div class="vars-type">
          <label class="vars-type-title">{!! $variables['title'] !!}</label>
          @foreach($variables['vars'] as $name => $var)
            <div class="checkbox">
              <label>
                <input type="checkbox" value="1"
                  name="{!! $name !!}" @if($var->value) checked @endif><span @if($var->value)class="var-name"@endif>{!! $var->var !!}</span><span class="var-title">{!! $var->title !!}</span>
              </label>
            </div>
          @endforeach
        </div>
      @endforeach
    </fieldset>
    <div class="row">
      <div class="form-group actions">
        <button type="submit" class="btn btn-success">Сохранить</button>
      </div>
    </div>
  </form>
  </section>
@endsection
