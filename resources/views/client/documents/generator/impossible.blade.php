<?php
/**
 * @var $client \App\Model\Client\Client
 */
$state = \Application\Task\State::Finished;
?>
@extends('layouts.app')
@section('page_title', 'Генератор')

@push('scripts')
  <script src="{{ mix('js/pages/client/documents_generator.js') }}"></script>
@endpush

@section('content')
  @php ($route_params = ['client' => $client->id])
  @component('client.documents.tabs', ['route_params' => $route_params, 'active' => 'generator'])@endcomponent
  <h1>Генератор документов для клиента "{{$client->name}}"</h1>
  @if($missing['templates'])
    <div class="alert alert-warning">Для клиента не загружены шаблоны документов. Вы можете <a href="{{route('client.documents.templates', $route_params)}}">загрузить</a> их сейчас.</div>
  @elseif($missing['tasks'])
    <div class="alert alert-warning">У клиента нет задач в состоянии <span class="task-state {!! $state->cssClass() !!}">{{ $state->label() }}</span>.
    </div>
    <a class="btn btn-secondary" href="{{route('client.documents.generator', $route_params + ['state' => 'closed'])}}">Сгенерировать для закрытых</a>
  @endif
@endsection
