<?php
/**
 * @var $client \App\Model\Client\Client
 * @var $only_closed bool
 * @var $can_to_pdf bool
 */
?>
@extends('layouts.app')
@section('page_title', 'Генератор')

@push('scripts')
  <script src="{{ mix('js/pages/client/documents_generator.js') }}"></script>
@endpush

@section('content')
  @php ($route_params = ['client' => $client->id])
  @component('client.documents.tabs', ['route_params' => $route_params, 'active' => 'generator'])@endcomponent
  <section class="tab-content">
    <header><h1>Генератор документов для клиента "{{$client->name}}"</h1></header>
    <main>
      <input type="hidden" id="client-id" value="{!! $client->id !!}"/>
      <generator
        csrf='@csrf'
        client-id="{!! $client->id !!}"
        :can-to-pdf='@json($can_to_pdf)'
        :only-closed='@json($only_closed)'
      ></generator>
    </main>
  </section>
@endsection
