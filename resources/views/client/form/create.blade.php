@extends('layouts.app')
@section('page_title', 'Новый клиент')

@push('scripts')
  <script src="{{ mix('js/pages/client/form.js') }}"></script>
@endpush

@section('content')
  @spaceless
  <h1>Новый клиент</h1>
  <form id="client-form" method="post" action="{{route('clients.add')}}">
    {{csrf_field()}}

    <div class="form-group required @error('name')has-error @enderror">
      <label for="name">Название:</label>
      <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>
      @field_error('name')
    </div>

    <div class="form-group @error('note')has-error @enderror">
      <label for="note">Примечание:</label>
      <textarea id="note" class="form-control" name="note" rows="3">{{ old('note') }}</textarea>
      @field_error('note')
    </div>

    <contacts></contacts>
    <sites></sites>

    <div class="form-actions">
      <button type="submit" class="btn btn-success" name="save" value="save">Сохранить</button>
      <button type="submit" class="btn btn-success" name="save" value="save_and_add">Сохранить и добавить ещё</button>
    </div>
  </form>
  @endspaceless
@endsection
