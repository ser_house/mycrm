<?php
/**
 * @var $client \App\Model\Client\Client
 */
?>
@extends('layouts.app')
@section('page_title', 'Редактирование клиента')

@push('scripts')
  <script src="{{ mix('js/pages/client/form.js') }}"></script>
@endpush

@section('content')
  @spaceless
  <h1>Редактирование клиента "{{$client->name}}"</h1>
  <form id="client-form" method="post" action="{{route('client.update', ['client' => $client->id])}}">
    {{csrf_field()}}
    {{ method_field('PATCH') }}

    <div class="form-check form-group">
      <input id="is-selectable" type="checkbox" class="form-check-input" value="1" name="is_selectable"
             @if($client->is_selectable) checked @endif>
      <label class="form-check-label" for="is-selectable">доступен для выбора</label>
    </div>

    <div class="form-group required @error('name')has-error @enderror">
      <label for="name">Название:</label>
      <input id="name" type="text" class="form-control" name="name" required value="{{ old('name', $client->name) }}">
      @field_error('name')
    </div>

    <div class="form-group @error('note')has-error @enderror">
      <label for="note">Примечание:</label>
      <textarea id="note" class="form-control" name="note" rows="3">{{ old('note', $client->note) }}</textarea>
      @field_error('note')
    </div>

    <contacts :init-items='@json($client->contacts)'></contacts>
    <sites :init-items='@json($client->sites)'></sites>

    <div class="form-actions">
      <button type="submit" class="btn btn-success">Сохранить</button>
    </div>
  </form>
  @endspaceless
@endsection
