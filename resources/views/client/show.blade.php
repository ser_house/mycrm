<?php
/**
 * @var $client Application\Client\View\Full
 * @var $documents Application\Client\View\Document[]
 */
?>
@extends('layouts.app')
@section('page_title', $client->name)

@section('content')
	@spaceless
	<section class="client">
		<header><h1>Клиент "{{$client->name}}"</h1>
			<div class="actions">
				<a class="btn btn-success" href="{{route('client.task.create', ['client' => $client->id])}}">Добавить задачу</a>
			</div>
		</header>
		<main>
			<div class="form-group inline-wrapper">
				<label>Добавлен:</label> {!! $client->created_at !!}
			</div>
			@if($client->note)
				<div class="form-group">
					<label>Примечание:</label> {!! nl2br(e($client->note), false) !!}
				</div>
			@endif
			<div class="form-group inline-wrapper">
				<label>Доступен для выбора:</label> {!! $client->active !!}
			</div>
			<div class="form-group inline-wrapper">
				<label>Город:</label> {!! $client->city !!}</div>
			<div class="form-group">
				<label>Контакты:</label>
				@if(count($client->contacts) > 0)
					<table>
						<thead>
						<tr>
							<th>Метка</th>
							<th>Значение</th>
							<th>Примечание</th>
						</tr>
						</thead>
						<tbody>
						@foreach($client->contacts as $contact)
							<tr>
								<td>{{$contact->name}}</td>
								<td>{{$contact->value}}</td>
								<td>{{$contact->note}}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				@else
					<div>Нет контактов.</div>
				@endif
			</div>
		</main>
		<footer>
			<fieldset>
				<legend>Документы</legend>
				@if(count($documents) > 0)
					<div class="client-documents">
						@foreach($documents as $document)
							<dl>
								<dt>
									<a href="{!! $document->url !!}">{!! $document->title !!}</a>
								</dt>
								<dd>Загружен: {!! $document->created_at !!}</dd>
								@if($document->date)
									<dd>Дата документа: {!! $document->date !!}</dd>
								@endif
							</dl>
						@endforeach
					</div>
					{!! $documents_pager !!}
				@else
					<div>Нет документов.</div>
				@endif
			</fieldset>
		</footer>
	</section>
	@endspaceless
@endsection
