<?php
/**
 * @var $client App\Model\Client\Client
 * @var $active string
 */
?>

@spaceless
<ul class="tabs">
  @php
    $tasks_route = route('client.tasks', ['client' => $client->id]);
    $templates_route = route('client.task_templates', ['client' => $client->id]);
  @endphp
  <li class="tab-button @if('tasks' === $active) active @endif">
    <a href="{{ $tasks_route }}"><span class="icon icon-task"></span>Задачи</a>
  </li>
  <li class="tab-button @if('templates' === $active) active @endif">
    <a href="{{ $templates_route }}"><span class="icon icon-template"></span>Шаблоны</a>
  </li>
</ul>
@endspaceless
