@extends('layouts.app')
@section('page_title', 'Шаблоны задач')

@push('scripts')
  <script src="{{ mix('js/pages/client/task_templates.js') }}"></script>
@endpush

@section('content')
  @component('client.tasks.tabs', ['client' => $client, 'active' => 'templates'])@endcomponent
  <task-templates
    :client='@json($client)'
    :client-sites='@json($client->sites)'
    :placeholders='@json($placeholders['placeholders'])'
    :modifiers='@json($placeholders['modifiers'])'
    :examples='@json($placeholders['examples'])'
    :current-templates='@json($templates)'
  ></task-templates>
@stop
