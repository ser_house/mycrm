@extends('layouts.app')
@section('page_title', 'Задачи клиента ' . $client->name)

@push('scripts')
  <script src="{{ mix('js/pages/task/index.js') }}"></script>
@endpush

@section('content')
  @spaceless
  @component('client.tasks.tabs', ['client' => $client, 'active' => 'tasks'])@endcomponent
  <section class="tab-content">
    <header>
      <h1>Задачи клиента "{{$client->name}}"</h1>
      @if($client->is_selectable)
        <div class="actions">
          <a class="btn btn-primary" href="{{ route('client.task.create', ['client' => $client->id]) }}">Добавить</a>
        </div>
      @endif
    </header>
    <main class="client-tasks">
      @if(count($tasks) > 0)
        <tasks :init-tasks='@json($tasks)'></tasks>
        {!! $pager !!}
      @else
        <div>Нет задач.</div>
      @endif
    </main>
  </section>
  @endspaceless
@endsection
