<?php
/**
 * @var $client \App\Model\Client\Client
 * @var $active string|null
 */
$active = !empty($active) ? $active : '';
?>
@if (!empty($client))
	<section>
		<header>
			<h3>Клиент {{ $client->name }}</h3>
		</header>
		<main>
			@component('client.actions', ['client' => $client, 'active' => $active])@endcomponent
		</main>
	</section>
@endif
