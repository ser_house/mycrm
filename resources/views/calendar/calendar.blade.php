<?php
/**
 * @var $years int[]
 * @var $data \Application\Calendar\Month[]
 * @var $moneyFormatter \Application\Infrastructure\Formatter\Money
 * @var $year int
 */
?>
@if(count($years) > 1)
  <form id="filter-form" class="filter-form form-inline" method="get" enctype="multipart/form-data" autocomplete="off">
    <div class="year-wrapper form-group">
      <label>Год</label>
      <div class="form-inline">
        @foreach($years as $year_item)
          <label><input type="radio" class="radio" name="year" value="{!! $year_item !!}" @if($year_item === $year) checked @endif>{!! $year_item !!}</label>
        @endforeach
      </div>
    </div>
    <div class="actions">
      <button type="submit" class="btn btn-info">Применить</button>
    </div>
  </form>
@else
  Год {!! $year !!}
@endif
<div class="calendar">
  @foreach($data as $month)
    <table class="month">
      <thead>
      <tr>
        <td colspan="7" class="month-name">{!! $month->name !!}</td>
      </tr>
      <tr>
        @foreach($month->getWeekdayNames() as $weekdayName)
          <td class="weekday-name">{!! $weekdayName !!}</td>
        @endforeach
      </tr>
      </thead>
      <tbody>
      @foreach($month->getWeeks() as $week)
        <tr class="week">
          @foreach($week as $day)
            @if($day)
              <td class="day{!! $day->cssClasses() !!}" @if($amount_items = $day->getAmountItems())data-tooltip="@foreach($amount_items as $amountItem)<div>{!! $amountItem->title !!}: {!! $moneyFormatter->format($amountItem->amount) !!}</div>@endforeach"@endif>{!! $day->day !!}</td>
            @else
              <td class="day-empty"></td>
            @endif
          @endforeach
        </tr>
      @endforeach
      </tbody>
    </table>
  @endforeach
</div>
