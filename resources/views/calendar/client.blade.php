<?php
/**
 * @var $client \App\Model\Client\Client
 * @var $years int[]
 * @var $data \Application\Calendar\Month[]
 * @var $moneyFormatter \Application\Infrastructure\Formatter\Money
 * @var $year int
 */
?>
@extends('layouts.app')
@section('page_title', 'Календарь клиента "' . $client->name . '"')

@push('scripts')
  <script src="{{ mix('js/utils/tooltip.js') }}"></script>
@endpush

@push('styles')
  <link href="{{ mix('css/calendar.css') }}" rel="stylesheet">
@endpush

@section('content')
  @spaceless
  <h1>Календарь клиента "{!! $client->name !!}"</h1>
  @component('calendar.calendar', ['data' => $data, 'moneyFormatter' => $moneyFormatter, 'years' => $years, 'year' => $year])@endcomponent
  @endspaceless
@endsection
