<?php
/**
 * @var $client \App\Model\Client\Client
 * @var $years int[]
 * @var $data \Application\Calendar\Month[]
 * @var $moneyFormatter \Application\Infrastructure\Formatter\Money
 * @var $year int
 */
?>
@extends('layouts.app')
@section('page_title', 'Календарь выходных клиента "' . $client->name . '"')

@push('scripts')
  <script src="{{ mix('js/pages/holidays.js') }}"></script>
@endpush

@push('styles')
  <link href="{{ mix('css/calendar.css') }}" rel="stylesheet">
@endpush

@section('content')
  @spaceless
  <h1>Календарь выходных клиента "{!! $client->name !!}"</h1>
  <form id="filter-form" class="filter-form form-inline" method="get" enctype="multipart/form-data" autocomplete="off">
    <div class="year-wrapper form-group">
      <label>Год</label>
      <div class="form-inline">
        @foreach($years as $year_item)
          <label><input type="radio" class="radio" name="year" value="{!! $year_item !!}" @if($year_item === $year) checked @endif>{!! $year_item !!}</label>
        @endforeach
      </div>
    </div>
    <div class="actions">
      <button type="submit" class="btn btn-info">Применить</button>
    </div>
  </form>
  <form method="post" action="{{ route('client.holidays.save', ['client' => $client->id]) }}">
    @csrf
    <input id="year" type="text" name="year" value="{!! $year !!}" hidden>
    <div id="holidays" class="calendar">
      @foreach($data as $month)
        <table class="month">
          <thead>
          <tr>
            <td colspan="7" class="month-name">{!! $month->name !!}</td>
          </tr>
          <tr>
            @foreach($month->getWeekdayNames() as $weekdayName)
              <td class="weekday-name">{!! $weekdayName !!}</td>
            @endforeach
          </tr>
          </thead>
          <tbody>
          @foreach($month->getWeeks() as $week)
            <tr class="week">
              @foreach($week as $day)
                @if($day)
                  <td class="day{!! $day->cssClasses() !!}" data-date="{!! $day->getDateString() !!}">{!! $day->day !!}<input type="checkbox" name="date[{!! $day->getDateString() !!}]" value="1" hidden @if($day->isHoliday() || $day->isOfficialHoliday())checked @endif></td>
                @else
                  <td class="day-empty"></td>
                @endif
              @endforeach
            </tr>
          @endforeach
          </tbody>
        </table>
      @endforeach
    </div>

    <div class="calendar-actions form-actions">
      <label>Действия:</label>
      <div class="form-group-sm checkbox">
        <label for="check-all-weekends"><input id="check-all-weekends" class="checkbox" type="checkbox">отметить все выходные</label>
      </div>
      <div class="form-group-sm checkbox">
        <label for="check-all-official-holidays"><input id="check-all-official-holidays" class="checkbox" type="checkbox">отметить все праздники</label>
      </div>

      <div class="actions">
        <button type="submit" class="btn btn-success">Сохранить</button>
      </div>
    </div>
  </form>
  @endspaceless
@endsection
