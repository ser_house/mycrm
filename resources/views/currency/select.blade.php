<select class="form-control currency" name="@isset($name){!! $name !!}@else{!! 'currency' !!}@endisset">
	@foreach($currencies as $code => $title)
		<option value="{!! $code !!}"
			@isset($default)
			@if($code == $default) selected @endif>
			@endisset
			{!! $title !!}</option>
	@endforeach
</select>
