<div class="radio currency">
	@foreach($currencies as $code => $title)
		<label><input type="radio" value="{!! $code !!}" name="@isset($name){!! $name !!}@else{!! 'currency' !!}@endisset"@isset($default)@if($code == $default) checked @endif @endisset>{!! $title !!}</label>
	@endforeach
</div>
