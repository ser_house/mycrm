<?php
/**
 * @var $taskView \Application\Task\View\Full
 * @var $status_history array
 */
?>
@extends('layouts.app')
@section('page_title', $taskView->client->title . ': ' . $taskView->title)

@section('content')
  <header>
    <h1>{{$taskView->title}}</h1>
    <div class="actions">
      <a class="btn btn-success" href="{{route('client.task.create', ['client' => $taskView->client->id])}}">Добавить ещё задачу клиенту</a>
    </div>
  </header>
  @component('task.view', ['taskView' => $taskView])@endcomponent
@endsection
@section('sidebar')
  @parent
  @component('task.blocks.actions', ['client_id' => $taskView->client->id, 'task_id' => $taskView->id, 'active' => 'show'])@endcomponent
  @component('task.blocks.history', ['task_id' => $taskView->id, 'history' => $status_history])@endcomponent
@endsection
