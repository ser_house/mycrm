<?php
/**
 * @var $history array
 */
?>
<section class="block task-history">
  <header><h3>История</h3></header>
  <main>
    <ul class="inline-list">
      @foreach($history as $item)
        <li>
          <div class="state-time">{!! $item['time'] !!}</div>
          <div class="task-state {!! $item['state']->cssClass() !!}">{!! $item['state']->label() !!}</div>
        </li>
      @endforeach
    </ul>
  </main>
</section>
