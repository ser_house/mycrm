<?php
/**
 * @var $client_id int
 * @var $task_id int
 */
$route_params = ['client' => $client_id, 'task' => $task_id]
?>
@spaceless
<section>
	<header><h3>Действия</h3></header>
	<main>
		<ul class="task-actions actions menu-vert">
			<li>
				<a class="list-group-item @if('show' === $active)active @endif"
					href="{{route('client.task.show', $route_params)}}"><span class="icon icon-task"></span>Просмотр</a>
			</li>
			<li>
				<a class="list-group-item @if('edit' === $active)active @endif"
				href="{{route('client.task.edit', $route_params)}}"><span class="icon icon-edit"></span>Изменить</a>
			</li>
{{--			<li>--}}
{{--				<a class="list-group-item @if('remove' === $active)active @endif"--}}
{{--					href="{{route('client.task.remove', $route_params)}}"><span class="icon icon-delete"></span>Удалить</a>--}}
{{--			</li>--}}
		</ul>
	</main>
</section>
@endspaceless
