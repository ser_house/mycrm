@extends('layouts.app')
@section('page_title', 'Новая задача')

@push('scripts')
  <script src="{{ mix('js/pages/task/form.js') }}"></script>
@endpush

@section('content')
  <h1>Новая задача</h1>
  <task-form
    :init-task='@json($task)'
    :init-sub-tasks='@json($sub_tasks)'
    :init-payments='@json([])'
    :client-sites='@json($sites)'
    :client-task-templates='@json($task_templates)'
    :states='@json(\Application\Task\State::cases())'
    :accounts='@json($accounts)'
    csrf='@csrf'
  ></task-form>
@endsection
