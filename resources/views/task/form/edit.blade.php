@extends('layouts.app')
@section('page_title', 'Редактирование ' . $task->title)

@push('scripts')
  <script src="{{ mix('js/pages/task/form.js') }}"></script>
@endpush

@section('content')
  <h1>Редактирование задачи "{{$task->title}}"</h1>
  <task-form
    :is-new="false"
    :init-task='@json($task)'
    :init-sub-tasks='@json($sub_tasks)'
    :init-payments='@json($payments)'
    :client-sites='@json($sites)'
    :client-task-templates='@json([])'
    :states='@json(\Application\Task\State::cases())'
    :accounts='@json($accounts)'
    csrf='@csrf'
  ></task-form>
@endsection
@section('sidebar')
  @parent
  @component('task.blocks.actions', ['client_id' => $task->client_id, 'task_id' => $task->id, 'active' => 'edit'])@endcomponent
@endsection
