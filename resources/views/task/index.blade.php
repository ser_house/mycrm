@extends('layouts.app')
@section('page_title', 'Задачи')

@push('scripts')
  <script src="{{ mix('js/pages/task/index.js') }}"></script>
@endpush

@section('content')
  @spaceless
  <header><h1>Задачи</h1></header>
  @if(count($tasks) > 0)
    <tasks :init-tasks='@json($tasks)'></tasks>
    {!! $pager !!}
  @else
    <div>Нет задач.</div>
  @endif
  @endspaceless
@endsection
