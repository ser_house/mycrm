<?php
/**
 * @var $taskView \Application\Task\View\Full
 * @var $budget \Application\Money\Amount
 */
?>
<div class="task-quick-show">
  <div class="inline-wrapper">
    <label>Статус:</label>
    <div class="task-state {!! $taskView->state->cssClass() !!}">{!! $taskView->state->label() !!}</div>
  </div>
  <div class="inline-wrapper">
    <label>Клиент:</label>
    <div class="client-name"><a href="{{route('client.show', ['client' => $taskView->client->id])}}">{{$taskView->client->title}}</a></div>
  </div>
  @if(!empty($task->site_url))
    <div class="inline-wrapper">
      <label>Сайт:</label>
      <div class="site"><a href="{!! $taskView->site_url !!}">{{$taskView->site_url}}</a></div>
    </div>
  @endif
</div>
<div class="task-data">
  @if($taskView->launched_at)
    <div class="inline-wrapper"><label>Начата:</label>{{$taskView->launched_at}}</div>
  @endif
  @if($taskView->completed_at)
    <div class="inline-wrapper"><label>Завершена:</label>{{$taskView->completed_at}}</div>
  @endif
  <div class="inline-wrapper">
    <label>Бюджет:</label><span class="amount">{!! $taskView->budget !!}</span>
  </div>
  @if($taskView->hours)
    <div class="inline-wrapper"><label>Часы:</label>{{$taskView->hours}}</div>
  @endif
</div>
@if(!empty($taskView->description))
  <div class="task-description">
    <label>Описание:</label>
    <div class="description">{!! nl2br(e($taskView->description), false) !!}</div>
  </div>
@endif
@if($taskView->sub_tasks)
  <fieldset>
    <legend>Подзадачи</legend>
    <ul class="sub-tasks">
      @foreach ($taskView->sub_tasks as $subTaskView)
        <li>
          <dl>
            <dt>{{$subTaskView->title}}</dt>
            <dd>Бюджет: {{ $subTaskView->amount }}</dd>
            @if(!empty($subTaskView->hours))
              <dd>Часы: {{ $subTaskView->hours }}</dd>
            @endif
          </dl>
        </li>
      @endforeach
    </ul>
  </fieldset>
@endif

@if($taskView->payments)
  <fieldset>
    <legend>Оплаты</legend>
    <ul class="payments">
      @foreach ($taskView->payments as $payment)
        <li class="payment">
          <div>
            <span class="payment-date">{{$payment->created_at}}</span> <span class="amount">{{ $payment->amount }}</span>
          </div>
          @if($payment->note)
            <div class="payment-note">
              {{$payment->note}}
            </div>
          @endif
        </li>
      @endforeach
    </ul>
  </fieldset>
@endif
