<?php
/**
 * @var \App\Model\Task\Task[] $tasks
 */
?>

<div class="tasks">
  @foreach($tasks as $task)
    <task-short-card :task='@json($task)'></task-short-card>
  @endforeach
</div>
{!! $pager !!}
