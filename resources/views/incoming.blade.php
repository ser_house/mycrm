<?php
/**
 * @var $items \Application\Incoming\IncomeTreeItem[]
 */
?>
@extends('layouts.app')
@section('page_title', 'Доходы')

@push('scripts')
  <script src="{{ mix('js/pages/incoming.js') }}"></script>
@endpush

@section('content')
  <h1>Доходы</h1>
  @if($items)
    <tree :data='@json($items)'></tree>
  @else
    Нет доходов.
  @endif
@endsection
