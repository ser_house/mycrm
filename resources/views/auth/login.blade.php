@extends('layouts.app')

@section('content')
  <section class="login">
    <header>Login</header>
    <main>
      <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

        <div class="form-group @error('email')has-error @enderror">
          <label for="email" class="control-label">E-Mail Address</label>
          <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
          @field_error('email')
        </div>

        <div class="form-group @error('password')has-error @enderror">
          <label for="password" class="control-label">Password</label>
          <input id="password" type="password" class="form-control" name="password" required>
          @field_error('password')
        </div>

        <div class="form-group">
          <label>
            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
          </label>
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-primary">Login</button>
          <a class="btn btn-link" href="{{ route('password.request') }}">
            Forgot Your Password?
          </a>
        </div>
      </form>
    </main>
  </section>
@endsection
