@extends('layouts.app')

@section('content')
  <section class="register">
    <header>Register</header>
    <main>
      <form class="form-horizontal" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}

        <div class="form-group @error('name')has-error @enderror">
          <label for="name" class="control-label">Name</label>
          <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
          @field_error('name')
        </div>

        <div class="form-group @error('email')has-error @enderror">
          <label for="email" class="control-label">E-Mail Address</label>
          <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
          @field_error('email')
        </div>

        <div class="form-group @error('password')has-error @enderror">
          <label for="password" class="control-label">Password</label>
          <input id="password" type="password" class="form-control" name="password" required>
          @field_error('password')
        </div>

        <div class="form-group">
          <label for="password-confirm" class="control-label">Confirm Password</label>
          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
          @field_error('password_confirmation')
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-primary">Register</button>
        </div>
      </form>
    </main>
  </section>
@endsection
