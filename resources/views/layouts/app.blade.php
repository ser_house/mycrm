<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('page_title', config('app.name', 'Laravel'))</title>

  <!-- Styles -->
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
  @stack('styles')
</head>
<body>
<nav class="navbar">
  @component('navbar')@endcomponent
</nav>
<div id="app" class="container">
  @if(!request()->is('*accounts') && !request()->is('login') && !request()->is('register'))
    <section class="content with-sidebar">
      @component('breadcrumbs')@endcomponent
      @component('flash')@endcomponent
      @yield('content')
    </section>
    <aside id="sidebar" class="sidebar">
      @yield('sidebar')
      @if(!empty($client))
        @component('client.blocks.actions', ['client' => $client, 'active' => $action])@endcomponent
      @endif
      @if(isset($active_clients))
        @component('blocks.active_clients', ['clients' => $active_clients])@endcomponent
      @endif
    </aside>
  @else
    <section class="content">
      @component('breadcrumbs')@endcomponent
      @component('flash')@endcomponent
      @yield('content')
    </section>
  @endif
</div>
@routes

<!-- Scripts -->
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ mix('js/utils/collapsible.js') }}"></script>
<script src="{{ mix('js/utils/menu.js') }}"></script>
@stack('scripts')
<script>
  const app = new Vue({
    el: '#app',
  });
</script>
</body>
</html>
