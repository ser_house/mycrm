<section>
  <header><h3>Разделы</h3></header>
  <main>
    <ul class="actions">
      <li><a class="@if('caches' === $active)active @endif" href="{{route('admin.caches')}}">Кэши</a></li>
      <li class="collapsible">
        <label class="toggle-link">Стили CSS<span class="icon icon-right toggle-icon"></span></label>
        <ul class="toggle-content">
          <li>
            <a class="@if('base_styles' === $active)active @endif" href="{{route('admin.styles.base')}}">Базовые</a>
          </li>
          <li>
            <a class="@if('form_styles' === $active)active @endif" href="{{ route('admin.styles.form') }}">Форма</a>
          </li>
          <li>
            <a class="@if('color_styles' === $active)active @endif" href="{{ route('admin.styles.color') }}">Цвет</a>
          <li>
            <a class="@if('icons_styles' === $active)active @endif" href="{{ route('admin.styles.icons') }}">Иконки</a>
          </li>
        </ul>
      </li>
    </ul>
  </main>
</section>
