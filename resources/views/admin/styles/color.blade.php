@extends('layouts.app')
@section('page_title', 'Цвет CSS')

@push('scripts')
  <script src="{{ mix('js/pages/admin/colors.js') }}"></script>
@endpush

@section('content')
  @component('admin.styles.tabs', ['active' => 'color'])@endcomponent
  <section class="tab-content">
    <header><h1>Цвет</h1></header>
    <main>
      <div class="alert alert-success">Alert success. <a href="/">link</a></div>
      <div class="alert alert-warning">Alert warning. <a href="/">link</a></div>
      <div class="alert alert-danger">Alert danger. <a href="/">link</a></div>
      <table id="colors-table">
        <thead>
        <tr>
          <th>Класс</th>
          <th>Цвет</th>
        </tr>
        </thead>
        <tbody></tbody>
      </table>
    </main>
  </section>
@stop

@section('sidebar')
  @component('admin.menu_block', ['active' => 'color_styles'])@endcomponent
  @parent
@endsection
