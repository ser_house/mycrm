@extends('layouts.app')
@section('page_title', 'Базовый CSS')

@section('content')
  @component('admin.styles.tabs')@endcomponent
  <section class="tab-content">
    <header><h1>Базовые стили</h1></header>
    <main>
      <!------------------------------------------------------------>
      <h1 id="headings">Headings</h1>
      <h1>Heading 1</h1>
      <h2>Heading 2</h2>
      <h3>Heading 3</h3>
      <h4>Heading 4</h4>
      <h5>Heading 5</h5>
      <h6>Heading 6</h6>
      <p class="top"><a href="#top">[top]</a></p>
      <!------------------------------------------------------------>
      <hr/>

      <h1 id="paragraphs">Paragraph</h1>
      <img style="width:276px;height:110px;float:right;" src="http://www.google.co.uk/intl/en_uk/images/logo.gif" alt="Google Logo"/>
      <p>Lorem ipsum dolor sit amet, <a href="#" title="test link">test link</a> adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet
        at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, <strong>bold</strong> mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla
        nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.</p>
      <p>Lorem ipsum dolor sit amet, <em>emphasis</em> consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt
        nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante.
        Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.</p>
      <p class="top"><a href="#top">[top]</a></p>
      <!------------------------------------------------------------>
      <hr/>

      <h1 id="lists">Lists</h1>
      <h3>Definition List</h3>
      <dl>
        <dt>List Item 1</dt>
        <dd>List item 1 description.</dd>
        <dt>List Item 2</dt>
        <dd>List item 2 description.</dd>
        <dt>List Item 3</dt>
        <dd>List item 3 description.</dd>
      </dl>
      <h3>Ordered List</h3>
      <ol>
        <li>List Item 1</li>
        <li>List Item 2</li>
        <li>List Item 3</li>
      </ol>
      <h3>Unordered List</h3>
      <ul>
        <li>List Item 1</li>
        <li>List Item 2</li>
        <li>List Item 3</li>
      </ul>
      <p class="top"><a href="#top">[top]</a></p>
      <!------------------------------------------------------------>
      <hr/>

      <h1>Tables</h1>
      <table>
        <caption>With caption</caption>
        <thead>
        <tr>
          <th>Heading 1</th>
          <th>Heading 2</th>
          <th>Heading 3</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>Cell 1-1</td>
          <td>Cell 2-1</td>
          <td>Cell 3-1</td>
        </tr>
        <tr>
          <td>Cell 1-2</td>
          <td>Cell 2-2</td>
          <td>Cell 3-2</td>
        </tr>
        <tr>
          <td>Cell 1-3</td>
          <td>Cell 2-3</td>
          <td>Cell 3-3</td>
        </tr>
        </tbody>
      </table>
      <table>
        <thead>
        <tr>
          <th>Heading 1</th>
          <th>Heading 2</th>
          <th>Heading 3</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>Cell 1-1</td>
          <td>Cell 2-1</td>
          <td>Cell 3-1</td>
        </tr>
        <tr>
          <td>Cell 1-2</td>
          <td>Cell 2-2</td>
          <td>Cell 3-2</td>
        </tr>
        <tr>
          <td>Cell 1-3</td>
          <td>Cell 2-3</td>
          <td>Cell 3-3</td>
        </tr>
        </tbody>
      </table>
      <p class="top"><a href="#top">[top]</a></p>
      <!------------------------------------------------------------>
      <hr/>

      <h1 id="misc">Misc Stuff - abbr, acronym, pre, code, sub, sup, etc.</h1>
      <p>Lorem <sup>superscript</sup> dolor <sub>subscript</sub> amet, consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. <cite>cite</cite>. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi,
        imperdiet
        at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, <code>inline code block</code> fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla
        nonummy.
        Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus. <abbr title="Avenue">AVE</abbr></p>
      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida
        vehicula,
        nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat
        at,
        commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus. <abbr title="Avenue">AVE</abbr></p>
      <blockquote>"This stylesheet is going to help so freaking much." -Blockquote</blockquote>
      <p class="top"><a href="#top">[top]</a></p>
      <!------------------------------------------------------------>
    </main>
  </section>
@stop

@section('sidebar')
  @component('admin.menu_block', ['active' => 'base_styles'])@endcomponent
  @parent
@endsection
