@extends('layouts.app')
@section('page_title', 'Форма CSS')

@section('content')
  @component('admin.styles.tabs', ['active' => 'form'])@endcomponent
  <section class="tab-content">
    <header><h1>Form Elements</h1></header>
    <main>
      <form>
        <div class="form-group">
          <label for="text-field-1">Text Field</label>
          <input class="form-control" type="text" id="text-field-1" name="text-field-1"/>
          <div class="help">Help text.</div>
        </div>
        <div class="form-group">
          <label for="text-area-1">Text Area</label>
          <textarea class="form-control" id="text-area-1" name="text-area-1"></textarea>
          <div class="help">Help text.</div>
        </div>
        <div class="form-group required">
          <label>Дата</label>
          <input class="form-control" type="date" name="date" required>
          <div class="help">type date, form-control, required.</div>
        </div>
        <div class="form-group required">
          <label>Дата</label>
          <input type="date" name="date" required>
          <div class="help">type date, required.</div>
        </div>
        <div class="form-group required">
          <label>Number</label>
          <input class="form-control" type="number" name="number" required>
          <div class="help">type number, form-control, required.</div>
        </div>
        <div class="form-group required">
          <label>Number</label>
          <input type="number" name="number" required>
          <div class="help">type number, required.</div>
        </div>
        <div class="form-group">
          <label for="select-element-1">Select Element</label>
          <select class="form-control" id="select-element-1" name="select-element-1">
            <option value="1">Option 1</option>
            <option value="2">Option 2</option>
            <option value="3">Option 3</option>
          </select>
          <div class="help">Help text.</div>
        </div>
        <div class="form-group">
          <label for="select-element-1">Select Element with groups</label>
          <select class="form-control" id="select-element-1" name="select-element-1">
            <optgroup label="Option Group 1">
              <option value="1">Option 1</option>
              <option value="2">Option 2</option>
              <option value="3">Option 3</option>
            </optgroup>
            <optgroup label="Option Group 2">
              <option value="1">Option 1</option>
              <option value="2">Option 2</option>
              <option value="3">Option 3</option>
            </optgroup>
          </select>
          <div class="help">Help text.</div>
        </div>
        <div class="form-group">
          <label>Radio Buttons</label>
          <label><input type="radio" name="radio-1">Radio 1</label>
          <label><input type="radio" name="radio-1" checked>Radio 2</label>
          <label><input type="radio" name="radio-1">Radio 3</label>
          <div class="help">Help text.</div>
        </div>
        <div class="form-group">
          <label>Radio Button before<input type="radio" name="radio-2"/></label>
          <label><input type="radio" name="radio-2"/>Radio Button after</label>
          <div class="help">Help text.</div>
        </div>
        <div class="form-group">
          <label>Checkboxes</label>
          <label><input type="checkbox" name="checkbox-1">Checkbox 1</label>
          <label><input type="checkbox" name="checkbox-2">Checkbox 2</label>
          <label><input type="checkbox" name="checkbox-3">Checkbox 3</label>
          <div class="help">Help text.</div>
        </div>
        <div class="form-group">
          <label>Checkbox before<input type="checkbox" name="checkbox-4"/></label>
          <label><input type="checkbox" name="checkbox-4"/>Checkbox after</label>
          <div class="help">Help text.</div>
        </div>
        <div class="form-group">
          <label for="password-field-1">Password Field</label>
          <input class="form-control" type="password" id="password-field-1" name="password-field-1"/>
          <div class="help">Help text.</div>
        </div>
        <div class="form-group">
          <label for="file-input-1">File Input</label>
          <input class="form-control" type="file" id="file-input-1" name="file-input-1"/>
          <div class="help">Help text.</div>
        </div>
        <div class="form-group">
          <label>Buttons as input</label>
          <input type="submit" value="Submit"/>
          <input type="reset" value="Reset"/>
          <input type="button" value="Button"/>
        </div>
        <div class="form-group">
          <label>Buttons as button styled</label>
          <button type="button" class="btn btn-primary">btn-primary</button>
          <button type="button" class="btn btn-secondary">btn-secondary</button>
          <button type="button" class="btn btn-success">btn-success</button>
          <button type="button" class="btn btn-info">btn-info</button>
          <button type="button" class="btn btn-danger">btn-danger</button>
          <button type="button" class="btn btn-warning">btn-warning</button>
          <button type="button" class="btn btn-link">btn-link</button>
        </div>
      </form>
      <p class="top"><a href="#top">[top]</a></p>
      <!------------------------------------------------------------>
    </main>
  </section>
@stop

@section('sidebar')
  @component('admin.menu_block', ['active' => 'form_styles'])@endcomponent
  @parent
@endsection
