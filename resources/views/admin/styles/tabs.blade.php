<?php
$active = $active ?? '';
?>
@spaceless
<ul class="tabs">
  <li class="tab-button @if('' === $active) active @endif">
    <a href="{{ route('admin.styles.base') }}">Базовые</a>
  </li>
  <li class="tab-button @if('form' === $active) active @endif">
    <a href="{{ route('admin.styles.form') }}">Форма</a>
  </li>
  <li class="tab-button @if('color' === $active) active @endif">
    <a href="{{ route('admin.styles.color') }}">Цвет</a>
  </li>
  <li class="tab-button @if('icons' === $active) active @endif">
    <a href="{{ route('admin.styles.icons') }}">Иконки</a>
  </li>
</ul>
@endspaceless
