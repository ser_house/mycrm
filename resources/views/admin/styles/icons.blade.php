<?php
/**
 * @var $icon_classes array
 */
?>
@extends('layouts.app')
@section('page_title', 'Иконки CSS')
@push('styles')
  <style>
    .styles-icons {
      font-size: 1.5em;
    }

    .styles-icons .icon {
      margin-right: 1em;
    }

    .styles-icons div {
      padding: 0.2em;
    }
  </style>
@endpush
@section('content')
  @component('admin.styles.tabs', ['active' => 'icons'])@endcomponent
  <section class="tab-content">
    <header><h1>Иконки</h1></header>
    <main class="styles-icons">
      @foreach($icon_classes as $icon)
        <div><span class="icon {!! $icon !!}"></span>{!! $icon !!}</div>
      @endforeach
    </main>
  </section>
@stop

@section('sidebar')
  @component('admin.menu_block', ['active' => 'icons_styles'])@endcomponent
  @parent
@endsection
