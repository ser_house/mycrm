@extends('layouts.app')
@section('page_title', 'Админ: кэши')

@section('content')
    <h1>Кэши</h1>
    <form id="caches-form" method="post" action="{{route('admin.caches_reset')}}">
        {{csrf_field()}}
        <div class="row">
            <div class="form-group">
                <label>Пользователь:</label>
                <select class="form-control" name="user_id">
                    <option value="">- Все -</option>
                    @foreach($users as $user)
                        <option value="{!! $user->id !!}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Кэши:</label>
							<div class="checkbox">
								<label><input type="checkbox" name="mode[by_years]" value="1">По годам</label>
							</div>
						<div class="checkbox">
								<label><input type="checkbox" name="mode[by_current_year_months]" value="1">По месяцам текущего года</label>
							</div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <button type="submit" class="btn btn-success">Сбросить</button>
            </div>
        </div>
    </form>
@endsection

@section('sidebar')
	@component('admin.menu_block', ['active' => 'caches'])@endcomponent
  @parent
@endsection
