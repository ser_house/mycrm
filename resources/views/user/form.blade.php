@extends('layouts.app')
@section('page_title', 'Профиль')

@push('scripts')
  <script src="{{ mix('js/pages/user/form.js') }}"></script>
@endpush

@section('content')
  <h1>Профиль</h1>
  <form method="post" action="{{route('user.profile.save', ['user' => $id])}}">
    {{csrf_field()}}
    <div class="form-group">
      <label>Name:</label>
      <input class="form-control" type="text" name="name" value="{!! $name !!}">
    </div>
    <div class="form-group">
      <label>Email:</label>
      <input class="form-control" type="text" name="email" value="{!! $email !!}">
    </div>
    <div class="form-group">
      <label for="tz">Временная зона</label>
      <select class="form-control" id="tz" name="tz">
        @foreach($timezones as $key => $title)
          <option value="{!! $key !!}" @if($current_tz == $key)selected @endif>{!! $title !!}</option>
        @endforeach
      </select>
    </div>
    <user-currencies class="form-group"
                     :init-currencies='@json($user_currencies)'
                     :all-currencies='@json($currencies)'>
      <label>Используемые валюты</label>
    </user-currencies>
    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
  </form>
@endsection
