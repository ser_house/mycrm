@extends('layouts.app')
@section('page_title', 'Счета')
@push('scripts')
  <script src="{{ mix('js/pages/accounts.js') }}"></script>
@endpush

@section('content')
  <h1>Счета</h1>
  @if(!empty($missing_user_currencies))
    <div class="alert alert-warning">Не настроены валюты, Вы можете их настроить <a href="{{ route('user.profile', ['user' => Auth::user()->getAuthIdentifier()]) }}">на странице профиля.</a></div>
  @endif
  <form id="accounts-form" method="post" action="{{route('accounts.update')}}">
    {{csrf_field()}}
    <my-accounts :init-accounts='@json($accounts)' :types='@json($types)' :currencies='@json($currencies)'></my-accounts>
    <div class="form-actions">
      <button type="submit" class="btn btn-success">Сохранить</button>
    </div>
  </form>
@endsection
