@foreach(['success', 'error', 'warning'] as $type)
  @php($css_class = ('error' === $type) ? 'danger' : $type)
  @if (\Session::has($type))
    <div class="alert alert-{!! $css_class !!}"><p>{!! \Session::get($type) !!}</p></div>
  @endif
@endforeach
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

