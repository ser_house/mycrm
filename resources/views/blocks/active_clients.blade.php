<section>
	<header><h3>Активные клиенты</h3></header>
	<main>
		<ul class="clients">
			@foreach($clients as $client)
				<li class="collapsible">
					<label class="toggle-link collapsed">{{$client->name}}<span class="icon icon-right toggle-icon"></span></label>
					<div class="toggle-content collapsed">
						@component('client.actions', ['client' => $client, 'is_active_clients' => true])@endcomponent
					</div>
				</li>
			@endforeach
		</ul>
	</main>
</section>
