<div class="navbar-header">
  <a class="navbar-brand" href="{{ url('/') }}">
    {{ config('app.name', 'Laravel') }}
  </a>
</div>

<div class="navbar-content">
  <ul class="nav">
    @auth
      <li><a href="{{ route('clients.index') }}">Клиенты</a></li>
      <li><a href="{{ route('accounts') }}">Счета</a></li>
      <li><a href="{{ route('tasks') }}">Задачи</a></li>
      <li>
        <div class="dropdown">
          <span class="dropdown-toggle">Доходы</span>
          <ul class="menu">
            <li><a href="{{ route('incoming') }}">Доходы</a></li>
            <li><a href="{{ route('calendar') }}">Календарь</a></li>
          </ul>
        </div>
      </li>
      <li>
        <div class="dropdown">
          <span class="dropdown-toggle">Админ</span>
          <ul class="menu">
            <li><a href="{{ route('admin.caches') }}">Кэши</a></li>
            <li><a href="{{ route('admin.styles.base') }}">Стили CSS</a></li>
          </ul>
        </div>
      </li>
      <li>
        <div class="dropdown">
          <span class="dropdown-toggle">{{ Auth::user()->name }}</span>
          <ul class="menu">
            <li><a href="{{ route('user.profile', ['user' => Auth::user()->getAuthIdentifier()]) }}">Профиль</a></li>
            <li>
              <a href="{{ route('logout') }}"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Logout
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
          </ul>
        </div>
      </li>
    @else
      <li><a href="{{ route('login') }}">Login</a></li>
      <li><a href="{{ route('register') }}">Register</a></li>
    @endauth
  </ul>
</div>
