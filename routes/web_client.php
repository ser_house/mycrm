<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.11.2019
 * Time: 14:03
 */

use App\Http\Controllers\Client\Controller as ClientController;
use App\Http\Controllers\Client\AddController as ClientAddController;
use App\Http\Controllers\Client\UpdateController as ClientUpdateController;
use App\Http\Controllers\Client\Task\Controller as TaskController;
use App\Http\Controllers\Client\Task\AddController as TaskAddController;
use App\Http\Controllers\Client\Task\UpdateController as TaskUpdateController;
use App\Http\Controllers\Client\Task\FinishController as TaskFinishController;
use App\Http\Controllers\Client\Task\TemplateController as TaskTemplateController;
use App\Http\Controllers\Client\PaymentController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\DocGeneratorController;
use App\Http\Controllers\Client\Document\Controller as DocumentController;
use App\Http\Controllers\Client\Document\SettingsController as DocumentSettingsController;
use App\Http\Controllers\Client\Document\TemplateController as DocumentTemplateController;

Route::middleware(['web', 'auth', 'breadcrumbs'])->group(function () {

  Route::group(['prefix' => 'clients'], function () {
    Route::get('/', [ClientController::class, 'index'])->name('clients.index');
    Route::get('create', [ClientAddController::class, 'form'])->name('client.create');

    Route::prefix('{client}')->middleware('can:client,client')->group(function () {
      Route::get('/', [ClientController::class, 'show'])->name('client.show');
      Route::get('edit', [ClientUpdateController::class, 'form'])->name('client.edit');
//      Route::get('delete', [ClientController::class, 'remove'])->name('client.remove');

      Route::prefix('tasks')->group(function () {
        Route::get('/', [TaskController::class, 'index'])->name('client.tasks');
        Route::get('create', [TaskAddController::class, 'form'])->name('client.task.create');
        Route::get('create-by-template/{template_id}', [TaskAddController::class, 'createByTemplate'])->name('client.tasks.create_by_template');
        Route::prefix('{task}')->group(function () {
          Route::get('/', [TaskController::class, 'show'])->name('client.task.show');
          Route::get('view', [TaskController::class, 'view'])->name('client.task.view');
          Route::get('edit', [TaskUpdateController::class, 'form'])->name('client.task.edit');
        });
      });

      Route::prefix('documents')->group(function () {
        Route::get('/', [DocumentController::class, 'index'])->name('client.documents');
        Route::get('templates', [DocumentTemplateController::class, 'index'])->name('client.documents.templates');
        Route::get('settings', [DocumentSettingsController::class, 'settings'])->name('client.documents.settings');
        Route::get('generator', [DocGeneratorController::class, 'generator'])->name('client.documents.generator');
      });

      Route::get('task-templates', [TaskTemplateController::class, 'index'])->name('client.task_templates');
      Route::get('calendar', [CalendarController::class, 'clientCalendar'])->name('client.calendar');
      Route::get('holidays', [CalendarController::class, 'holidays'])->name('client.holidays');
    });
  });
});

Route::middleware(['web', 'auth'])->group(function () {

  Route::group(['prefix' => 'clients'], function () {
    Route::post('add', [ClientAddController::class, 'add'])->name('clients.add');

    Route::prefix('{client}')->middleware('can:client,client')->group(function () {
      Route::patch('update', [ClientUpdateController::class, 'update'])->name('client.update');
//      Route::get('delete', [ClientController::class, 'remove'])->name('client.remove');

      Route::prefix('tasks')->group(function () {
        Route::post('store', [TaskAddController::class, 'add'])->name('client.task.store');
        Route::prefix('{task}')->group(function () {
          Route::post('finish', [TaskFinishController::class, 'run'])->name('client.task.finish');
          Route::prefix('payments')->group(function () {
            Route::get('/', [PaymentController::class, 'index'])->name('client.task.payments');
            Route::post('save', [PaymentController::class, 'savePayments'])->name('client.task.payments.save');
          });

          Route::patch('update', [TaskUpdateController::class, 'update'])->name('client.task.update');
          Route::patch('update-dates', [TaskUpdateController::class, 'updateDates'])->name('client.task.update_dates');
//          Route::get('delete', TaskRemoveController::class)->name('client.task.remove');
        });
      });

      Route::prefix('documents')->group(function () {
        Route::post('upload', [DocumentController::class, 'upload'])->name('client.documents.upload');
        Route::post('update', [DocumentController::class, 'update'])->name('client.document.update');

        Route::post('templates-upload', [DocumentTemplateController::class, 'upload'])->name('client.documents.templates.upload');
        Route::post('template-update', [DocumentTemplateController::class, 'update'])->name('client.documents.template.update');

        Route::post('settings-save', [DocumentSettingsController::class, 'settingsSave'])->name('client.documents.settings.save');

        Route::get('data-to-generator-finished', [DocGeneratorController::class, 'dataToGeneratorFinished'])->name('client.documents.data_to_generator_finished');
        Route::get('data-to-generator-closed', [DocGeneratorController::class, 'dataToGeneratorClosed'])->name('client.documents.data_to_generator_closed');

        Route::post('generate', [DocGeneratorController::class, 'generate'])->name('client.documents.generate');
      });

      Route::post('task-templates/add', [TaskTemplateController::class, 'add'])->name('client.task_templates.add');

      Route::prefix('task-templates/{id}')->group(function () {
        Route::post('update', [TaskTemplateController::class, 'update'])->name('client.task_template.update');
        Route::delete('remove', [TaskTemplateController::class, 'remove'])->name('client.task_template.remove');
      });

      Route::post('save-holidays', [CalendarController::class, 'saveHolidays'])->name('client.holidays.save');
    });
  });
});
