<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\IncomingController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;


Auth::routes();

Route::middleware(['web', 'auth', 'breadcrumbs'])->group(function () {
  Route::get('/', [HomeController::class, 'index'])->name('home');
  Route::get('tasks', [TaskController::class, 'index'])->name('tasks');

  Route::get('accounts', [AccountController::class, 'index'])->name('accounts');
  Route::post('accounts', [AccountController::class, 'update'])->name('accounts.update');

  Route::delete('files/{file}', [FileController::class, 'delete'])->name('files.delete');

  Route::prefix('incoming')->group(function () {
    Route::get('/', [IncomingController::class, 'index'])->name('incoming');
  });
  Route::get('calendar', [CalendarController::class, 'calendar'])->name('calendar');

  Route::prefix('user')->group(function () {
    Route::get('{user}/profile', [UserController::class, 'form'])->name('user.profile');
    Route::post('{user}/profile', [UserController::class, 'save'])->name('user.profile.save');
  });

});

Route::middleware(['web', 'auth', 'breadcrumbs'])->prefix('admin')->group(function () {
    Route::get('/', [AdminController::class, 'caches'])->name('admin.caches');
    Route::post('caches-reset', [AdminController::class, 'resetCaches'])->name('admin.caches_reset');

    Route::prefix('styles')->group(function() {
      Route::get('/', [AdminController::class, 'styles'])->name('admin.styles.base');
      Route::get('/form', [AdminController::class, 'stylesForm'])->name('admin.styles.form');
      Route::get('/color', [AdminController::class, 'stylesColor'])->name('admin.styles.color');
      Route::get('/icons', [AdminController::class, 'stylesIcons'])->name('admin.styles.icons');
    });
});
