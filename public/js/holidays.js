window.onload = function() {
  document.getElementById('holidays').addEventListener('click', function(event) {
    let target = event.target;
    if ('TD' === target.tagName && target.classList.contains('day')) {
      let checkbox = target.getElementsByTagName('input')[0];
      checkbox.checked = !checkbox.checked;
      if (target.classList.contains('holiday')) {
        target.classList.remove('holiday');
      }
      else {
        target.classList.add('holiday');
      }
    }
  });

  document.getElementById('check-all-weekends').addEventListener('change', function() {
    let days = [];
    if (this.checked) {
      days = document.querySelectorAll('.weekend:not(.holiday)');
    }
    else {
      days = document.querySelectorAll('.weekend.holiday');
    }

    days.forEach((el) => {
      el.click();
    });
  });

  document.getElementById('check-all-official-holidays').addEventListener('change', function() {
    let days = [];
    if (this.checked) {
      days = document.querySelectorAll('.official-holiday:not(.holiday)');
    }
    else {
      days = document.querySelectorAll('.official-holiday.holiday');
    }

    days.forEach((el) => {
      el.click();
    });
  });
};
