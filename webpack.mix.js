let mix = require('laravel-mix');
let path = require('path');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.alias({
  // 'vue$': 'vue/dist/vue.esm.js',
  '@': path.resolve('resources'),
  'ext': path.resolve('node_modules'),
});

mix.override(webpackConfig => {
  webpackConfig.module.rules.push({
    test: /\.ttf$/,
    use: [
      {
        loader: 'ttf-loader',
        options: {
          name: './font/[hash].[ext]',
        },
      },
    ],
  });
});

mix.vue({version: 2});

mix.js('resources/js/app.js', 'public/js');

mix.copy('resources/js/utils/tooltip.js', 'public/js/utils');
mix.copy('resources/js/utils/collapsible.js', 'public/js/utils');
mix.copy('resources/js/utils/menu.js', 'public/js/utils');

mix.copy('resources/js/pages/holidays.js', 'public/js/pages');
mix.js('resources/js/pages/accounts.js', 'public/js/pages');
mix.js('resources/js/pages/incoming.js', 'public/js/pages');

mix.js('resources/js/pages/client/index.js', 'public/js/pages/client/index.js');
mix.js('resources/js/pages/client/form.js', 'public/js/pages/client/form.js');
mix.js('resources/js/pages/client/task_templates.js', 'public/js/pages/client/task_templates.js');
mix.js('resources/js/pages/client/documents.js', 'public/js/pages/client/documents.js');
mix.js('resources/js/pages/client/document_templates.js', 'public/js/pages/client/document_templates.js');
mix.js('resources/js/pages/client/documents_generator.js', 'public/js/pages/client/documents_generator.js');

mix.js('resources/js/pages/task/index.js', 'public/js/pages/task/index.js');
mix.js('resources/js/pages/task/form.js', 'public/js/pages/task/form.js');

mix.js('resources/js/pages/user/form.js', 'public/js/pages/user/form.js');

mix.copy('resources/js/pages/admin/colors.js', 'public/js/pages/admin/colors.js');

mix.js('resources/js/pages/home.js', 'public/js/pages/home.js');

if (!mix.inProduction()) {
  mix.sourceMaps();
}

mix.sass('resources/sass/app.scss', 'public/css');
mix.sass('resources/sass/file.scss', 'public/css');
mix.sass('resources/sass/calendar.scss', 'public/css');
mix.sass('resources/sass/chart.scss', 'public/css');

mix.disableNotifications();
mix.version();
