<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.05.2020
 * Time: 9:34
 */


namespace Application\Infrastructure\Formatter;


class Number {

  /**
   * @param string $input
   *
   * @return float
   */
  public function floatFromInput(string $input): float {
    $value = str_replace([' ', ','], ['', '.'], $input);
    return number_format($value, 2, '.', '');
  }
}
