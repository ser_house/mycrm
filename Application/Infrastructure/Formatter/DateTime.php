<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 31.03.2018
 * Time: 10:34
 */

namespace Application\Infrastructure\Formatter;

use Auth;
use Config;
use DateTimeImmutable;
use IntlDateFormatter;


// http://userguide.icu-project.org/formatparse/datetime

/**
 * Class DateTime
 *
 * @package Application\Infrastructure\Formatter
 */
class DateTime {
  public const MONTH_WHERE_FORM = 0;
  public const MONTH_WHEN_FORM = 1;


  /**
   * DateTime constructor.
   *
   * @param string $locale
   */
  public function __construct(private string $locale = 'ru') {

  }

  /**
   *
   * @return bool
   */
  private function isRu(): bool {
    return ('ru' === $this->locale);
  }

  /**
   * @param IntlDateFormatter $formatter
   */
  private function setFourDigitsYear(IntlDateFormatter $formatter): void {
    // Forcing four-digit years on IntlDateFormatter::SHORT
    // https://gist.github.com/bryanburgers/f375ea3086a0ed029636
    $pattern = $formatter->getPattern();
    $pattern = preg_replace('/(?<!y)yy(?!y)/', 'yyyy', $pattern);
    $formatter->setPattern($pattern);
  }

  /**
   * @param string $locale
   */
  public function setLocale(string $locale): void {
    $this->locale = $locale;
  }

  /**
   *
   * @return string
   */
  public function getFormatterLocale(): string {
    return $this->locale . '_' . strtoupper($this->locale);
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return string
   */
  public function dateShort(DateTimeImmutable $dateTime): string {
    $tz = $this->getTz();
    $formatter = new IntlDateFormatter(
      $this->getFormatterLocale(),
      IntlDateFormatter::SHORT,
      IntlDateFormatter::NONE,
      $tz
    );

    if ($this->isRu()) {
      $this->setFourDigitsYear($formatter);
    }

    return $formatter->format($dateTime);
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return string
   */
  public function dateTimeShort(DateTimeImmutable $dateTime): string {
    $tz = $this->getTz();
    $formatter = new IntlDateFormatter(
      $this->getFormatterLocale(),
      IntlDateFormatter::SHORT,
      IntlDateFormatter::SHORT,
      $tz
    );

    if ($this->isRu()) {
      $this->setFourDigitsYear($formatter);
    }

    return $formatter->format($dateTime);
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return string
   */
  public function dateLong(DateTimeImmutable $dateTime): string {
    $tz = $this->getTz();
    $formatter = new IntlDateFormatter(
      $this->getFormatterLocale(),
      IntlDateFormatter::LONG,
      IntlDateFormatter::NONE,
      $tz,
    );

    return $formatter->format($dateTime);
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return string
   */
  public function dateToDb(DateTimeImmutable $dateTime): string {
    return $dateTime->format('Y-m-d');
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return string
   */
  public function dateTimeToDb(DateTimeImmutable $dateTime): string {
    return $dateTime->format('Y-m-d H:i:s');
  }

  /**
   * @param DateTimeImmutable|null $date
   *
   * @return array
   */
  public function getDateFormats(DateTimeImmutable $date = null): array {
    if (null === $date) {
      $date = new DateTimeImmutable();
    }
    return [
      'd.m.Y' => $this->dateShort($date),
      'd F Y г.' => $this->dateLong($date),
    ];
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return string
   */
  public function month(DateTimeImmutable $dateTime): string {
    $tz = $this->getTz();
    if ($this->isRu()) {
      $formatter = new IntlDateFormatter(
        $this->getFormatterLocale(),
        IntlDateFormatter::LONG,
        IntlDateFormatter::NONE,
        $tz,
        null,
        'LLLL');
    }
    else {
      $formatter = new IntlDateFormatter(
        $this->getFormatterLocale(),
        IntlDateFormatter::LONG,
        IntlDateFormatter::NONE,
        $tz
      );
    }

    return mb_ucfirst($formatter->format($dateTime));
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return string
   */
  public function monthShort(DateTimeImmutable $dateTime): string {
    $tz = $this->getTz();
    if ($this->isRu()) {
      $formatter = new IntlDateFormatter(
        $this->getFormatterLocale(),
        IntlDateFormatter::LONG,
        IntlDateFormatter::NONE,
        $tz,
        null,
        'MMM');
    }
    else {
      $formatter = new IntlDateFormatter(
        $this->getFormatterLocale(),
        IntlDateFormatter::LONG,
        IntlDateFormatter::NONE,
        $tz
      );
    }

    return mb_ucfirst($formatter->format($dateTime));
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param int $form
   *
   * @return string
   */
  public function monthDeclesion(DateTimeImmutable $dateTime, int $form = self::MONTH_WHERE_FORM): string {
    if ($this->isRu()) {
      $declesions = [
        self::MONTH_WHERE_FORM => [
          'январь' => 'январе',
          'февраль' => 'феврале',
          'март' => 'марте',
          'апрель' => 'апреле',
          'май' => 'мае',
          'июнь' => 'июне',
          'июль' => 'июле',
          'август' => 'августе',
          'сентябрь' => 'сентябре',
          'октябрь' => 'октябре',
          'ноябрь' => 'ноябре',
          'декабрь' => 'декабре',
        ],
        self::MONTH_WHEN_FORM => [
          'январь' => 'января',
          'февраль' => 'февраля',
          'март' => 'марта',
          'апрель' => 'апреля',
          'май' => 'мая',
          'июнь' => 'июня',
          'июль' => 'июля',
          'август' => 'августа',
          'сентябрь' => 'сентября',
          'октябрь' => 'октября',
          'ноябрь' => 'ноября',
          'декабрь' => 'декабря',
        ],
      ];
      $month = mb_strtolower($this->month($dateTime));

//setlocale(LC_TIME, 'ru_RU.UTF-8');
//$ru_duedatetime = strftime('%B %d, %Y', $duedatetime);
//$search = ("ь", "Март", "Май", "Август");
//$replace = ("я", "Марта", "Мая", "Августа");
//echo $rudate = str_replace($search, $replace, $ru_duedatetime);

      return $declesions[$form][$month];
    }

    $tz = $this->getTz();
    $formatter = new IntlDateFormatter(
      $this->getFormatterLocale(),
      IntlDateFormatter::NONE,
      IntlDateFormatter::NONE,
      $tz,
    );

    return $formatter->format($dateTime);
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return string
   */
  public function year(DateTimeImmutable $dateTime): string {
    return $dateTime->format('Y');
  }

  /**
   * @param float $value
   *
   * @return string
   */
  public function hours(float $value): string {
    return number_format($value, 1);
  }

  /**
   *
   * @return array|string[]
   */
  public function months(): array {
    return [
      1 => 'Январь',
      2 => 'Февраль',
      3 => 'Март',
      4 => 'Апрель',
      5 => 'Май',
      6 => 'Июнь',
      7 => 'Июль',
      8 => 'Август',
      9 => 'Сентябрь',
      10 => 'Октябрь',
      11 => 'Ноябрь',
      12 => 'Декабрь',
    ];
  }

  /**
   * Получение текущей временной зоны.
   * Делаем это не в конструкторе, а в методах форматирования.
   * Потому что класс инстанцируется рано, раньше, чем мы получаем авторизованного юзера,
   * а методы вызываются позже, когда авторизованный юзер уже есть.
   *
   * @return string
   */
  private function getTz(): string {
    $authUser = Auth::user();
    return $authUser->timezone ?? \Config::get('app.timezone');
  }
}
