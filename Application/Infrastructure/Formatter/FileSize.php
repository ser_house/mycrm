<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.05.2020
 * Time: 3:54
 */


namespace Application\Infrastructure\Formatter;


/**
 * Class FileSize
 *
 * @package Application\Infrastructure\Formatter
 */
class FileSize {

  /**
   * @param int $bytes
   * @param int $decimals
   *
   * @return string
   */
  public function format(int $bytes, int $decimals = 2): string {
    $size = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    $factor = floor(log($bytes, 1024));
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
  }
}
