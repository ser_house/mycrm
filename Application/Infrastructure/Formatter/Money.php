<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.08.2018
 * Time: 07:39
 */

namespace Application\Infrastructure\Formatter;

use Application\Money\Amount;
use NumberFormatter;

/**
 * Class Money
 *
 * @package Application\Infrastructure\Formatter
 */
class Money {

  /**
   * @param Amount $amount
   *
   * @return string
   */
  public function format(Amount $amount): string {
    $currency = $amount->currency;

    $locale = $this->getLocaleFromCurrency($currency);

    $formatter = new NumberFormatter($locale, NumberFormatter::CURRENCY);

    $value = $amount->amount;

    return $formatter->formatCurrency($value, $currency);
  }

  /**
   * @param Amount $amount
   *
   * @return string
   */
  public function formatForDocument(Amount $amount): string {
    $currency = $amount->currency;

    $locale = $this->getLocaleFromCurrency($currency);

    $formatter = new NumberFormatter($locale, NumberFormatter::CURRENCY);
    $formatter->setTextAttribute(NumberFormatter::CURRENCY_CODE, $currency);

    $value = $amount->amount;

    // В документах не выводим дробную часть, если она нулевая.
    if (0 === $value % 100) {
      $formatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 0);
    }

    // Не позволяет избавиться от дробной части.
    // $str = $formatter->formatCurrency($amount, $currency);
    $str = $formatter->format($value);

    // Для документов заменяем символ рубля на более привычный бюрократии "р.",
    // а то бухгалтерия испугается.
    if ('RUB' === $currency) {
      $fmt = new NumberFormatter($locale . "@currency=$currency", NumberFormatter::CURRENCY);
      $symbol = $fmt->getSymbol(NumberFormatter::CURRENCY_SYMBOL);
      $str = str_replace($symbol, 'р.', $str);
    }

    return $str;
  }

  /**
   * Возвращает сумму прописью
   *
   * @param string $amount
   * @param bool $show_units
   * @param bool $with_kop
   *
   * @return string
   * @author runcore
   * @uses morph(...)
   *
   */
  public function formatHumanRu(string $amount, bool $show_units = true, bool $with_kop = false): string {
    $amount = str_replace(' ', '', $amount);
    if (str_contains($amount, ',')) {
      $amount = str_replace(',', '.', $amount);
    }
    $amount = (float)$amount;

    $nul = 'ноль';

    $digits = [
      [
        '',
        'один',
        'два',
        'три',
        'четыре',
        'пять',
        'шесть',
        'семь',
        'восемь',
        'девять',
      ],
      [
        '',
        'одна',
        'две',
        'три',
        'четыре',
        'пять',
        'шесть',
        'семь',
        'восемь',
        'девять',
      ],
    ];
    $a20 = [
      'десять',
      'одиннадцать',
      'двенадцать',
      'тринадцать',
      'четырнадцать',
      'пятнадцать',
      'шестнадцать',
      'семнадцать',
      'восемнадцать',
      'девятнадцать',
    ];
    $tens = [
      2 => 'двадцать', // начальный индекс 2, ибо 1 не нужен (10 в $a20), а ноль - тем более.
      'тридцать',
      'сорок',
      'пятьдесят',
      'шестьдесят',
      'семьдесят',
      'восемьдесят',
      'девяносто',
    ];
    $hundreds = [
      '', // 0 сотен, заглушка для более удобной индексации.
      'сто',
      'двести',
      'триста',
      'четыреста',
      'пятьсот',
      'шестьсот',
      'семьсот',
      'восемьсот',
      'девятьсот',
    ];
    $units = [
      ['копейка', 'копейки', 'копеек', 1],
      ['рубль', 'рубля', 'рублей', 0],
      ['тысяча', 'тысячи', 'тысяч', 1],
      ['миллион', 'миллиона', 'миллионов', 0],
      ['миллиард', 'милиарда', 'миллиардов', 0],
    ];

    [$rub, $kop] = explode('.', sprintf('%015.2f', $amount));

    $out = [];

    if ((int)$rub > 0) {
      foreach (str_split($rub, 3) as $index => $group) { // by 3 symbols
        if (!(int)$group) {
          continue;
        }
        $unit_index = count($units) - $index - 1; // units key
        $digit_morph_index = $units[$unit_index][3];

        [$dig1, $dig2, $dig3] = array_map('intval', str_split($group));

        // mega-logic
        $out[] = $hundreds[$dig1]; # 1xx-9xx

        if ($dig2 > 1) {
          $out[] = $tens[$dig2] . ' ' . $digits[$digit_morph_index][$dig3];
        } # 20-99
        else {
          $out[] = $dig2 > 0 ? $a20[$dig3] : $digits[$digit_morph_index][$dig3];
        } # 10-19 | 1-9

        // units without rub & kop
        if ($unit_index > 1) {
          $current_unit = $units[$unit_index];
          $out[] = morph($group, $current_unit[0], $current_unit[1], $current_unit[2]);
        }
      }
    }
    else {
      $out[] = $nul;
    }

    if ($show_units) {
      $rub_unit = $units[1];
      $out[] = morph((int)$rub, $rub_unit[0], $rub_unit[1], $rub_unit[2]); // rub
    }

    if ($with_kop) {
      $kop_unit = $units[0];
      $out[] = $kop . ' ' . morph($kop, $kop_unit[0], $kop_unit[1], $kop_unit[2]); // kop
    }

    $str = implode(' ', $out);

    return trim(preg_replace('/ {2,}/', ' ', $str));
  }

  /**
   * @param string $amount
   * @param string $currency
   *
   * @return float
   */
  public function fromInput(string $amount, string $currency): float {
    switch ($currency) {
      case 'USD':
        // 1,200.50
        $amount = str_replace(',', '', $amount);
        return (float)$amount;

      default:
        $amount = str_replace([' ', ','], ['', '.'], $amount);
        return (float)$amount;
    }
  }

  /**
   * @param string $currency
   *
   * @return string
   */
  private function getLocaleFromCurrency(string $currency): string {
    $locales = [
      'USD' => 'en_US',
      'EUR' => 'nl_NL',
      'RUB' => 'ru_RU',
    ];

    return $locales[$currency] ?: $locales['RUB'];
  }
}
