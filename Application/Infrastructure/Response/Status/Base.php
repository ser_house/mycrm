<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.04.2020
 * Time: 10:04
 */


namespace Application\Infrastructure\Response\Status;

use JsonSerializable;

/**
 * Class Base
 *
 * @package Application\Infrastructure\Response\Status
 */
class Base implements JsonSerializable {
  protected const STATUS = '';

  /**
   * Base constructor.
   *
   * @param string $msg
   */
  public function __construct(protected readonly string $msg) {

  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    return [
      'status' => static::STATUS,
      'msg' => $this->msg,
    ];
  }
}
