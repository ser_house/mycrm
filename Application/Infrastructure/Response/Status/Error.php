<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.04.2020
 * Time: 10:03
 */


namespace Application\Infrastructure\Response\Status;

/**
 * Class Error
 *
 * @package Application\Infrastructure\Response\Status
 */
class Error extends Base {

  protected const STATUS = 'error';

}
