<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.04.2020
 * Time: 10:03
 */


namespace Application\Infrastructure\Response\Status;


/**
 * Class Success
 *
 * @package Application\Infrastructure\Response\Status
 */
class Success extends Base {

  protected const STATUS = 'success';

  private array $vars;

  /**
   * Success constructor.
   *
   * @param string $msg
   * @param array $vars
   */
  public function __construct(string $msg = '', array $vars = []) {
    parent::__construct($msg);
    $this->vars = $vars;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    $json = parent::jsonSerialize();
    if ($this->vars) {
      $json = array_merge($json, $this->vars);
    }
    return $json;
  }
}
