<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.06.2020
 * Time: 4:06
 */


namespace Application\Infrastructure\Holiday;


use App\Model\Holiday;
use Application\Calendar\IHolidayDataProvider;
use DateTimeImmutable;

class DataProvider implements IHolidayDataProvider {
  /**
   * @inheritDoc
   */
  public function getDates(int $client_id, DateTimeImmutable $startDate, DateTimeImmutable $endDate): array {
    $start_date = $startDate->format('Y-m-d');
    $end_date = $endDate->format('Y-m-d');

    $items = Holiday::whereClientId($client_id)
      ->whereBetween('date', [$start_date, $end_date])
      ->get(['date'])
    ;

    $holidays = [];

    foreach ($items as $item) {
      $holidays[$item->date] = $item->date;
    }

    return $holidays;
  }
}
