<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.02.2020
 * Time: 10:13
 */


namespace Application\Infrastructure\Exception;


use DomainException;

class NegativeRateException extends DomainException {

}
