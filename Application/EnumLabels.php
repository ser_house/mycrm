<?php

namespace Application;

trait EnumLabels {
  /**
   * @return array
   */
  public static function labels(): array {
    $labels = [];
    foreach (self::cases() as $case) {
      $labels[(string)$case->value] = $case->label();
    }
    return $labels;
  }

  /**
   * @return array
   */
  public static function values(): array {
    $values = [];
    foreach (self::cases() as $case) {
      $values[] = (string)$case->value;
    }
    return $values;
  }
}
