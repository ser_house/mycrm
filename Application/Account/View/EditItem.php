<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 18.05.2020
 * Time: 10:08
 */


namespace Application\Account\View;

/**
 * Class EditItem
 *
 * @package Application\Account\View
 */
class EditItem {
  public int $id;
  public int $type_id;
  public string $name;
  public string $note;
  public string $currency;
  public bool $is_active;
  public bool $has_payments;
}
