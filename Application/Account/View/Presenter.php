<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 18.05.2020
 * Time: 10:09
 */


namespace Application\Account\View;


use App\Model\Account;

/**
 * Class Presenter
 *
 * @package Application\Account\View
 */
class Presenter {

  /**
   * @param Account[] $accountModels
   *
   * @return array
   */
  public function presentEditItems(iterable $accountModels): array {
    // @todo: сделать нормальный один запрос на кол-во платежей.
    $accounts = [];
    /** @var Account $accountModel */
    foreach($accountModels as $accountModel) {
      $view = new EditItem();
      $view->id = $accountModel->id;
      $view->type_id = $accountModel->type_id;
      $view->name = $accountModel->name;
      $view->note = (string)$accountModel->note;
      $view->currency = $accountModel->currency;
      $view->is_active = $accountModel->is_active;
      $view->has_payments = $accountModel->payments()->count() > 0;
      $accounts[] = $view;
    }

    return $accounts;
  }

  /**
   * @param Account[] $accountModels
   *
   * @return array
   */
  public function presentGroupedAccounts(iterable $accountModels): array {
    $accounts = [];
    foreach($accountModels as $accountModel) {
      $type = $accountModel->accountType->name;
      $accounts[$type][] = $accountModel;
    }

    return $accounts;
  }
}
