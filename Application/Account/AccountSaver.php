<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.01.2018
 * Time: 19:59
 */

namespace Application\Account;


use App\Model\Account;
use Exception;

/**
 * Class AccountSaver
 *
 * @package Application\Account
 */
class AccountSaver {

  /**
   * @param array $accounts
   * @param int $user_id
   *
   * @throws Exception
   */
  public function update(array $accounts, int $user_id): void {
    $exists_accounts = Account::user($user_id)->get();

    if (!empty($accounts)) {
      $to_update = [];

      foreach ($accounts as $account) {
        $account['is_active'] = $account['is_active'] ?? false;
        // Обновление обновленных (на самом деле просто перезапись всех тех текущих,
        // для которых пришли данные с формы - даже если они и не изменились).
        if (!empty($account['id'])) {
          $id = $account['id'];
          Account::find($id)?->update($account);
          // Это существующие, пригодятся при определении удаленных.
          $to_update[$id] = $id;
        }
        else {
          $account['user_id'] = $user_id;
          (new Account($account))->save();
        }
      }

      // Удаление удаленных.
      foreach ($exists_accounts as $exist_account) {
        $exist_id = $exist_account->id;
        if (!isset($to_update[$exist_id])) {
          Account::find($exist_id)?->delete();
        }
      }
    }
    elseif (!empty($exists_accounts)) {
      foreach ($exists_accounts as $exist_account) {
        Account::find($exist_account->id)?->delete();
      }
    }
    else {
      Account::user($user_id)->delete();
    }
  }
}
