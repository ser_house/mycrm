<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.07.2020
 * Time: 13:17
 */


namespace Application\Account;

use Application\Money\Currency;
use App\Model\Account;
use App\Model\User;
use DB;

class DataProvider {

  /**
   * @param int $user_id
   *
   * @return iterable
   */
  public function getUserAccounts(int $user_id): iterable {
    return Account::user($user_id)->get();
  }

  /**
   * @return array
   */
  public function getTypes(): array {
    $type_items = DB::select('SELECT * FROM account_type ORDER BY name');

    $types = [];
    foreach ($type_items as $type_item) {
      $types[] = [
        'id' => $type_item->id,
        'name' => $type_item->name,
      ];
    }

    return $types;
  }

  /**
   * @param int $user_id
   *
   * @return array
   */
  public function getUserCurrencies(int $user_id): array {
    $user = User::find($user_id);
    $user_currencies = $user->getCurrencies();

    $currencies = [];
    if ($user_currencies) {
      $currencies = array_values(Currency::currenciesByCodes($user_currencies));
    }

    return $currencies;
  }

  /**
   * @param int $user_id
   *
   * @return Account[]
   */
  public function getTypedActiveAccounts(int $user_id): iterable {
    return Account::userActive($user_id)->with('accountType')->get();
  }

  /**
   * @param int $user_id
   *
   * @return Account|null
   */
  public function getLastPaymentAccountForUser(int $user_id): ?Account {
    $sql = 'SELECT p.account_id
	  FROM payment AS p
	  INNER JOIN task AS t ON p.task_id = t.id
	  WHERE t.user_id = :user_id
	  ORDER BY p.created_at DESC
	  LIMIT 1';
    $data = DB::select($sql, ['user_id' => $user_id]);

    if (empty($data)) {
      return null;
    }

    return Account::find($data[0]->account_id);
  }

  /**
   * @param int $client_id
   *
   * @return string|null
   */
  public function getClientLastPaymentAccount(int $client_id): ?string {
    $last_account_sql = 'SELECT payment.account_id
			FROM payment
			INNER JOIN task ON task.id = payment.task_id
			WHERE task.client_id = :client_id
			ORDER BY payment.id DESC
			LIMIT 1';
    $db_result = DB::select($last_account_sql, ['client_id' => $client_id]);
    return $db_result ? $db_result[0]->account_id : null;
  }
}
