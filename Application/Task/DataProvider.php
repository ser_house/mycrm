<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.07.2020
 * Time: 13:55
 */


namespace Application\Task;


use App\Model\Task\Task;
use App\Model\Task\TaskStateJournal;
use Application\Task\Loader\LoaderByClient;
use Application\Task\Loader\LoaderByIds;
use Application\Task\Loader\LoaderByUser;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use DB;

/**
 * Class DataProvider
 *
 * @package Application\Task
 */
class DataProvider {

  /**
   * @param int $task_id
   *
   * @return Task
   */
  public function getTaskWithClient(int $task_id): Task {
    return Task::withClient()->findOrFail($task_id);
  }

  /**
   * @param int $task_id
   *
   * @return array
   */
  public function getTaskStatusHistory(int $task_id): array {
    return TaskStateJournal::where('task_id', $task_id)->get()->all();
  }

  /**
   * @param int $user_id
   *
   * @return LengthAwarePaginator
   */
  public function getUserTasks(int $user_id): LengthAwarePaginator {
    $loader = new LoaderByUser($user_id);
    return $loader->load();
  }

  /**
   * @param int $user_id
   *
   * @return LengthAwarePaginator
   */
  public function getUserActiveTasks(int $user_id): LengthAwarePaginator {
    $states_str = implode(',',
      array_map(static function ($state) {
        return "'$state->value'";
      }, State::statesActiveTasks()));
    $sql = "SELECT task.id
		FROM task
		WHERE user_id = :user_id AND state IN ($states_str)";
    $items = DB::select($sql, ['user_id' => $user_id]);
    $ids = [];
    foreach ($items as $item) {
      $ids[] = $item->id;
    }

    $loader = new LoaderByIds($ids);
    return $loader->load();
  }

  /**
   * @param int $client_id
   *
   * @return LengthAwarePaginator
   */
  public function getClientTasks(int $client_id): LengthAwarePaginator {
    $loader = new LoaderByClient($client_id);
    return $loader->load();
  }
}
