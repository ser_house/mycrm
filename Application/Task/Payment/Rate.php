<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.05.2020
 * Time: 9:37
 */


namespace Application\Task\Payment;

use Application\Infrastructure\Exception\NegativeRateException;

class Rate {
  private float $value;

  /**
   * Rate constructor.
   *
   * @param string $value
   */
  public function __construct(string $value) {
    $float_value = (float)str_replace([',', ' '], ['.', ''], $value);
    if ($float_value < 0.0) {
      throw new NegativeRateException('Отрицательный курс недопустим.');
    }
    $this->value = number_format($float_value, 2, '.', '');
  }

  /**
   * @return float
   */
  public function value(): float {
    return $this->value;
  }

  /**
   * @param Rate $rate
   *
   * @return bool
   */
  public function equals(Rate $rate): bool {
    return $rate->value() === $this->value();
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return (string)$this->value();
  }
}
