<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.07.2020
 * Time: 18:42
 */


namespace Application\Task\Payment;


use App\Model\Payment;
use App\Model\Task\Task;
use Application\Incoming\Service as IncomingService;
use Application\Infrastructure\Formatter\Money as MoneyFormatter;
use Exception;

class PaymentSaver {
  public function __construct(
    private readonly MoneyFormatter $moneyFormatter,
    private readonly IncomingService $incomingService) {

  }

  /**
   * @param array $payment
   *
   * @return bool
   */
  private function isValidPayment(array $payment): bool {
    return !empty($payment['created_at'])
      && !empty($payment['account_id'])
      && !empty($payment['amount']);
  }

  /**
   * @param Task $task
   * @param array $items
   *
   * @throws Exception
   */
  public function processPayments(Task $task, array $items): void {
    $changed_years = [];

    if (!empty($items)) {

      $items = array_filter($items, [$this, 'isValidPayment']);

      $to_update = [];
      $to_add = [];

      foreach ($items as $item) {
        $item['amount'] = $this->moneyFormatter->fromInput($item['amount'], $task->currency);
        if (!empty($item['rate'])) {
          $rate = new Rate($item['rate']);
          $item['rate'] = $rate->value();
        }
        else {
          $item['rate'] = 1.00;
        }
        // Обновление обновленных (на самом деле просто перезапись всех тех текущих,
        // для которых пришли данные с формы - даже если они и не изменились).
        $changed_year = date('Y', strtotime($item['created_at']));
        $changed_years[$changed_year] = $changed_year;
        if (!empty($item['id'])) {
          $id = $item['id'];
          $task->payments()->findOrFail($id)?->update($item);
          // Это существующие, пригодятся при определении удаленных.
          $to_update[$id] = $id;
        }
        else {
          $to_add[] = new Payment($item);
        }
      }

      // Удаление удаленных.
      foreach ($task->payments as $exist_item) {
        $exist_id = $exist_item->id;
        if (!isset($to_update[$exist_id])) {
          Payment::findOrFail($exist_id)->delete();
          $changed_year = date('Y', strtotime($exist_item['created_at']));
          $changed_years[$changed_year] = $changed_year;
        }
      }

      // Добавление добавленных.
      if (!empty($to_add)) {
        $task->payments()->saveMany($to_add);
      }
    }
    else {
      $payments = $task->payments();
      foreach ($payments as $payment) {
        $changed_year = date('Y', strtotime($payment->created_at));
        $changed_years[$changed_year] = $changed_year;
      }
      $payments->delete();
    }

    // Сбрасываем кэш статистики по годам,
    $this->incomingService->resetUserCache($task->user_id);
    // кэш статистики по клиенту
    $this->incomingService->resetClientCache($task->user_id, $task->client_id);
    // и кэши конкретного клиента и пользователя по измененным годам.
    foreach ($changed_years as $changed_year) {
      $this->incomingService->resetCache($task->user_id, $changed_year, $task->client_id);
      $this->incomingService->resetUserYearCache($task->user_id, $changed_year);
    }
  }
}
