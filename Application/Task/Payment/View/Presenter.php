<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2020
 * Time: 21:14
 */


namespace Application\Task\Payment\View;


use App\Model\Payment;
use App\Model\Task\Task;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Infrastructure\Formatter\Money as MoneyFormatter;
use Application\Money\Amount;
use DateTimeImmutable;
use Exception;

class Presenter {
  public function __construct(
    private readonly DateTimeFormatter $dateTimeFormatter,
    private readonly MoneyFormatter $moneyFormatter) {

  }

  /**
   * @param Payment $payment
   * @param Task $task
   *
   * @return Full
   * @throws Exception
   */
  public function full(Payment $payment, Task $task): Full {
    $view = new Full();

    $view->id = $payment->id;
    $view->task_id = $task->id;

    $createdAt = new DateTimeImmutable($payment->created_at);
    $view->created_at = $this->dateTimeFormatter->dateShort($createdAt);

    $account = $payment->account;
    $account_type = $account->accountType->name;
    $view->account = "$account->name ($account_type)";

    $view->note = $payment->note ?: '';

    $amount = new Amount($payment->amount, $account->currency);
    $view->amount = $this->moneyFormatter->format($amount);

    return $view;
  }

  /**
   * @param Payment $payment
   *
   * @return Short
   * @throws Exception
   */
  public function short(Payment $payment): Short {
    $view = new Short();

    $createdAt = new DateTimeImmutable($payment->created_at);
    $view->created_at = $this->dateTimeFormatter->dateShort($createdAt);

    $account = $payment->account;
    $amount = new Amount($payment->amount, $account->currency);
    $view->amount = $amount->amount;
    $view->currency = $amount->currency;

    return $view;
  }
}
