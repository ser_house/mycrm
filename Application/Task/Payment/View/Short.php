<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.05.2020
 * Time: 10:05
 */


namespace Application\Task\Payment\View;


class Short {

  public string $created_at;
  public float $amount;
  public string $currency;
}
