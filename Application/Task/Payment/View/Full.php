<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2020
 * Time: 21:16
 */


namespace Application\Task\Payment\View;


class Full {

  public int $id;
  public int $task_id;
  public string $created_at;
  public string $account;
  public string $note;
  public string $amount;
}
