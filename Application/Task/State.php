<?php

namespace Application\Task;

use Application\EnumLabels;
use JsonSerializable;

enum State: string implements JsonSerializable {
  use EnumLabels;

  case New = 'new';
  case Running = 'running';
  case Finished = 'finished';
  case Closed = 'closed';
  case Canceled = 'canceled';

  /**
   * @return string
   */
  public function label(): string {
    return match ($this) {
      self::New => 'Новая',
      self::Running => 'Выполняется',
      self::Finished => 'Завершена',
      self::Closed => 'Оплачена',
      self::Canceled => 'Отменена',
    };
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    return [
      'key' => $this->value,
      'title' => $this->label(),
      'css_class' => $this->cssClass(),
    ];
  }

  /**
   *
   * @return string
   */
  public function cssClass(): string {
    return "task-state-{$this->value}";
  }

  /**
   *
   * @return array
   */
  public static function statesActiveTasks(): array {
    return [
      self::New,
      self::Running,
      self::Finished,
    ];
  }
}
