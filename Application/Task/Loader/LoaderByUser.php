<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.06.2018
 * Time: 16:51
 */

namespace Application\Task\Loader;


/**
 * Class LoaderByUser
 *
 * @package Application\Task\Loader
 */
class LoaderByUser extends Loader {

	/**
	 * LoaderByUser constructor.
	 *
	 * @param int $user_id
	 */
	public function __construct(int $user_id) {
		parent::__construct();

		$this->builder->where('user_id', $user_id);
	}
}
