<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.06.2018
 * Time: 17:03
 */

namespace Application\Task\Loader;


/**
 * Class LoaderByClient
 *
 * @package Application\Task\Loader
 */
class LoaderByClient extends Loader {

  /**
   * LoaderByClient constructor.
   *
   * @param int $client_id
   */
  public function __construct(int $client_id) {
    parent::__construct();

    $this->builder->where('client_id', $client_id);
  }
}
