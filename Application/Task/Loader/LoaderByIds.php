<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.06.2018
 * Time: 17:02
 */

namespace Application\Task\Loader;


/**
 * Class LoaderByIds
 *
 * @package Application\Task\Loader
 */
class LoaderByIds extends Loader {

  /**
   * LoaderByIds constructor.
   *
   * @param array $ids
   */
  public function __construct(array $ids) {
    parent::__construct();

    $this->builder->whereIn('id', $ids);
  }
}
