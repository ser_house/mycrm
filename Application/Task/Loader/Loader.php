<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.06.2018
 * Time: 16:52
 */

namespace Application\Task\Loader;


use App\Model\Task\Task;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as ResultType;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Loader
 *
 * @package Application\Task\Loader
 */
class Loader implements ILoader {

  protected Builder $builder;

  protected string $orderField = 'launched_at';
  protected string $orderDirection = 'desc';

  /**
   * Loader constructor.
   */
  public function __construct() {
    $this->builder = Task::withClient()->withSubtasks()->withPayments()->with('payments.account');
  }

  /**
   * @param string $field
   * @param string $order
   */
  public function setOrderBy(string $field, string $order = 'desc'): void {
    $this->orderField = $field;
    $this->orderDirection = $order;
  }

  /**
   * @param int $per_page
   *
   * @return ResultType
   */
  public function load(int $per_page = ILoader::DEFAULT_TASKS_PER_PAGE): ResultType {
    return $this->builder->orderBy($this->orderField, $this->orderDirection)->paginate($per_page);
  }
}
