<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.06.2018
 * Time: 16:48
 */

namespace Application\Task\Loader;


use Illuminate\Contracts\Pagination\LengthAwarePaginator as ResultType;

/**
 * Interface ILoader
 *
 * @package Application\Task\Loader
 */
interface ILoader {

  /**
   *
   */
  public const DEFAULT_TASKS_PER_PAGE = 12;

  /**
   *
   * @param int $per_page
   *
   * @return ResultType
   */
  public function load(int $per_page = ILoader::DEFAULT_TASKS_PER_PAGE): ResultType;
}
