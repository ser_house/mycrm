<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 01.02.2018
 * Time: 16:02
 */

namespace Application\Task;

use App\Model\Task\TaskTemplate;
use Application\Infrastructure\Formatter\Money as MoneyFormatter;

class Service {
  public function __construct(
    private readonly MoneyFormatter $moneyFormatter
  ) {

  }

  /**
   * @param array $inputs
   */
  public function prepareInputsToTaskTemplate(array &$inputs): void {
    if (!empty($inputs['budget'])) {
      $inputs['budget'] = $this->moneyFormatter->fromInput($inputs['budget'], $inputs['currency']);
    }
    else {
      $inputs['budget'] = 0.00;
    }
  }

  /**
   * @param int $user_id
   * @param int $client_id
   * @param array $data
   *
   * @return TaskTemplate
   */
  public function addTaskTemplate(int $user_id, int $client_id, array $data): TaskTemplate {
    $this->prepareInputsToTaskTemplate($data);

    $data['user_id'] = $user_id;
    $data['client_id'] = $client_id;

    $template = new TaskTemplate($data);
    $template->save();

    return $template;
  }

  /**
   * @param int $template_id
   * @param array $data
   *
   * @return TaskTemplate
   */
  public function updateTaskTemplate(int $template_id, array $data): TaskTemplate {
    $this->prepareInputsToTaskTemplate($data);
    /** @var TaskTemplate $template */
    $template = TaskTemplate::findOrFail($template_id);
    $template->fill($data);
    $template->save();

    return $template;
  }

  public function removeTaskTemplate(int $template_id): void {
    /** @var TaskTemplate $taskTemplate */
    $taskTemplate = TaskTemplate::findOrFail($template_id);
    $taskTemplate->delete();
  }
}
