<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2020
 * Time: 20:57
 */


namespace Application\Task\SubTask\View;


use Application\Money\Amount;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Infrastructure\Formatter\Money as MoneyFormatter;
use App\Model\Task\SubTask;
use App\Model\Task\Task;

class Presenter {
  public function __construct(
    private readonly DateTimeFormatter $dateTimeFormatter,
    private readonly MoneyFormatter $moneyFormatter) {

  }

  /**
   * @param SubTask $subTask
   * @param Task $task
   *
   * @return Full
   */
  public function full(SubTask $subTask, Task $task): Full {
    $view = new Full();

    $view->id = $subTask->id;
    $view->task_id = $task->id;
    $view->title = $subTask->title;
    $view->note = (string)$subTask->note;

    $budget = new Amount($subTask->budget, $task->currency);
    $view->amount = $this->moneyFormatter->format($budget);

    $view->hours = $subTask->hours ? $this->dateTimeFormatter->hours($subTask->hours) : '';

    return $view;
  }
}
