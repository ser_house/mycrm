<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2020
 * Time: 20:57
 */


namespace Application\Task\SubTask\View;


class Full {

  public int $id;
  public int $task_id;
  public string $title;
  public string $note;
  public string $amount;
  public string $hours;
}
