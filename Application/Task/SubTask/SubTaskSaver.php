<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.07.2020
 * Time: 18:49
 */


namespace Application\Task\SubTask;


use App\Model\Task\SubTask;
use App\Model\Task\Task;
use Application\Infrastructure\Formatter\Money as MoneyFormatter;
use Exception;

class SubTaskSaver {

  public function __construct(private readonly MoneyFormatter $moneyFormatter) {

  }

  /**
   * @param array $sub_task
   *
   * @return bool
   */
  private function filterSubTaskEmpty(array $sub_task): bool {
    return !empty($sub_task['title']) && !empty($sub_task['budget']);
  }

  /**
   * @param Task $task
   * @param array $items
   *
   * @throws Exception
   */
  public function processSubTasks(Task $task, array $items): void {
    if (!empty($items)) {

      $items = array_filter($items, [$this, 'filterSubTaskEmpty']);

      $to_update = [];
      $to_add = [];

      foreach ($items as $item) {
        $item['budget'] = $this->moneyFormatter->fromInput($item['budget'], $task->currency);
        // Обновление обновленных (на самом деле просто перезапись всех тех текущих,
        // для которых пришли данные с формы - даже если они и не изменились).
        if (!empty($item['id'])) {
          $id = $item['id'];
          $task->subTasks()->findOrFail($id)?->update($item);
          // Это существующие, пригодятся при определении удаленных.
          $to_update[$id] = $id;
        }
        else {
          $to_add[] = new SubTask($item);
        }
      }

      // Удаление удаленных.
      foreach ($task->subTasks as $exist_item) {
        $exist_id = $exist_item->id;
        if (!isset($to_update[$exist_id])) {
          SubTask::find($exist_id)?->delete();
        }
      }

      // Добавление добавленных.
      if (!empty($to_add)) {
        $task->subTasks()->saveMany($to_add);
      }
    }
    else {
      $task->subTasks()->delete();
    }
  }
}
