<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2020
 * Time: 10:00
 */


namespace Application\Task\View;


class Client {
  public function __construct(
    public readonly int $id,
    public readonly string $title,
    public readonly bool $is_selectable
  ) {

  }
}
