<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.04.2020
 * Time: 10:43
 */


namespace Application\Task\View;

use Application\Task\State;

class Short {

  public int $id;
  public int $user_id;

  public string $launched_at;
  public string $completed_at;

  public string $title;
  public string $description;

  public float $budget;
  public string $currency;

  public string $hours;

  /** @var \Application\Task\Payment\View\Short[] */
  public array $payments = [];

  public Client $client;
  public State $state;
}
