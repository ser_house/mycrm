<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2020
 * Time: 20:54
 */


namespace Application\Task\View;


use Application\Task\State;

class Full {
  public int $id;
  public int $user_id;

  public string $launched_at;
  public string $completed_at;

  public string $title;
  public string $description;

  public string $budget;

  public string $site_url;
  public string $template;

  public string $hours;

  public array $sub_tasks;
  public array $payments;

  public Client $client;
  public State $state;
}
