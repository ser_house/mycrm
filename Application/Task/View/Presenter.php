<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.04.2020
 * Time: 10:42
 */


namespace Application\Task\View;


use App\Model\Task\Task;
use App\Model\Task\TaskStateJournal;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Infrastructure\Formatter\Money as MoneyFormatter;
use Application\Task\Payment\View\Presenter as PaymentPresenter;
use Application\Task\SubTask\View\Presenter as SubTaskPresenter;
use DateTimeImmutable;
use Exception;


class Presenter {

  public function __construct(
    private readonly DateTimeFormatter $dateTimeFormatter,
    private readonly MoneyFormatter $moneyFormatter,
    private readonly SubTaskPresenter $subTaskPresenter,
    private readonly PaymentPresenter $paymentPresenter
  ) {

  }


  /**
   * @param Task $task
   *
   * @return Short
   * @throws Exception
   */
  public function short(Task $task): Short {
    $view = new Short();

    $view->id = $task->id;
    $view->user_id = $task->user_id;

    $launchedAt = new DateTimeImmutable($task->launched_at);
    $view->launched_at = $this->dateTimeFormatter->dateShort($launchedAt);

    $completedAt = $task->completed_at ? new DateTimeImmutable($task->completed_at) : null;
    $view->completed_at = $completedAt ? $this->dateTimeFormatter->dateShort($completedAt) : '';

    $view->title = $task->title;
    $view->description = $task->description;

    $budget = $task->getBudget();
    $view->budget = $budget->amount;
    $view->currency = $budget->currency;

    $hours = $task->getHours();
    $view->hours = $hours ? $this->dateTimeFormatter->hours($task->getHours()) : '';

    $view->client = new Client($task->client->id, $task->client->name, $task->client->is_selectable);
    $view->state = $task->state;

    foreach ($task->payments as $payment) {
      $view->payments[] = $this->paymentPresenter->short($payment);
    }

    return $view;
  }

  /**
   * @param array $tasks
   *
   * @return array
   * @throws Exception
   */
  public function shortList(array $tasks): array {
    $short_tasks = [];
    foreach ($tasks as $task) {
      $short_tasks[] = $this->short($task);
    }

    return $short_tasks;
  }

  /**
   * @param Task $task
   *
   * @return Full
   */
  public function full(Task $task): Full {
    $view = new Full();

    $this->fillBaseView($view, $task);

    $view->site_url = $task->site->url ?? '';

    $template = $task->template;
    $view->template = $template ? $template->title : '';

    $view->sub_tasks = [];
    foreach ($task->subTasks as $subTask) {
      $view->sub_tasks[] = $this->subTaskPresenter->full($subTask, $task);
    }

    $view->payments = [];
    foreach ($task->payments as $payment) {
      $view->payments[] = $this->paymentPresenter->full($payment, $task);
    }
    return $view;
  }

  /**
   * @param TaskStateJournal[] $history_items
   *
   * @return array
   * @throws Exception
   */
  public function statusHistory(array $history_items): array {
    $status_history = [];
    foreach ($history_items as $item) {
      $status_history[] = [
        'time' => $this->dateTimeFormatter->dateTimeShort(new DateTimeImmutable($item->inserted_at)),
        'state' => $item->state_to,
      ];
    }

    return $status_history;
  }

  /**
   * @param Task $task
   *
   * @return iterable
   */
  public function taskPaymentsForEdit(Task $task): iterable {
    $payments = $task->payments;
    foreach ($payments as $payment) {
      $payment->task_id = $task->id;
    }
    return $payments;
  }

  /**
   * @param Task $task
   *
   * @return iterable
   */
  public function taskSubTasksForEdit(Task $task): iterable {
    $sub_tasks = $task->subTasks;

    foreach ($sub_tasks as $sub_task) {
      $str_budget = str_replace('.', ',', $sub_task->budget);
      $sub_task->budget = $str_budget;
    }
    return $sub_tasks;
  }

  /**
   * @param $view
   * @param Task $task
   *
   * @return void
   * @throws Exception
   */
  private function fillBaseView($view, Task $task): void {
    $view->id = $task->id;
    $view->user_id = $task->user_id;

    $launchedAt = new DateTimeImmutable($task->launched_at);
    $view->launched_at = $this->dateTimeFormatter->dateShort($launchedAt);

    $completedAt = $task->completed_at ? new DateTimeImmutable($task->completed_at) : null;
    $view->completed_at = $completedAt ? $this->dateTimeFormatter->dateShort($completedAt) : '';

    $view->title = $task->title;
    $view->description = $task->description;

    $budget = $task->getBudget();
    $view->budget = $this->moneyFormatter->format($budget);
    $view->raw_budget = $budget->amount;

    $hours = $task->getHours();
    $view->hours = $hours ? $this->dateTimeFormatter->hours($task->getHours()) : '';

    $view->client = new Client($task->client->id, $task->client->name, $task->client->is_selectable);
    $view->state = $task->state;
  }
}
