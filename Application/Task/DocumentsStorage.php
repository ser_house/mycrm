<?php

namespace Application\Task;

use Illuminate\Support\Facades\DB;
use Throwable;

class DocumentsStorage {

  /**
   * @param int $task_id
   * @param int $document_id
   *
   * @return void
   */
  public function add(int $task_id, int $document_id): void {
    // Если нарушение по unique, то значит такое сочетание уже есть.
    // А это то же самое, что и успешная вставка.
    try {
      DB::table('task_document')->insert(['task_id' => $task_id, 'document_id' => $document_id]);
    }
    catch (Throwable $e) {
    }
  }

  /**
   * @param int $task_id
   *
   * @return void
   */
  public function removeForTask(int $task_id): void {
    DB::table('task_document')->where(['task_id' => $task_id])->delete();
  }

  /**
   * @param int $task_id
   * @param int $document_type_id
   *
   * @return int[] document ids
   */
  public function getForTask(int $task_id, int $document_type_id): array {
    $sql = "SELECT d.id
    FROM task_document AS td
    INNER JOIN document AS d ON d.id = td.document_id
    WHERE td.task_id = :task_id AND d.type_id = :type_id
    ";
    $data = DB::select($sql, [
      'task_id' => $task_id,
      'type_id' => $document_type_id,
    ]);
    if (empty($data)) {
      return [];
    }
    return array_column($data, 'id');
  }
}
