<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 01.02.2018
 * Time: 16:02
 */

namespace Application\Task\Action;

use App\Model\Task\Task;
use App\Model\Task\TaskStateJournal;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Task\State;
use DateTimeImmutable;
use DB;
use Throwable;

class Finish {
  public function __construct(private readonly DateTimeFormatter $dateTimeFormatter) {

  }

  /**
   * @param Task $task
   *
   * @return Task
   * @throws Throwable
   */
  public function run(Task $task): Task {

    $task->completed_at = $this->dateTimeFormatter->dateToDb(new DateTimeImmutable());
    $task->state = State::Finished;

    DB::beginTransaction();

    try {
      /** @var TaskStateJournal $status */
      $status = TaskStateJournal::create(['task_id' => $task->id, 'state_to' => $task->state->value]);
      $status->save();

      $task->save();

      DB::commit();

      return $task;
    }
    catch (Throwable $e) {
      DB::rollBack();
      throw $e;
    }
  }
}
