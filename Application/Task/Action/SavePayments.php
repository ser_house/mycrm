<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.07.2020
 * Time: 19:18
 */


namespace Application\Task\Action;

use App\Model\Task\Task;
use App\Model\Task\TaskStateJournal;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Task\Payment\PaymentSaver;
use Application\Task\State;
use DateTimeImmutable;
use DB;
use Throwable;

class SavePayments {
  public function __construct(
    private readonly DateTimeFormatter $dateTimeFormatter,
    private readonly PaymentSaver $paymentSaver
  ) {
  }

  /**
   * @param Task $task
   * @param array $data
   *
   * @return Task
   * @throws Throwable
   */
  public function run(Task $task, array $data): Task {
    DB::beginTransaction();

    try {
      $this->paymentSaver->processPayments($task, $data);
      $task->load('payments');
      if ($task->getPayed() >= $task->getBudget()) {
        $task->state = State::Closed;
        if (empty($task->completed_at)) {
          $task->completed_at = $this->dateTimeFormatter->dateToDb(new DateTimeImmutable());
        }
        /** @var TaskStateJournal $status */
        $status = TaskStateJournal::create(['task_id' => $task->id, 'state_to' => $task->state->value]);
        $status->save();
        $task->save();
      }
      DB::commit();
    }
    catch (Throwable $e) {
      DB::rollBack();
      throw $e;
    }

    return $task;
  }
}
