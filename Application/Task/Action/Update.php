<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 01.02.2018
 * Time: 16:02
 */

namespace Application\Task\Action;

use App\Model\Task\Task;
use App\Model\Task\TaskStateJournal;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Task\Payment\PaymentSaver;
use Application\Task\State;
use Application\Task\SubTask\SubTaskSaver;
use DateTimeImmutable;
use DB;
use Throwable;

class Update {
  public function __construct(
    private readonly PaymentSaver $paymentSaver,
    private readonly SubTaskSaver $subTaskSaver,
    private readonly DateTimeFormatter $dateTimeFormatter
  ) {
  }

  /**
   * @param int $task_id
   * @param array $data
   *
   * @return Task
   * @throws Throwable
   */
  public function run(int $task_id, array $data): Task {
    /** @var Task $task */
    $task = Task::findOrFail($task_id);

    $is_state_changed = false;
    if (!empty($data['state'])) {
      $is_state_changed = $task->state->value != $data['state'];
    }

    if (empty($data['description'])) {
      $data['description'] = '';
    }
    if (empty($data['client_site_id'])) {
      $data['client_site_id'] = null;
    }

    if ($is_state_changed && State::Finished === $task->state && empty($data['completed_at'])) {
      $data['completed_at'] = $this->dateTimeFormatter->dateToDb(new DateTimeImmutable());
    }
    $task->fill($data);

    DB::beginTransaction();

    try {
      if ($is_state_changed) {
        /** @var TaskStateJournal $status */
        $status = TaskStateJournal::create(['task_id' => $task->id, 'state_to' => $task->state->value]);
        $status->save();
      }

      if (isset($data['sub_tasks'])) {
        $this->subTaskSaver->processSubTasks($task, $data['sub_tasks']);
      }

      if (isset($data['payments'])) {
        $this->paymentSaver->processPayments($task, $data['payments']);
      }
      $task->save();

      DB::commit();

      return $task;
    }
    catch (Throwable $e) {
      DB::rollBack();
      throw $e;
    }
  }
}
