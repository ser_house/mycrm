<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 01.02.2018
 * Time: 16:02
 */

namespace Application\Task\Action;

use App\Model\Task\SubTask;
use App\Model\Task\Task;
use App\Model\Task\TaskStateJournal;
use App\Model\Task\TaskTemplate;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Placeholder\Processor;
use Application\Task\Payment\PaymentSaver;
use Application\Task\State;
use Application\Task\SubTask\SubTaskSaver;
use DateTimeImmutable;
use DB;
use Throwable;

class Add {
  public function __construct(
    private readonly PaymentSaver $paymentSaver,
    private readonly SubTaskSaver $subTaskSaver,
    private readonly DateTimeFormatter $dateTimeFormatter
  ) {

  }

  /**
   *
   * @return array
   */
  public function buildNewSubtask(): array {
    return [
      'id' => '',
      'title' => 'Основная',
      'task_id' => '',
      'budget' => '',
      'hours' => '',
    ];
  }

  /**
   * @param int $user_id
   * @param int $client_id
   *
   * @return array
   */
  public function buildCreateTaskData(int $user_id, int $client_id): array {
    return [
      'user_id' => $user_id,
      'title' => '',
      'description' => '',
      'client_id' => $client_id,
      'client_site_id' => '',
      'currency' => 'RUB',
      'launched_at' => date('Y-m-d'),
      'completed_at' => '',
      'state' => State::New->value,
    ];
  }

  /**
   * @param array $data
   * @param int $user_id
   * @param int $client_id
   *
   * @return Task
   * @throws Throwable
   */
  public function run(array $data, int $user_id, int $client_id): Task {
    if (empty($data['description'])) {
      $data['description'] = '';
    }
    if (empty($data['client_site_id'])) {
      $data['client_site_id'] = null;
    }
    $data['user_id'] = $user_id;
    $data['client_id'] = $client_id;

    DB::beginTransaction();

    try {
      /** @var Task $task */
      $task = Task::create($data);

      /** @var TaskStateJournal $status */
      $status = TaskStateJournal::create(['task_id' => $task->id, 'state_to' => $data['state']]);
      $status->save();

      if ($data['client_site_id']) {
        $task->client_site_id = $data['client_site_id'];
      }

      $task->save();

      $this->subTaskSaver->processSubTasks($task, $data['sub_tasks']);

      if (isset($data['payments'])) {
        $this->paymentSaver->processPayments($task, $data['payments']);
      }

      DB::commit();

      return $task;
    }
    catch (Throwable $e) {
      DB::rollBack();
      throw $e;
    }
  }

  /**
   * @param int $task_template_id
   *
   * @return Task
   */
  public function createTaskByTemplateId(int $task_template_id): Task {
    /** @var TaskTemplate $taskTemplate */
    $template = TaskTemplate::findOrFail($task_template_id);

    $client = $template->client;
    $data = [
      'user_id' => $client->user_id,
      'client_id' => $client->id,
      'title' => (string)$template->title,
      'description' => (string)$template->description,
      'currency' => $template->currency,
      'launched_at' => date('Y-m-d'),
      'task_template_id' => $template->id,
    ];

    $sub_task = [
      'budget' => $template->budget,
      'hours' => $template->hours,
    ];

    $processor = new Processor($this->dateTimeFormatter);

    $dateTime = new DateTimeImmutable();
    foreach (['title', 'description'] as $field) {
      $data[$field] = $processor->process($dateTime, $data[$field]);
    }

    $task = new Task($data);
    $task->sub_task = new SubTask($sub_task);

    return $task;
  }
}
