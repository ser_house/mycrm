<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.07.2020
 * Time: 18:24
 */


namespace Application\Task\Action;


use App\Model\Task\Task;
use Exception;

class Remove {
  /**
   * @param int $task_id
   *
   * @throws Exception
   */
  public function run(int $task_id): void {
    /** @var Task $task */
    $task = Task::findOrFail($task_id);
    $task->delete();
  }
}
