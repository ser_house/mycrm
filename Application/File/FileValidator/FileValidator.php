<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.03.2018
 * Time: 09:26
 */

namespace Application\File\FileValidator;

use Application\Infrastructure\Formatter\FileSize as FileSizeFormatter;
use Validator;

abstract class FileValidator {
  protected const TYPES = [
    'application/msword' => [
      'text' => 'doc',
      'class' => 'file-type-doc',
      'extensions' => 'doc',
    ],
    'application/vnd.ms-excel' => [
      'text' => 'xls',
      'class' => 'file-type-xls',
      'extensions' => 'xls',
    ],
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => [
      'text' => 'xls',
      'class' => 'file-type-xls',
      'extensions' => 'xlsx',
    ],
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => [
      'text' => 'doc',
      'class' => 'file-type-doc',
      'extensions' => 'docx',
    ],
    'application/pdf' => [
      'text' => 'pdf',
      'class' => 'file-type-pdf',
      'extensions' => 'pdf',
    ],
    'image/jpeg' => [
      'text' => 'pic',
      'class' => 'file-type-pic',
      'extensions' => 'jpg,jpeg',
    ],
    'image/png' => [
      'text' => 'pic',
      'class' => 'file-type-pic',
      'extensions' => 'png',
    ],
  ];

  protected const MIMES = [];
  protected const FIELD_NAME = '';

  protected const MAX_FILE_SIZE_BYTES = 1024 * 5;
  protected const MAX_FILESIZE = self::MAX_FILE_SIZE_BYTES * 1024;

  public function __construct(public readonly FileSizeFormatter $fileSizeFormatter) {

  }

  /**
   *
   * @return string[]
   */
  public function getHelp(): array {
    $valid_extensions_str = $this->getExtensionsAsString();
    return [
      "Допускаются файлы одного из следующих типов: $valid_extensions_str",
      'Максимальный размер файла: ' . $this->fileSizeFormatter->format(static::MAX_FILESIZE),
    ];
  }

  /**
   *
   * @return array
   */
  public function getTypes(): array {
    $types = [];
    foreach (static::MIMES as $mime) {
      $types[$mime] = static::TYPES[$mime];
    }
    return $types;
  }

  /**
   * @param string $delimeter
   *
   * @return string
   */
  public function getMimeTypesAsString(string $delimeter = ','): string {
    return implode($delimeter, static::MIMES);
  }

  /**
   *
   * @return array
   */
  public function getExtensions(): array {
    $valid_extensions = [];
    foreach (static::MIMES as $mime) {
      $valid_extensions[] = static::TYPES[$mime]['extensions'];
    }

    return $valid_extensions;
  }

  /**
   * @param string $delimeter
   *
   * @return string
   */
  public function getExtensionsAsString(string $delimeter = ','): string {
    $valid_extensions = $this->getExtensions();
    return implode($delimeter, $valid_extensions);
  }

  /**
   *
   * @return int
   */
  public function getMaxFileSize(): int {
    return static::MAX_FILESIZE;
  }

  /**
   *
   * @return int
   */
  public function getMaxFileSizeBytes(): int {
    return static::MAX_FILE_SIZE_BYTES;
  }

  /**
   * @param $data
   *
   * @return \Illuminate\Contracts\Validation\Validator
   */
  public function getValidator($data): \Illuminate\Contracts\Validation\Validator {
    $valid_extensions_str = $this->getExtensionsAsString();
    $mime_types_str = $this->getMimeTypesAsString();
    $max_filesize_bytes = $this->getMaxFileSizeBytes();

    $messages = [
      'max' => 'The file may not be greater than :max kilobytes.',
      'mimes' => 'The file must be a file of type: :values.',
      'mimetypes' => 'The file must be a file of type: :values.',
    ];

    $field_name = static::FIELD_NAME;

    return Validator::make($data, [
      $field_name => 'required|array',
      "$field_name.*" => "required|file|mimes:$valid_extensions_str|mimetypes:$mime_types_str|max:$max_filesize_bytes",
    ], $messages);
  }
}
