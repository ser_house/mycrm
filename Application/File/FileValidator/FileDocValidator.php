<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.03.2018
 * Time: 09:31
 */

namespace Application\File\FileValidator;


class FileDocValidator extends FileValidator {
	protected const MIMES = [
		'application/msword',
		'application/vnd.ms-excel',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'application/pdf',
		'image/jpeg',
		'image/png',
	];

	protected const FIELD_NAME = 'documents';
}
