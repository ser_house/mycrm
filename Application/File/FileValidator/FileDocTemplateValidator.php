<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.03.2018
 * Time: 09:31
 */

namespace Application\File\FileValidator;


class FileDocTemplateValidator extends FileValidator {
	protected const MIMES = [
		'application/msword',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
	];

	protected const FIELD_NAME = 'templates';
}
