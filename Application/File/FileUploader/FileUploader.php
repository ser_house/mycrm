<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.01.2018
 * Time: 10:47
 */

namespace Application\File\FileUploader;


use App\Model\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class FileUploader {
  protected const DESTINATION = 'uploads';

  public function __construct(
    protected readonly UploadedFile $uploadedFile,
    protected readonly int $user_id,
    protected readonly string $file_name = '') {

  }

  /**
   * @return File|Model
   */
  public function upload(): File|Model {
    $destination = static::DESTINATION;
    if (empty($this->file_name)) {
      $filename = $this->uploadedFile->getClientOriginalName();
    }
    else {
      $filename = $this->file_name;
    }

    $this->uploadedFile->storePubliclyAs("public/$destination", $filename);

    $data = [
      'user_id' => $this->user_id,
      'path' => $destination,
      'name' => $filename,
      'size' => $this->uploadedFile->getSize(),
      'mime' => $this->uploadedFile->getMimeType(),
      'uri' => "$destination/$filename",
    ];

    return File::create($data);
  }
}
