<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.01.2018
 * Time: 23:42
 */

namespace Application\File\FileUploader;


use Application\File\FileNameGenerator\DocTemplateFileNameGenerator;
use App\Model\Client\Client;
use App\Model\DocumentTemplate;
use DB;
use Illuminate\Http\UploadedFile;

class DocTemplateUploader extends FileUploader {
  protected const DESTINATION = 'uploads/templates';

  protected DocumentTemplate $template;


  /**
   * DocTemplateUploader constructor.
   *
   * @param UploadedFile $file
   * @param array $inputs
   * @param int $user_id
   * @param int $client_id
   * @param string $file_name
   */
  public function __construct(UploadedFile $file, array $inputs, int $user_id, int $client_id, string $file_name = '') {
    $client = Client::findOrFail($client_id);
    if (empty($inputs['weight'])) {
      $db_data = DB::select('SELECT MAX(weight) AS weight FROM document_template WHERE client_id = :client_id', ['client_id' => $client_id]);
      $weight = $db_data ? $db_data[0]->weight + 1 : 0;
    }
    else {
      $weight = (int)$inputs['weight'];
    }
    $data = [
      'client_id' => $client_id,
      'type_id' => $inputs['type_id'],
      'note' => $inputs['note'],
      'weight' => $weight,
    ];

    unset($inputs['type_id'], $inputs['note'], $inputs['weight']);

    $data['options'] = json_encode($inputs['options'], JSON_THROW_ON_ERROR);

    $this->template = new DocumentTemplate($data);

    if (empty($file_name)) {
      $ext = $file->getClientOriginalExtension();
      $filenameGenerator = new DocTemplateFileNameGenerator($client, $this->template, $ext);
      $file_name = $filenameGenerator->generate();
    }

    parent::__construct($file, $user_id, $file_name);
  }

  /**
   * @return DocumentTemplate
   */
  public function upload(): DocumentTemplate {
    $uploaded_file = parent::upload();

    $this->template->file_id = $uploaded_file->id;
    $this->template->save();

    return $this->template;
  }
}
