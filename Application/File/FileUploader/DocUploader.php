<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.01.2018
 * Time: 23:42
 */

namespace Application\File\FileUploader;

use Application\File\FileNameGenerator\DocFileNameGenerator;
use App\Model\Client\Client;
use App\Model\Document;
use DB;
use Illuminate\Http\UploadedFile;

class DocUploader extends FileUploader {
  protected const DESTINATION = 'uploads/documents';

  protected Document $document;

  /**
   * DocUploader constructor.
   *
   * @param UploadedFile $file
   * @param array $inputs
   * @param int $user_id
   * @param int $client_id
   * @param string $file_name
   */
  public function __construct(UploadedFile $file, array $inputs, int $user_id, int $client_id, string $file_name = '') {
    $client = Client::findOrFail($client_id);

    if (!isset($inputs['weight'])) {
      $weight = $this->getNextWeight($client_id);
    }
    else {
      $weight = (int)$inputs['weight'];
    }
    $data = [
      'client_id' => $client_id,
      'type_id' => $inputs['type_id'],
      'date' => $inputs['date'],
      'number' => empty($inputs['number']) ? null : $inputs['number'],
      'page_num' => empty($inputs['page_num']) ? 1 : $inputs['page_num'],
      'title' => empty($inputs['title']) ? '' : $inputs['title'],
      'weight' => $weight,
    ];

    $this->document = new Document($data);

    if (empty($file_name)) {
      $ext = $file->getClientOriginalExtension();
      $filenameGenerator = new DocFileNameGenerator($client, $this->document, $ext);
      $file_name = $filenameGenerator->generate();
    }

    parent::__construct($file, $user_id, $file_name);
  }

  /**
   * @return Document
   */
  public function upload(): Document {
    $uploaded_file = parent::upload();

    $this->document->file_id = $uploaded_file->id;
    $this->document->save();

    return $this->document;
  }

  /**
   * @param int $client_id
   *
   * @return int
   */
  private function getNextWeight(int $client_id): int {
    $sql = 'SELECT MAX(weight) + 1 AS weight
				FROM document
				WHERE client_id = :client_id';
    $db_result = DB::select($sql, ['client_id' => $client_id]);
    return $db_result[0]->weight ?? 0;
  }
}
