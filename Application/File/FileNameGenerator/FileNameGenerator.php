<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2018
 * Time: 03:41
 */

namespace Application\File\FileNameGenerator;

use App\Model\Client\Client;
use App\Model\Document;
use Application\Client\Document\DocumentType;

abstract class FileNameGenerator {
  protected DocumentType $documentType;

  /**
   * FileNameGenerator constructor.
   *
   * @param Client $client
   * @param int $type_id
   * @param string $ext
   */
  public function __construct(protected readonly Client $client, int $type_id, protected readonly string $ext) {
    $types = Document::types();
    $this->documentType = $types[$type_id];
  }

  /**
   * @return string
   */
  abstract public function generate(): string;
}
