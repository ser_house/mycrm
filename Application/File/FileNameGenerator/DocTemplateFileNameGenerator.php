<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2018
 * Time: 03:28
 */

namespace Application\File\FileNameGenerator;

use App\Model\Client\Client;
use App\Model\DocumentTemplate;
use Illuminate\Support\Facades\Storage;


class DocTemplateFileNameGenerator extends FileNameGenerator {
  /**
   * DocTemplateFileNameGenerator constructor.
   *
   * @param Client $client
   * @param DocumentTemplate $template
   * @param string $ext
   */
  public function __construct(Client $client, DocumentTemplate $template, string $ext) {
    parent::__construct($client, $template->type_id, $ext);
  }

  /**
   * @inheritDoc
   */
  public function generate(): string {
    $client_id = $this->client->id;
    $type_machine_name = $this->documentType->machine_name;

    $file_name = "{$type_machine_name}_{$client_id}";

    if (Storage::exists("public/uploads/templates/{$file_name}.{$this->ext}")) {
      $file_name .= '-' . time();
    }
    $file_name .= ".{$this->ext}";

    return $file_name;
  }
}
