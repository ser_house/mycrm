<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 01.04.2018
 * Time: 16:56
 */

namespace Application\File\FileNameGenerator;


use App\Model\Client\Client;
use App\Model\Document;


class DocFileNameGenerator extends FileNameGenerator {
  /**
   * DocFileNameGenerator constructor.
   *
   * @param Client $client
   * @param Document $document
   * @param string $ext
   */
  public function __construct(Client $client, protected readonly Document $document, string $ext) {
    parent::__construct($client, $document->type_id, $ext);
  }

  /**
   * @inheritDoc
   */
  public function generate(): string {
    $client_id = $this->client->id;
    $type_machine_name = $this->documentType->machine_name;

    $file_name = "{$type_machine_name}_{$client_id}";

    if (!empty($this->document->number)) {
      $file_name .= "_{$this->document->number}";
    }

    if (!empty($this->document->page_num)) {
      $file_name .= "-{$this->document->page_num}";
    }

    $file_name .= ".{$this->ext}";

    return $file_name;
  }
}
