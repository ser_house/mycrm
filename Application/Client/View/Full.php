<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.04.2020
 * Time: 9:58
 */


namespace Application\Client\View;


class Full extends Short {
  public string $created_at;
  public string $city;
  /** @var Contact[] */
  public array $contacts;
}
