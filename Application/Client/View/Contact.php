<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.04.2020
 * Time: 10:07
 */


namespace Application\Client\View;


class Contact {

  public string $name;
  public string $value;
  public string $note;
}
