<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.04.2020
 * Time: 9:58
 */


namespace Application\Client\View;


use App\Model\Client\Client;
use App\Model\Document;
use App\Model\Document as ModelDocument;
use DateTimeImmutable;
use Exception;

class Presenter {

  /**
   * @param Client $client
   *
   * @return Short
   */
  public function short(Client $client): Short {
    $view = new Short();
    $this->fillBaseView($view, $client);

    return $view;
  }

  /**
   * @param array $clients
   *
   * @return array
   */
  public function shortViews(array $clients): array {
    $views = [];
    foreach ($clients as $client) {
      $views[] = $this->short($client);
    }
    return $views;
  }

  /**
   * @param Client $client
   *
   * @return Full
   * @throws Exception
   */
  public function full(Client $client): Full {
    $view = new Full();
    $this->fillBaseView($view, $client);

    $createdAt = new DateTimeImmutable($client->created_at);
    $view->created_at = $createdAt->format('d.m.Y H:i');
    $view->city = $client->city ?: 'не указан';
    $view->contacts = [];
    foreach ($client->contacts as $contact) {
      $contactView = new Contact();
      $contactView->name = $contact->name;
      $contactView->value = $contact->value;
      $contactView->note = (string)$contact->note;
      $view->contacts[] = $contactView;
    }

    return $view;
  }

  /**
   * @param ModelDocument $document
   *
   * @return Document
   */
  public function document(ModelDocument $document): Document {
    $view = new Document();
    $view->url = asset("storage/{$document->file->uri}");
    // Если у документа указано поле title, значит берем его.
    // Если не указано, то берем тип документа и
    //   если указан номер, то добавляем к строке с типом.
    if (!empty($document->title)) {
      $view->title = (string)$document->title;
    }
    else {
      $types = ModelDocument::types();
      $view->title = $types[$document->type_id]->title;
      if (!empty($document->number)) {
        $view->title .= " №{$document->number}";
      }
    }

    $view->created_at = date('d.m.Y H:i:s', strtotime($document->file->created_at));
    $view->date = date('d.m.Y', strtotime($document->date));

    return $view;
  }

  /**
   * @param ModelDocument[] $documents
   *
   * @return array
   */
  public function documents(array $documents): array {
    $views = [];

    foreach ($documents as $document) {
      $views[] = $this->document($document);
    }

    return $views;
  }

  /**
   * @param $view
   * @param $client
   */
  private function fillBaseView($view, $client): void {
    $view->id = $client->id;
    $view->name = $client->name;
    $view->note = (string)$client->note;
    $view->is_active = $client->is_selectable;
    $view->active = $view->is_active ? 'Да' : 'Нет';
  }
}
