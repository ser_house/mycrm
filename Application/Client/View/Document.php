<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.04.2020
 * Time: 10:11
 */


namespace Application\Client\View;


class Document {

  public string $url;
  public string $title;
  public string $created_at;
  public string $date;
}
