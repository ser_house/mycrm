<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.04.2020
 * Time: 9:58
 */


namespace Application\Client\View;


class Short {

  public int $id;
  public string $name;
  public string $note;
  public bool $is_active;
  public string $active;
}
