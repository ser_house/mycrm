<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.07.2020
 * Time: 17:36
 */


namespace Application\Client\Document;

use App\Model\Document;
use Application\File\FileValidator\FileDocValidator;

class DataProvider {
  private array $types;
  private int $max_filesize;
  private array $help;

  public function __construct(
    private readonly FileService $fileService,
    private readonly FileDocValidator $fileDocValidator
  ) {
    $this->types = $this->fileDocValidator->getTypes();
    $this->max_filesize = $this->fileDocValidator->getMaxFileSize();
    $this->help = $this->fileDocValidator->getHelp();
  }


  /**
   *
   * @return array
   */
  public function rules(): array {
    return [
      'types' => $this->types,
      'max_filesize' => $this->max_filesize,
      'help' => $this->help,
    ];
  }

  /**
   * @param int $client_id
   *
   * @return array
   */
  public function getClientDocuments(int $client_id): array {
    $clientDocuments = Document::where('client_id', $client_id)
      ->with('file')->orderBy('weight', 'desc')->get()
    ;

    $items = [];
    foreach ($clientDocuments as $clientDocument) {
      $items[] = $this->fileService->getDocumentData($clientDocument);
    }

    return $items;
  }
}
