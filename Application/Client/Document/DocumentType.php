<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 01.04.2018
 * Time: 17:00
 */

namespace Application\Client\Document;


use JsonSerializable;

/**
 * Class DocumentType.
 *
 * VO типа документа.
 */
class DocumentType implements JsonSerializable {
  public function __construct(
    public readonly int $id,
    public readonly string $machine_name,
    public readonly string $title
  ) {

  }

  public function getTitleForMulti(): string {
    return match ($this->machine_name) {
      'job' => 'Задания',
      'act' => 'Акты',
      'bill' => 'Счета',
      default => $this->title,
    };
  }

  public function equalsTo(DocumentType $type): bool {
    return $this->id === $type->id;
  }

  /**
   * Specify data which should be serialized to JSON
   *
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  public function jsonSerialize(): array {
    return [
      'id' => $this->id,
      'machine_name' => $this->machine_name,
      'title' => $this->title,
    ];
  }
}
