<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 31.03.2018
 * Time: 23:06
 */

namespace Application\Client\Document;

use Application\File\FileUploader\DocUploader;
use Application\File\FileValidator\FileDocValidator;
use App\Model\Document;
use Illuminate\Http\UploadedFile;
use RuntimeException;
use Throwable;


class ClientDocumentService {
  public function __construct(
    private readonly FileService $fileService,
    private readonly FileDocValidator $fileDocValidator
  ) {
  }

  /**
   * @param UploadedFile[] $uploadedFiles
   * @param array $all_inputs
   * @param array $data_inputs
   * @param int $user_id
   * @param int $client_id
   *
   * @return array
   */
  public function upload(array $uploadedFiles, array $all_inputs, array $data_inputs, int $user_id, int $client_id): array {
    $response_files = [];

    $fileValidator = $this->fileDocValidator->getValidator($all_inputs);
    if (!$fileValidator->fails()) {
      foreach ($uploadedFiles as $index => $file) {
        try {
          if (Document::UNDEFINED_TYPE == $data_inputs[$index]['type_id']) {
            throw new RuntimeException('Не указан тип документа.');
          }
          $uploader = new DocUploader($file, $data_inputs[$index], $user_id, $client_id);
          $id = $uploader->upload()->id;
          $clientDocument = Document::where('id', $id)->with('file')->get()->first();
          $response_files[] = $this->fileService->getDocumentData($clientDocument);
        }
        catch (Throwable $e) {
          $response_files[] = $this->fileService->getFileErrors($file, [$e->getMessage()]);
        }
      }
    }
    else {
      $errors = $fileValidator->errors();
      foreach ($uploadedFiles as $index => $file) {
        $file_errors = $errors->get("documents.$index");
        $response_files[] = $this->fileService->getFileErrors($file, $file_errors);
      }
    }

    return $response_files;
  }

  /**
   * @param int $id
   * @param array $inputs
   */
  public function update(int $id, array $inputs): void {
    if (Document::UNDEFINED_TYPE == $inputs['type_id']) {
      throw new RuntimeException('Не указан тип документа.');
    }
    /** @var Document $document */
    $document = Document::findOrFail($id);
    $document->type_id = $inputs['type_id'];
    $document->date = $inputs['date'];
    $document->number = $inputs['number'];
    $document->page_num = $inputs['page_num'];
    $document->title = $inputs['title'];
    $document->save();
  }
}
