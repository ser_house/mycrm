<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 31.03.2018
 * Time: 23:01
 */

namespace Application\Client\Document\Template;

use Application\File\FileUploader\DocTemplateUploader;
use Application\File\FileValidator\FileDocTemplateValidator;
use App\Model\Document;
use App\Model\DocumentTemplate;
use Application\Client\Document\FileService;
use Illuminate\Http\UploadedFile;
use RuntimeException;
use Throwable;


/**
 * Class DocumentTemplateService
 *
 * @package App\Service
 */
class DocumentTemplateService {
  private array $types;
  private int $max_filesize;
  private array $help;


  public function __construct(
    private readonly FileService $fileService,
    private readonly FileDocTemplateValidator $docTemplateValidator
  ) {
    $this->types = $this->docTemplateValidator->getTypes();
    $this->max_filesize = $this->docTemplateValidator->getMaxFileSize();
    $this->help = $this->docTemplateValidator->getHelp();
  }

  /**
   *
   * @return array
   */
  public function rules(): array {
    return [
      'types' => $this->types,
      'max_filesize' => $this->max_filesize,
      'help' => $this->help,
    ];
  }

  /**
   * @param int $client_id
   *
   * @return array
   */
  public function getClientDocumentTemplates(int $client_id): array {
    $documentTemplates = DocumentTemplate::where('client_id', $client_id)
      ->with('file')->orderBy('weight', 'desc')->get()
    ;
    $items = [];
    foreach ($documentTemplates as $documentTemplate) {
      $items[] = $this->fileService->getDocumentTemplateData($documentTemplate);
    }

    return $items;
  }

  /**
   * @param UploadedFile[] $uploadedFiles
   * @param array $all_inputs
   * @param array $data_inputs
   * @param int $user_id
   * @param int $client_id
   *
   * @return array
   */
  public function upload(array $uploadedFiles, array $all_inputs, array $data_inputs, int $user_id, int $client_id): array {
    $response_files = [];

    $fileValidator = $this->docTemplateValidator->getValidator($all_inputs);
    if (!$fileValidator->fails()) {
      foreach ($uploadedFiles as $index => $file) {
        try {
          if (Document::UNDEFINED_TYPE == $data_inputs[$index]['type_id']) {
            throw new RuntimeException('Не указан тип шаблона.');
          }
          $uploader = new DocTemplateUploader($file, $data_inputs[$index], $user_id, $client_id);
          $id = $uploader->upload()->id;
          $documentTemplate = DocumentTemplate::where('id', $id)->with('file')->get()->first();
          $response_files[] = $this->fileService->getDocumentTemplateData($documentTemplate);
        }
        catch (Throwable $e) {
          $response_files[] = $this->fileService->getFileErrors($file, [$e->getMessage()]);
        }
      }
    }
    else {
      $errors = $fileValidator->errors();
      foreach ($uploadedFiles as $index => $file) {
        $file_errors = $errors->get("templates.$index");
        $response_files[] = $this->fileService->getFileErrors($file, $file_errors);
      }
    }

    return $response_files;
  }

  /**
   * @param int $id
   * @param array $inputs
   */
  public function update(int $id, array $inputs): void {
    if (Document::UNDEFINED_TYPE == $inputs['type_id']) {
      throw new RuntimeException('Не указан тип шаблона.');
    }
    /** @var DocumentTemplate $template */
    $template = DocumentTemplate::findOrFail($id);
    $template->type_id = $inputs['type_id'];
    $template->note = $inputs['note'];
    unset($inputs['type_id'], $inputs['note'], $inputs['id']);
    if (isset($inputs['options'])) {
      $template->options = json_encode($inputs['options'], JSON_THROW_ON_ERROR);
    }
    else {
      $template->options = json_encode($inputs, JSON_THROW_ON_ERROR);
    }
    $template->save();
  }
}
