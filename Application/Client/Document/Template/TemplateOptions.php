<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.02.2018
 * Time: 13:03
 */

namespace Application\Client\Document\Template;

use JsonSerializable;

class TemplateOptions implements JsonSerializable {
  private array $settings;

  public function __construct(array $options, private readonly array $date_formats) {
    $this->settings = [
      'append_hours_suffix' => false,
      'human_amount_upfirst' => false,
      'human_amount_append_units_suffix' => false,
      'date_format' => key($this->date_formats),
    ];

    foreach ($this->settings as $key => $default_value) {
      if (isset($options[$key])) {
        $this->settings[$key] = $options[$key];
      }
    }
  }


  /**
   *
   * @return mixed
   */
  public function jsonSerialize(): mixed {
    return $this->settings;
  }

  /**
   *
   * @return array [format(string) => example(string)]
   */
  public function getDateFormats(): array {
    return $this->date_formats;
  }

  /**
   * @param $key
   *
   * @return mixed
   */
  public function get($key): mixed {
    return $this->settings[$key] ?? null;
  }

  /**
   * @return array
   */
  public function getKeys(): array {
    return array_keys($this->settings);
  }

  /**
   *
   * @return array[]
   */
  public function definitions(): array {
    return [
      'append_hours_suffix' => [
        'title' => 'выводить ли "час", "часа" и проч. в строке услуги',
        'default_value' => $this->get('append_hours_suffix'),
      ],
      'human_amount_upfirst' => [
        'title' => 'делать ли строчной первую букву суммы прописью',
        'default_value' => $this->get('human_amount_upfirst'),
      ],
      'human_amount_append_units_suffix' => [
        'title' => 'выводить ли "рубль", "рублей" и проч. в сумме прописью',
        'default_value' => $this->get('human_amount_append_units_suffix'),
      ],
      'date_format' => [
        'title' => 'Формат даты',
        'options' => $this->getDateFormats(),
        'default_value' => $this->get('date_format'),
      ],
    ];
  }
}
