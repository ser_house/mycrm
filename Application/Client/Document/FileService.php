<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 18.04.2020
 * Time: 19:38
 */


namespace Application\Client\Document;


use Application\File\FileValidator\FileDocTemplateValidator;
use Application\File\FileValidator\FileDocValidator;
use App\Model\Document;
use App\Model\DocumentTemplate;
use App\Model\File;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Illuminate\Http\UploadedFile;

class FileService {
  public function __construct(
    private readonly FileDocValidator $docValidator,
    private readonly FileDocTemplateValidator $docTemplateValidator,
    private readonly DateTimeFormatter $dateTimeFormatter
  ) {

  }


  /**
   * @param DocumentTemplate $documentTemplate
   *
   * @return array
   */
  public function getDocumentTemplateData(DocumentTemplate $documentTemplate): array {
    $file = $documentTemplate->file;
    $doc_types = Document::types();
    $type_id = $documentTemplate->type_id;

    $types = $this->docTemplateValidator->getTypes();
    $client_id = $documentTemplate->client_id;

    $date_formats = $this->dateTimeFormatter->getDateFormats();
    return [
      'id' => $documentTemplate->id,
      'weight' => $documentTemplate->weight,
      'type' => $doc_types[$type_id],
      'note' => $documentTemplate->note,
      'options' => $documentTemplate->getOptions($date_formats),
      'update_url' => route('client.documents.template.update', ['client' => $client_id]),
      'file' => $this->getFileData($file, $types[$file->mime]),
    ];
  }

  /**
   * @param Document $document
   *
   * @return array
   */
  public function getDocumentData(Document $document): array {
    $file = $document->file;
    $doc_types = Document::types();
    $type_id = $document->type_id;
    $client_id = $document->client_id;

    $types = $this->docValidator->getTypes();
    return [
      'id' => $document->id,
      'weight' => $document->weight,
      'type' => $doc_types[$type_id],
      'title' => $document->title,
      'date' => $document->date,
      'number' => $document->number,
      'page_num' => $document->page_num,
      'update_url' => route('client.document.update', ['client' => $client_id]),
      'file' => $this->getFileData($file, $types[$file->mime]),
    ];
  }

  /**
   * @param File $file
   * @param array $file_type
   *
   * @return array
   */
  public function getFileData(File $file, array $file_type): array {
    return [
      'id' => $file->id,
      'name' => $file->name,
      'type' => $file_type,
      'size' => $this->docValidator->fileSizeFormatter->format($file->size),
      'url' => asset("storage/{$file->uri}"),
      'delete_url' => route('files.delete', ['file' => $file->id]),
    ];
  }

  /**
   * @param UploadedFile $file
   * @param array $errors
   *
   * @return array
   */
  public function getFileErrors(UploadedFile $file, array $errors): array {
    return [
      'name' => $file->getClientOriginalName(),
      'error' => implode(PHP_EOL, $errors),
    ];
  }
}
