<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.07.2020
 * Time: 16:54
 */


namespace Application\Client\Action;


use App\Model\Client\Client;
use Application\Client\ClientSaver;

class Update {
  public function __construct(private readonly ClientSaver $clientSaver) {

  }

  /**
   * @param array $data
   * @param int $user_id
   * @param int $client_id
   *
   * @return Client
   */
  public function run(array $data, int $user_id, int $client_id): Client {
    $client = Client::findOrFail($client_id);

    $data['user_id'] = $user_id;
    $data['is_selectable'] = $data['is_selectable'] ?? false;

    return $this->clientSaver->saveClient($client, $data);
  }
}
