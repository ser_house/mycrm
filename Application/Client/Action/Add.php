<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2020
 * Time: 9:33
 */


namespace Application\Client\Action;


use App\Model\Client\Client;
use Application\Client\ClientSaver;

class Add {
  public function __construct(private readonly ClientSaver $clientSaver) {

  }

  /**
   * @param array $data
   * @param int $user_id
   *
   * @return Client
   */
  public function run(array $data, int $user_id): Client {
    $client = new Client();

    $data['user_id'] = $user_id;
    $data['is_selectable'] = true;

    return $this->clientSaver->saveClient($client, $data);
  }
}
