<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.01.2018
 * Time: 18:04
 */

namespace Application\Client;


use App\Model\Client\Client;
use App\Model\Client\ClientContact;
use App\Model\Client\ClientSite;


class ClientSaver {

  /**
   * @param Client $client
   * @param array $data
   *
   * @return Client
   */
  public function saveClient(Client $client, array $data): Client {
    // @todo: что у нас тут с транзакцией? с ошибками при сохранении
    // контактов и сайтов?
    $client->fill($data);
    $client->exists = !empty($client->id);
    $client->save();

    foreach (['contacts', 'sites'] as $key) {
      $this->processClientMultiProps($client, $data[$key] ?? [], $key);
    }

    return $client;
  }

  /**
   * @param array $contact
   *
   * @return bool
   */
  public function filterClientContactsEmpty(array $contact): bool {
    return !empty($contact['name']) && !empty($contact['value']);
  }

  /**
   * @param array $site
   *
   * @return bool
   */
  public function filterClientSitesEmpty(array $site): bool {
    return !empty($site['url']);
  }

  /**
   * @param Client $client
   * @param array $items
   * @param string $prop_name
   */
  private function processClientMultiProps(Client $client, array $items, string $prop_name): void {
    if (!empty($items)) {

      switch ($prop_name) {
        case 'contacts':
          $filter_callback = 'filterClientContactsEmpty';
          $item_class = ClientContact::class;
          break;

        case 'sites':
          $filter_callback = 'filterClientSitesEmpty';
          $item_class = ClientSite::class;
          break;

        default:
          return;
      }
      $items = array_filter($items, [$this, $filter_callback]);

      $updated = [];
      $to_add = [];

      foreach ($items as $item) {
        // Обновление обновленных (на самом деле просто перезапись всех тех текущих,
        // для которых пришли данные с формы - даже если они и не изменились).
        if (!empty($item['id'])) {
          $id = $item['id'];
          $client->$prop_name()->find($id)->update($item);
          // Это существующие, пригодятся при определении удаленных.
          $updated[$id] = $id;
        }
        else {
          $to_add[] = new $item_class($item);
        }
      }

      // Удаление удаленных.
      foreach ($client->$prop_name as $exist_item) {
        $exist_id = $exist_item->id;
        if (!isset($updated[$exist_id])) {
          $item_class::find($exist_id)?->delete();
        }
      }

      // Добавление добавленных.
      if (!empty($to_add)) {
        $client->$prop_name()->saveMany($to_add);
      }
    }
    else {
      $client->$prop_name()->delete();
    }
  }
}
