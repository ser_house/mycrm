<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2020
 * Time: 9:34
 */


namespace Application\Client;


use App\Model\Client\Client;
use App\Model\Task\TaskTemplate;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Auth;
use DB;

class DataProvider {
  /**
   * @param Client $client
   * @param int $per_page
   *
   * @return LengthAwarePaginator
   */
  public function getClientShowDocuments(Client $client, int $per_page): LengthAwarePaginator {
    // @todo: дублирование с \Application\Client\Document\DataProvider::getClientDocuments()
    return $client->documents()->with('file')->orderBy('date', 'desc')->paginate($per_page);
  }

  /**
   * @param string $active
   * @param int|null $user_id
   *
   * @return array
   */
  public function getUserClients(string $active, int $user_id = null): array {
    if (null === $user_id) {
      $user_id = Auth::user()->id;
    }

    $sql = "SELECT 
		  client.*
    FROM client
      INNER JOIN task ON task.client_id = client.id
    WHERE 
      client.user_id = :user_id AND client.is_selectable = :is_selectable
      AND task.id = (SELECT MIN(task.id) FROM task WHERE client_id = client.id)
    ORDER BY task.id DESC";
    $data = DB::select($sql, ['user_id' => $user_id, 'is_selectable' => ('active' === $active)]);

    // Модельные коллекции сортируют по-своему, поэтому - ручками, без коллекций.
    // А вообще надо dto.
    $clients = [];
    foreach ($data as $datum) {
      $client = new Client((array)$datum);
      $client->id = $datum->id;
      $clients[] = $client;
    }
    return $clients;
  }

  /**
   * @param int $client_id
   *
   * @return array
   */
  public function getClientSites(int $client_id): array {
    $sql = 'SELECT
			id,
			url
		FROM client_site
		WHERE client_id = :client_id
		ORDER BY url';
    $items = DB::select($sql, [$client_id]);
    return $items ?: [];
  }

  /**
   * @param int $client_id
   * @param int $state
   *
   * @return Collection
   */
  public function getClientTaskTemplates(int $client_id, int $state): Collection {
    $where = [['client_id', $client_id]];
    if (TaskTemplate::STATE_ACTIVE === $state) {
      $where[] = ['is_active', TaskTemplate::STATE_ACTIVE];
    }
    return TaskTemplate::where($where)->get();
    // @todo: добавить шаблону время создания и сортировать по нему по убыванию.
//		return TaskTemplate::where($where)->orderBy('weight', 'asc')->get();
  }
}
