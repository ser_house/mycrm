<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.04.2018
 * Time: 10:23
 */

namespace Application\DocGenerator;

use App\Model\Task\SubTask as SubTaskModel;
use App\Model\Task\Task as TaskModel;
use Application\DocGenerator\Generator\SubTask;
use Application\DocGenerator\Generator\Task;
use Application\File\FileUploader\DocUploader;
use App\Model\Client\Client;
use App\Model\Document;
use App\Model\DocumentTemplate;
use Application\Client\Document\DocumentType;
use Application\DocGenerator\Generator\DocData;
use Application\DocGenerator\Generator\TBS\Generator;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Task\DocumentsStorage;
use Application\Task\State;
use DB;
use Exception;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;


/**
 * Class Service
 *
 * @package App\Service
 */
class Service {

  public function __construct(
    private readonly Generator $generator,
    private readonly DateTimeFormatter $dateTimeFormatter,
    private readonly DocumentsStorage $documentsStorage,
  ) {

  }

  /**
   * @param int $client_id
   *
   * @return bool
   */
  public function hasTemplatesForGenerator(int $client_id): bool {
    return DocumentTemplate::where('client_id', $client_id)->limit(1)->exists();
  }

  /**
   * @param Client $client
   *
   * @return DocumentTemplate[]|Collection
   */
  public function getTemplatesForGenerator(Client $client): array|Collection {
    return DocumentTemplate::where('client_id', $client->id)
      ->with('file')->orderBy('weight', 'desc')->get()
    ;
  }

  /**
   * @param string $client_id
   *
   * @return array
   */
  public function getClientTaskYears(string $client_id): array {
    $sql = "SELECT DISTINCT 
			YEAR(completed_at) AS year
		FROM task
		WHERE client_id = :client_id AND completed_at IS NOT NULL
		ORDER BY YEAR(completed_at) ASC";
    $data = DB::select($sql, ['client_id' => $client_id]);

    return array_column($data, 'year');
  }

  /**
   * @param int $client_id
   *
   * @return bool
   */
  public function hasItemsForGenerateFinished(int $client_id): bool {
    $sql = "SELECT 
			COUNT(*) AS num
		FROM task
		WHERE client_id = :client_id AND state = 'finished'
		LIMIT 1";
    $data = DB::select($sql, ['client_id' => $client_id]);
    return !empty($data) && !empty($data[0]->num);
  }

  /**
   * @param int $client_id
   *
   * @return Tasks
   */
  public function getItemsForGeneratorDefault(int $client_id): Tasks {
    return $this->getTasksForClientDocumentsGenerator($client_id, State::Finished);
  }

  /**
   * @param int $client_id
   *
   * @return Tasks
   */
  public function getItemsForGeneratorClosed(int $client_id): Tasks {
    return $this->getTasksForClientDocumentsGenerator($client_id, State::Closed);
  }

  /**
   * @param int $client_id
   * @param State $state
   *
   * @return Tasks
   */
  private function getTasksForClientDocumentsGenerator(int $client_id, State $state): Tasks {
    $sql = "SELECT 
			task.id AS task_id,
			task.launched_at AS launched_at,
			task.completed_at AS completed_at,
			task.title AS task_title,
			task.currency AS task_currency,
			st.id AS item_id,
			st.title AS item_title,
			st.budget AS item_budget,
			st.hours AS item_hours
		FROM task
			INNER JOIN sub_task AS st ON st.task_id = task.id
			LEFT JOIN task_document AS td ON td.task_id = task.id
		WHERE td.id IS NULL 
		  AND task.client_id = :client_id 
		  AND task.state = '$state->value'
		ORDER BY task.launched_at ASC";
    $rows = DB::select($sql, ['client_id' => $client_id]);

    return new Tasks($rows);
  }

  /**
   * @param Client $client
   * @param Collection $templates
   *
   * @return int
   */
  public function getNextDocNum(Client $client, Collection $templates): int {
    $type_ids = [];
    foreach ($templates as $template) {
      $type_ids[] = $template->type_id;
    }
    // @todo: не types, а type - последний номер документа конкретного типа.
    // Потому что хоть акт и счет и должны идти ноздря в ноздрю по номерам - а вдруг нет?
    // Да и не логично использовать заранее и жестко один номер.
    $params = [['client_id', $client->id], ['type_id', $type_ids]];
    /** @var Document $document */
    $document = Document::where($params)->orderBy('id', 'desc')->limit(1)->first();

    $new_number = 1;
    if (!empty($document->number) && is_numeric($document->number)) {
      $new_number = $document->number + 1;
    }

    return $new_number;
  }

  /**
   * @param int $client_id
   * @param DocInput[] $doc_inputs
   *
   * @return DocData[]
   */
  public function buildDocumentsForGenerate(int $client_id, array $doc_inputs): array {
    $docs_data = [];
    foreach ($doc_inputs as $docInput) {
      $tasks = [];
      $currency = '';
      foreach ($docInput->task_inputs as $taskInput) {
        $taskModel = TaskModel::firstWhere('id', $taskInput->task_id);
        $task = Task::buildFromModel($taskModel);

        if (empty($currency)) {
          $currency = $task->currency;
        }


        /** @var SubTaskModel[] $subTasks */
        $subTasks = SubTaskModel::where('task_id', $task->id)->get()->sortBy('id');
        foreach ($subTasks as $subTaskModel) {
          if ($taskInput->hasSubTaskIds() && !in_array($subTaskModel->id, $taskInput->sub_task_ids)) {
            continue;
          }
          $subTask = SubTask::buildFromModel($subTaskModel, $currency);
          $task->addSubTask($subTask);
        }

        $tasks[] = $task;
      }

      $docs_data[] = new DocData($client_id, $docInput->doc_num, $docInput->date, $tasks);
    }

    return $docs_data;
  }

  /**
   * @param DocData $docData
   * @param Client $client
   * @param DocumentTemplate $template
   * @param bool $save_as_document
   * @param bool $to_pdf
   *
   * @return GeneratedFile
   */
  public function generateFromTemplate(DocData $docData, Client $client, DocumentTemplate $template, bool $save_as_document, bool $to_pdf): GeneratedFile {
    $documentTypes = Document::types();
    $documentType = $documentTypes[$template->type_id];

    $tmp_file_path = $this->generator->generate($template, $docData, $this->dateTimeFormatter->getDateFormats(), $client);

    if ($to_pdf) {
      $out_dir = dirname($tmp_file_path);
      $cmd = "lowriter --convert-to pdf:writer_pdf_Export $tmp_file_path --outdir $out_dir";
      $cmd = escapeshellcmd($cmd);

      $output = null;
      $retval = null;
      putenv('PATH=/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin');
      putenv('HOME=/tmp');
      $result = exec($cmd, $output, $retval);

      $file = basename($tmp_file_path) . '.pdf';
      unlink($tmp_file_path);
      $tmp_file_path = "$out_dir/$file";
      if (!file_exists($tmp_file_path)) {
        throw new Exception("Файл не существует '$tmp_file_path', result: " . '<pre>' . print_r([
            '$cmd' => $cmd,
            'user' => get_current_user(),
            '$tmp_file_path' => $tmp_file_path,
            '$out_dir' => $out_dir,
            '$result' => $result,
            'output' => $output,
            'retval' => $retval,
          ], true) . '</pre>');
      }
    }

    $document = null;
    if ($save_as_document) {
      $file = new File($tmp_file_path, false);
      $uploadedFile = new UploadedFile($file->getRealPath(), $file->getFilename(), $file->getMimeType());
      $inputs = $this->buildUploadInputs($documentType, $docData);
      $ext = $file->guessExtension();
      if (empty($ext)) {
        throw new Exception('Не удалось определить расширение документа.');
      }

      $output_file_name = $this->generateRealFileName($documentType, $client, $docData, $ext);
      $docUploader = new DocUploader($uploadedFile, $inputs, $client->user_id, $client->id, $output_file_name);
      $document = $docUploader->upload();

      unlink($tmp_file_path);

      foreach ($docData->tasks as $task) {
        $this->documentsStorage->add($task->id, $document->id);
      }

      $file_uri = $document->file->uri;
      $file_path = Storage::path("public/{$document->file->path}");
    }
    else {
      $file_uri = $tmp_file_path;
      $file_path = $tmp_file_path;
    }

    $file_name = "$documentType->title №$docData->num";
    return new GeneratedFile($file_name, $file_uri, $file_path, $document);
  }

  /**
   * @param DocumentType $docType
   * @param DocData $docData
   *
   * @return array
   */
  private function buildUploadInputs(DocumentType $docType, DocData $docData): array {
    $date = $docData->date;
    $doc_number = $docData->num;

    return [
      'type_id' => $docType->id,
      'date' => $this->dateTimeFormatter->dateToDb($date),
      'number' => $doc_number,
      'title' => "$docType->title №$doc_number",
    ];
  }

  /**
   * @param DocumentType $docType
   * @param Client $client
   * @param DocData $docData
   * @param string $ext
   *
   * @return string
   */
  private function generateRealFileName(DocumentType $docType, Client $client, DocData $docData, string $ext): string {
    $prefix = $docType->machine_name;
    $doc_number = $docData->num;

    return "{$prefix}_{$client->id}_{$doc_number}.$ext";
  }

  /**
   * @param DocumentType $docType
   * @param DocData $docData
   * @param string $file_name_pattern
   *
   * @return string
   */
  private function generateUserFileName(DocumentType $docType, DocData $docData, string $file_name_pattern): string {
    $file_name = str_replace('{doc_type}', $docType->machine_name, $file_name_pattern);
    $doc_num = $docData->num;
    return str_replace('{doc_num}', $doc_num, $file_name);
  }
}
