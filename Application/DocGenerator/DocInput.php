<?php

namespace Application\DocGenerator;

use DateTimeImmutable;
use Exception;

class DocInput {

  /**
   * @param DateTimeImmutable $date
   * @param int $doc_num
   * @param TaskInput[] $task_inputs
   */
  public function __construct(
    public readonly DateTimeImmutable $date,
    public readonly int $doc_num,
    public readonly array $task_inputs
  ) {
  }

  /**
   * @param array $doc
   *
   * @return static
   * @throws Exception
   */
  public static function fromArray(array $doc): self {
    $date = new DateTimeImmutable($doc['date']);
    $num = $doc['num'];
    $task_inputs = [];
    foreach ($doc['tasks'] as $task_data) {
      $task_inputs[] = new TaskInput($task_data['id'], $task_data['sub_task_ids'] ?? []);
    }
    return new self(date: $date, doc_num: $num, task_inputs: $task_inputs);
  }
}
