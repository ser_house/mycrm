<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.05.2020
 * Time: 8:34
 */


namespace Application\DocGenerator;

use App\Model\Document;

class GeneratedFile {

  public function __construct(
    public readonly string $file_name,
    public readonly string $file_uri,
    public readonly string $file_path,
    public readonly ?Document $document = null,
  ) {

  }
}
