<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.05.2020
 * Time: 10:24
 */


namespace Application\DocGenerator;

use DateTimeImmutable;

/**
 * Class Items
 *
 * @package Application\DocGenerator
 */
class Tasks {

  private array $items = [];

  /**
   * Items constructor.
   *
   * @param iterable $db_result
   */
  public function __construct(iterable $db_result) {
    $tasks_by_id = [];
    foreach ($db_result as $row) {
      $task_id = $row->task_id;
      if (!isset($tasks_by_id[$task_id])) {
        $tasks_by_id[$task_id] = [
          'id' => $task_id,
          'title' => $row->task_title,
          'launched_at' => $row->launched_at,
          'completed_at' => $row->completed_at,
          'currency' => $row->task_currency,
          'items' => [],
        ];
      }
      $item = [
        'id' => $row->item_id,
        'title' => $row->item_title,
        'budget' => $row->item_budget,
        'hours' => $row->item_hours,
        'enabled' => true,
      ];
      $tasks_by_id[$task_id]['items'][] = $item;
    }

    foreach ($tasks_by_id as $task_data) {
      $completedAt = new DateTimeImmutable($task_data['completed_at']);
      $task_year_month = $completedAt->format('Y-m');
      // Чтобы не приходилось выбирать и вообще видеть единственную подзадачу "Основная".
      if (1 === count($task_data['items'])) {
        $task_data['budget'] = $task_data['items'][0]['budget'];
        $task_data['hours'] = $task_data['items'][0]['hours'];
      }
      $this->items[$task_year_month][] = $task_data;
    }
  }

  /**
   * @return array
   */
  public function getItems(): array {
    return $this->items;
  }

  /**
   * @return array
   */
  public function getTaskIds(): array {
    if ($this->isEmpty()) {
      return [];
    }
    $ids = [];
    foreach ($this->items as $year_month_items) {
      $ids[] = array_column($year_month_items, 'id');
    }
    return array_merge(...$ids);
  }

  /**
   *
   * @return bool
   */
  public function isEmpty(): bool {
    return empty($this->items);
  }
}
