<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.05.2018
 * Time: 13:46
 */

namespace Application\DocGenerator\Generator\Vars;

class ItemLevelVars extends VariablesBase {
  public const KEY = 'item';
  protected const LABEL = 'Переменные одной выполненной работы';
  protected const DESCRIPTION = '';

  public const NUM = 'num';
  public const TITLE = 'title';
  public const HOURS = 'hours';
  public const AMOUNT = 'amount';

  public const BASE_VARS = [
    self::NUM => 'порядковый номер услуги',
    self::TITLE => 'наименование услуги',
    self::HOURS => 'кол-во часов',
    self::AMOUNT => 'сумма по услуге',
  ];
}
