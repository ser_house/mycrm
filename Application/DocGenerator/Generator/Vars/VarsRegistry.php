<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.05.2018
 * Time: 11:34
 */

namespace Application\DocGenerator\Generator\Vars;

use Application\DocGenerator\Generator\IVariables;
use Iterator;

class VarsRegistry implements Iterator {
  /** @var IVariables[] */
  private array $variables;

  /**
   * VarsRegistry constructor.
   */
  public function __construct() {
    $this->add(new DocLevelVars());
    $this->add(new ItemLevelVars());
  }

  /**
   * @param IVariables $variables
   */
  public function add(IVariables $variables): void {
    $key = $variables->getKey();
    $this->variables[$key] = $variables;
  }

  /**
   * @inheritDoc
   */
  public function current(): mixed {
    return current($this->variables);
  }

  /**
   * @inheritDoc
   */
  public function next(): mixed {
    return next($this->variables);
  }

  /**
   * @inheritDoc
   */
  public function key(): mixed {
    return key($this->variables);
  }

  /**
   * @inheritDoc
   */
  public function valid(): bool {
    $key = key($this->variables);
    return !empty($key);
  }

  /**
   * @inheritDoc
   */
  public function rewind(): void {
    reset($this->variables);
  }

  /**
   * @return array
   */
  public function allVariablesWithDefaultValues(): array {
    $keys = [];

    foreach ($this->variables as $variables) {
      $keys = array_merge($keys, $variables->getAllKeys(IVariables::BLOCK_PREFIXED));
    }

    return array_fill_keys($keys, false);
  }

  /**
   *
   * @return DocLevelVars
   */
  public function getDocLevelVars(): DocLevelVars {
    return $this->variables[DocLevelVars::KEY];
  }

  /**
   *
   * @return ItemLevelVars
   */
  public function getItemLevelVars(): ItemLevelVars {
    return $this->variables[ItemLevelVars::KEY];
  }
}
