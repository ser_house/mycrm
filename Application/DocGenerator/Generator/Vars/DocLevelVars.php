<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.05.2018
 * Time: 13:45
 */

namespace Application\DocGenerator\Generator\Vars;

class DocLevelVars extends VariablesBase {
  public const KEY = 'doc';
  protected const LABEL = 'Переменные уровня документа';
  protected const DESCRIPTION = '';

  public const NUM = 'num';
  public const DATE = 'date';
  public const DATE_START = 'date_start';
  public const DATE_END = 'date_end';
  public const AMOUNT = 'amount';
  public const AMOUNT_HUMAN = 'amount_human';
  public const HOURS = 'hours';
  public const DAYS_COUNT = 'days_count';
  public const DAY_AMOUNT = 'day_amount';

  public const BASE_VARS = [
    self::NUM => 'номер документа',
    self::DATE => 'дата документа',
    self::DATE_START => 'дата начала периода',
    self::DATE_END => 'дата конца периода',
    self::AMOUNT => 'сумма итого',
    self::AMOUNT_HUMAN => 'сумма итого прописью',
    self::HOURS => 'кол-во часов итого',
    self::DAYS_COUNT => 'кол-во рабочих дней',
    self::DAY_AMOUNT => 'сумма в день',
  ];
}
