<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.05.2018
 * Time: 09:05
 */

namespace Application\DocGenerator\Generator\Vars;

use Application\Calendar\IHolidayDataProvider;
use Application\Client\Document\Template\TemplateOptions;
use Application\DocGenerator\Generator\DataItem;
use Application\DocGenerator\Generator\DocData;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Infrastructure\Formatter\Money as MoneyFormatter;
use Application\Money\Amount;
use DateInterval;
use DatePeriod;

class VarsPresentTBS {
  public function __construct(
    protected readonly IHolidayDataProvider $holidayDataProvider,
    protected readonly DateTimeFormatter $dateTimeFormatter,
    protected readonly MoneyFormatter $moneyFormatter
  ) {

  }

  /**
   * @param DocLevelVars $docLevelVars
   * @param TemplateOptions $options
   * @param DocData $docData
   *
   * @return array
   */
  public function presentDocLevelVars(DocLevelVars $docLevelVars, TemplateOptions $options, DocData $docData): array {
    $keys = $docLevelVars->getCurrentKeys();

    $date_format = $options->get('date_format');

    foreach ($keys as $key) {
      $value = null;

      switch ($key) {
        case DocLevelVars::NUM:
          $value = $docData->num;
          break;

        case DocLevelVars::DATE:
          $value = match ($date_format) {
            'd F Y г.' => $this->dateTimeFormatter->dateLong($docData->date),
            default => $this->dateTimeFormatter->dateShort($docData->date),
          };
          break;

        case DocLevelVars::DATE_START:
          $value = match ($date_format) {
            'd F Y г.' => $this->dateTimeFormatter->dateLong($docData->getStartDate()),
            default => $this->dateTimeFormatter->dateShort($docData->getStartDate()),
          };
          break;

        case DocLevelVars::DATE_END:
          $value = match ($date_format) {
            'd F Y г.' => $this->dateTimeFormatter->dateLong($docData->getEndDate()),
            default => $this->dateTimeFormatter->dateShort($docData->getEndDate()),
          };
          break;

        case DocLevelVars::AMOUNT:
          $amount = $docData->getTotalAmount();
          $value = $this->moneyFormatter->formatForDocument($amount);
          break;

        case DocLevelVars::AMOUNT_HUMAN:
          $show_units = $options->get('human_amount_append_units_suffix');
          $amount = $docData->getTotalAmount();
          $value = $this->moneyFormatter->formatHumanRu($amount->amount, $show_units);
          break;

        case DocLevelVars::HOURS:
          $value = $docData->getTotalHours();
          if ($options->get('append_hours_suffix')) {
            $value .= ' ' . morph($value, 'час', 'часа', 'часов');
          }
          break;

        case DocLevelVars::DAYS_COUNT:
          $value = $this->getWorkedDaysCount($docData);
          break;

        case DocLevelVars::DAY_AMOUNT:
          $worked_days_count = $this->getWorkedDaysCount($docData);

          $totalAmount = $docData->getTotalAmount();

          $amount = new Amount($totalAmount->amount / $worked_days_count, $totalAmount->currency);
          $value = $this->moneyFormatter->formatForDocument($amount);
          break;

        default:
          break;
      }

      if (!empty($value)) {
        $docLevelVars->setValue($key, $value);
      }
    }

    $vars = $docLevelVars->getVars();

    $data = [];
    foreach ($vars as $key => $item) {
      $data[$key] = $item['value'];
    }

    return $data;
  }

  /**
   * @param ItemLevelVars $itemLevelVars
   * @param DataItem $dataItem
   * @param TemplateOptions $options
   *
   * @return array
   */
  public function presentItemLevelVars(ItemLevelVars $itemLevelVars, DataItem $dataItem, TemplateOptions $options): array {
    $keys = $itemLevelVars->getCurrentKeys();

    foreach ($keys as $key) {

      $value = null;

      switch ($key) {
        case ItemLevelVars::NUM:
          $value = $dataItem->num;
          break;

        case ItemLevelVars::TITLE:
          $value = $dataItem->title;
          break;

        case ItemLevelVars::HOURS:
          $value = $dataItem->hours;
          if ($options->get('append_hours_suffix')) {
            $value .= ' ' . morph($value, 'час', 'часа', 'часов');
          }
          break;

        case ItemLevelVars::AMOUNT:
          $amount = $dataItem->amount;
          $value = $this->moneyFormatter->formatForDocument($amount);
          break;

        default:
          break;
      }

      if (!empty($value)) {
        $itemLevelVars->setValue($key, $value);
      }
    }

    $vars = $itemLevelVars->getVars();

    $data = [];
    foreach ($vars as $key => $item) {
      $data[$key] = $item['value'];
    }

    return $data;
  }

  /**
   * @param DocData $docData
   *
   * @return int
   */
  private function getWorkedDaysCount(DocData $docData): int {
    $worked_days_count = 0;
    $holidays = $this->holidayDataProvider->getDates($docData->client_id, $docData->getStartDate(), $docData->getEndDate());

    $dates = new DatePeriod($docData->getStartDate(), new DateInterval('P1D'), $docData->getEndDate());
    foreach ($dates as $date) {
      $date_str = $date->format('Y-m-d');
      if (!isset($holidays[$date_str])) {
        $worked_days_count++;
      }
    }

    return $worked_days_count;
  }
}
