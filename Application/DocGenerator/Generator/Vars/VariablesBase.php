<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.05.2018
 * Time: 08:42
 */

namespace Application\DocGenerator\Generator\Vars;

use App\Model\Client\ClientSettings;
use Application\DocGenerator\Generator\IKeyFormatter;
use Application\DocGenerator\Generator\IVariables;
use function in_array;

abstract class VariablesBase implements IVariables {
  public const KEY = '';
  protected const LABEL = '';
  protected const DESCRIPTION = '';

  public const BASE_VARS = [];

  /**
   *  текущие переменные.
   *  каждая переменная хранится в своём ключе и описывается массивом
   *  с ключами 'value' и 'title'.
   */
  protected array $vars = [];

  /**
   * VariablesBase constructor.
   *
   * @param array $var_keys
   */
  public function __construct(array $var_keys = []) {
    foreach (static::BASE_VARS as $key => $label) {
      if (!empty($var_keys) && !in_array($key, $var_keys)) {
        continue;
      }

      $this->vars[$key] = [
        'value' => null,
        'title' => $label,
      ];
    }
  }

  /**
   * @inheritDoc
   */
  public function getTitles(): array {
    $labels = [];
    foreach ($this->vars as $key => $var) {
      $labels[$key] = $var['title'];
    }

    return $labels;
  }

  /**
   * @inheritDoc
   */
  public function getCurrentKeys(): array {
    return array_keys($this->vars);
  }

  /**
   * @inheritDoc
   */
  public function getAllKeys($prefixed = IVariables::NOT_PREFIXED): array {
    $keys = array_keys(static::BASE_VARS);

    if (IVariables::BLOCK_PREFIXED === $prefixed) {
      foreach ($keys as &$key) {
        $key = static::KEY . ":$key";
      }
      unset($key);
    }

    return $keys;
  }

  /**
   * @inheritDoc
   */
  public function getLabel(): string {
    return static::LABEL;
  }

  /**
   * @inheritDoc
   */
  public function getDescription(): string {
    return static::DESCRIPTION;
  }

  /**
   * @inheritDoc
   */
  public function getKey(): string {
    return static::KEY;
  }

  /**
   * @inheritDoc
   */
  public function getVars(): array {
    return $this->vars;
  }

  /**
   * @inheritDoc
   */
  public function setValue(string $key, $value): void {
    $this->vars[$key]['value'] = $value;
  }

  /**
   * @param ClientSettings $clientSettings
   * @param IKeyFormatter $keyFormatter
   *
   * @return array
   */
  public function getSelectedClientSettingsKeys(ClientSettings $clientSettings, IKeyFormatter $keyFormatter): array {
    $selected_keys = [];

    $all_keys = $this->getAllKeys();
    $vars_key = $this->getKey();

    foreach ($all_keys as $key) {
      $id = $keyFormatter->formatId($vars_key, $key);
      $value = $clientSettings->get($id);
      if (!empty($value)) {
        $selected_keys[] = $key;
      }
    }

    return $selected_keys;
  }

  /**
   * @param ClientSettings $clientSettings
   * @param IKeyFormatter $keyFormatter
   *
   * @return $this
   */
  public function buildBySelectedClientSettings(ClientSettings $clientSettings, IKeyFormatter $keyFormatter): self {
    $selected_keys = $this->getSelectedClientSettingsKeys($clientSettings, $keyFormatter);
    return new static($selected_keys);
  }
}
