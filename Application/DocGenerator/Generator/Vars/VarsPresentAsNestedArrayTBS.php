<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.05.2018
 * Time: 09:05
 */

namespace Application\DocGenerator\Generator\Vars;

use Application\Client\Document\Template\TemplateOptions;
use Application\DocGenerator\Generator\DocData;


class VarsPresentAsNestedArrayTBS extends VarsPresentTBS{

  /**
   * @inheritDoc
   */
  public function presentDocLevelVars(DocLevelVars $docLevelVars, TemplateOptions $options, DocData $docData): array {
    $data = parent::presentDocLevelVars($docLevelVars, $options, $docData);
    return [$data];
  }
}
