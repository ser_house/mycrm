<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.05.2018
 * Time: 12:55
 */

namespace Application\DocGenerator\Generator\Vars\Settings;

use App\Model\Client\ClientSettings;
use Application\DocGenerator\Generator\IKeyFormatter;
use Application\DocGenerator\Generator\IVariables;


class Presenter {
  public function __construct(private readonly IKeyFormatter $keyFormatter) {

  }

  /**
   * @param ClientSettings $clientSettings
   * @param IBuilder $varSettingsBuilder
   *
   * @return array
   */
  public function presentAllLevelsVariables(ClientSettings $clientSettings, IBuilder $varSettingsBuilder): array {
    $varsRegistry = $clientSettings->getVarsRegistry();

    $vars = [];

    foreach ($varsRegistry as $variables) {
      $vars += $this->presentLevelAllVariables($clientSettings, $variables, $varSettingsBuilder);
    }

    return $vars;
  }

  /**
   * @param ClientSettings $clientSettings
   * @param IVariables $variables
   * @param IBuilder $varSettingsBuilder
   *
   * @return array
   */
  public function presentLevelAllVariables(ClientSettings $clientSettings, IVariables $variables, IBuilder $varSettingsBuilder): array {
    $vars = [];

    $var_titles = $variables->getTitles();
    $block_key = $variables->getKey();

    $vars[$block_key]['title'] = $variables->getLabel();
    $vars[$block_key]['vars'] = [];

    foreach ($var_titles as $var => $title) {
      $varSettingsBuilder->addVarSettings($vars, $clientSettings, $block_key, $var, $title);
    }

    return $vars;
  }

  /**
   * @return IKeyFormatter
   */
  public function getKeyFormatter(): IKeyFormatter {
    return $this->keyFormatter;
  }
}
