<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.06.2020
 * Time: 9:35
 */


namespace Application\DocGenerator\Generator\Vars\Settings;


use App\Model\Client\ClientSettings;
use Application\DocGenerator\Generator\IKeyFormatter;


class SelectedBuilder implements IBuilder {
  public function __construct(private readonly IKeyFormatter $keyFormatter) {

  }

  /**
   * @inheritDoc
   */
  public function addVarSettings(array &$vars, ClientSettings $clientSettings, string $block_key, string $var, string $title): void {
    $id = $this->keyFormatter->formatId($block_key, $var);
    $value = $clientSettings->get($id);
    if (!empty($value)) {
      $human_var = $this->keyFormatter->formatHuman($block_key, $var);
      $vars[$block_key]['vars'][$id] = new VarSettings($human_var, $title, $value);
    }
  }
}
