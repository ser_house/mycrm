<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.05.2018
 * Time: 11:55
 */

namespace Application\DocGenerator\Generator\Vars\Settings;


class VarSettings {
  public function __construct(
    public readonly string $var,
    public readonly string $title,
    public readonly mixed $value
  ) {
  }
}
