<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.06.2020
 * Time: 9:29
 */


namespace Application\DocGenerator\Generator\Vars\Settings;


use App\Model\Client\ClientSettings;

interface IBuilder {

  /**
   * @param array $vars
   * @param ClientSettings $clientSettings
   * @param string $block_key
   * @param string $var
   * @param string $title
   */
  public function addVarSettings(array &$vars, ClientSettings $clientSettings, string $block_key, string $var, string $title): void;
}
