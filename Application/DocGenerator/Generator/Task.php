<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.04.2018
 * Time: 08:40
 */

namespace Application\DocGenerator\Generator;

use App\Model\Task\Task as TaskModel;
use DateTimeImmutable;
use Exception;

/**
 * Class Task.
 *
 * @package Application\DocGenerator\Generator
 */
class Task {

  /**
   * Task constructor.
   *
   * @param int $id
   * @param string $title
   * @param string $description
   * @param string $currency
   * @param DateTimeImmutable $launchedAt
   * @param DateTimeImmutable $completedAt
   * @param SubTask[] $sub_tasks
   */
  public function __construct(public readonly int $id,
    public readonly string $title,
    public readonly string $description,
    public readonly string $currency,
    public readonly DateTimeImmutable $launchedAt,
    public readonly DateTimeImmutable $completedAt,
    public array $sub_tasks = []) {

  }

  /**
   * @param TaskModel $model
   *
   * @return static
   * @throws Exception
   */
  static public function buildFromModel(TaskModel $model): self {
    return new self(
      $model->id,
      $model->title,
      $model->description,
      $model->currency,
      new DateTimeImmutable($model->launched_at),
      new DateTimeImmutable($model->completed_at),
    );
  }

  /**
   * @param SubTask $subTask
   */
  public function addSubTask(SubTask $subTask): void {
    $this->sub_tasks[] = $subTask;
  }
}
