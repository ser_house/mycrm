<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.06.2020
 * Time: 3:26
 */


namespace Application\DocGenerator\Generator;

use App\Model\Task\SubTask as SubTaskModel;
use Application\Money\Amount;

class SubTask {

  /**
   * SubTask constructor.
   *
   * @param string $title
   * @param Amount $budget
   * @param float $hours
   */
  public function __construct(
    public readonly string $title,
    public readonly Amount $budget,
    public readonly float $hours
  ) {
  }

  /**
   * @param SubTaskModel $model
   * @param string $currency
   *
   * @return static
   */
  static public function buildFromModel(SubTaskModel $model, string $currency): self {
    return new self(
      $model->title,
      new Amount($model->budget, $currency),
      $model->hours ?? 0.0,
    );
  }
}
