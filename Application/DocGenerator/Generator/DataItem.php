<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.04.2018
 * Time: 09:04
 */

namespace Application\DocGenerator\Generator;


use Application\Money\Amount;

/**
 * Class DataItem.
 *
 * Класс для создания и хранения отдельной позиции для документов.
 *
 * @package Application\DocGenerator\Generator
 */
class DataItem {
  public readonly string $title;

  /**
   * DataItem constructor.
   *
   * @param int $num
   * @param string $title
   * @param Amount $amount
   * @param float $hours
   */
  public function __construct(
    public readonly int $num,
    string $title,
    public readonly Amount $amount,
    public readonly float $hours
  ) {
    $this->title = str_replace(["\r\n", '"'], [' ', ''], $title);
  }
}
