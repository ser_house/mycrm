<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.05.2018
 * Time: 13:00
 */

namespace Application\DocGenerator\Generator;


interface IKeyFormatter {
  /**
   * @param string $key
   * @param string $var
   *
   * @return string
   */
	public function formatId(string $key, string $var): string;

  /**
   * @param string $key
   * @param string $var
   *
   * @return string
   */
	public function formatHuman(string $key, string $var): string;
}
