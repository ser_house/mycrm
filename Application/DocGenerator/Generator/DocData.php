<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.04.2018
 * Time: 12:21
 */

namespace Application\DocGenerator\Generator;


use Application\Money\Amount;
use DateTimeImmutable;


/**
 * Class DocData.
 *
 * Агрегатор для хранения данных, необходимых для генерации документа.
 *
 * @package Application\DocGenerator\Generator
 */
class DocData {

  protected ?DateTimeImmutable $startDate = null;
  protected ?DateTimeImmutable $endDate = null;
  protected string $currency = '';
  protected Amount $totalAmount;
  protected float $total_hours = 0.00;
  protected array $flatted_items = [];

  /**
   * @param int $client_id
   * @param int $num
   * @param DateTimeImmutable $date
   * @param Task[] $tasks
   */
  public function __construct(
    public readonly int $client_id,
    public readonly int $num,
    public readonly DateTimeImmutable $date,
    public readonly array $tasks) {

    $this->startDate = null;
    $this->endDate = null;

    $total = 0.00;
    $currency = '';

    $this->total_hours = 0.00;

    foreach ($tasks as $task) {
      if (empty($currency)) {
        $currency = $task->currency;
      }

      $taskStarted = $task->launchedAt;
      if (null === $this->startDate || $taskStarted < $this->startDate) {
        $this->startDate = $taskStarted;
      }

      $taskEnded = $task->completedAt;
      if (null === $this->endDate || $taskEnded > $this->endDate) {
        $this->endDate = $taskEnded;
      }

      foreach ($task->sub_tasks as $subTask) {
        $total += $subTask->budget->amount;
        $this->total_hours += $subTask->hours;
      }
    }

    $this->totalAmount = new Amount($total, $currency);


    // У каждой позиции должны быть поля из подзадачи и поля из задачи.

    $item_number = 1;

    foreach ($this->tasks as $task) {
      $sub_tasks = $task->sub_tasks;

      foreach ($sub_tasks as $subTask) {
        // Если подзадача "Основная", то берем:
        //   если описание задачи не пустое, то его
        //   в противном случае - название задачи
        // Если подзадача не "Основная", то берем ее название.
        if ('Основная' === $subTask->title) {
          if (!empty(trim($task->description))) {
            $title = $task->description;
          }
          else {
            $title = $task->title;
          }
        }
        else {
          $title = $subTask->title;
        }
        $this->flatted_items[] = new DataItem(
          $item_number++,
          $title,
          $subTask->budget,
          $subTask->hours
        );
      }
    }
  }

  /**
   * Возвращает линейный список позиций для документа.
   *
   * @return DataItem[]
   */
  public function getFlattedItems(): array {
    return $this->flatted_items;
  }

  /**
   *
   * @return float
   */
  public function getTotalHours(): float {
    return $this->total_hours;
  }

  /**
   *
   * @return Amount
   */
  public function getTotalAmount(): Amount {
    return $this->totalAmount;
  }

  /**
   * @return DateTimeImmutable
   */
  public function getStartDate(): DateTimeImmutable {
    return $this->startDate;
  }

  /**
   * @return DateTimeImmutable
   */
  public function getEndDate(): DateTimeImmutable {
    return $this->endDate;
  }
}
