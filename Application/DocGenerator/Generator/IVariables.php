<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.05.2018
 * Time: 08:20
 */

namespace Application\DocGenerator\Generator;


interface IVariables {
  public const NOT_PREFIXED = 0;
  public const BLOCK_PREFIXED = 1;

  /**
   *
   * @return array
   */
  public function getTitles(): array;

  /**
   *
   * @return array
   */
  public function getCurrentKeys(): array;

  /**
   * @param int $prefixed
   *
   * @return array
   */
  public function getAllKeys(int $prefixed = IVariables::NOT_PREFIXED): array;

  /**
   *
   * @return string
   */
  public function getLabel(): string;

  /**
   *
   * @return string
   */
  public function getDescription(): string;

  /**
   *
   * @return string
   */
  public function getKey(): string;

  /**
   *
   * @return array
   */
  public function getVars(): array;

  /**
   * @param string $key
   * @param $value
   */
  public function setValue(string $key, $value): void;
}
