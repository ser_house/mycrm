<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.05.2018
 * Time: 13:02
 */

namespace Application\DocGenerator\Generator\TBS;


use Application\DocGenerator\Generator\IKeyFormatter;

class KeyFormatter implements IKeyFormatter {
  /**
   * {@inheritdoc}
   */
  public function formatId(string $key, string $var): string {
    return "$key:$var";
  }

  /**
   * {@inheritdoc}
   */
  public function formatHuman(string $key, string $var): string {
    return "[$key.$var]";
  }
}
