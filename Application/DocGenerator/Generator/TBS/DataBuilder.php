<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.06.2020
 * Time: 8:23
 */


namespace Application\DocGenerator\Generator\TBS;


use App\Model\Client\ClientSettings;
use Application\Client\Document\Template\TemplateOptions;
use Application\DocGenerator\Generator\DocData;
use Application\DocGenerator\Generator\Vars\VarsPresentAsNestedArrayTBS;
use Application\DocGenerator\Generator\Vars\VarsPresentTBS;


class DataBuilder {
  public function __construct(
    private readonly KeyFormatter $keyFormatter,
    private readonly VarsPresentAsNestedArrayTBS $varsNestedPresenter,
    private readonly VarsPresentTBS $varsPresenter
  ) {

  }

  /**
   * @param DocData $docData
   * @param TemplateOptions $templateOptions
   * @param ClientSettings $clientSettings
   *
   * @return array
   */
  public function buildDocData(DocData $docData, TemplateOptions $templateOptions, ClientSettings $clientSettings): array {
    $docLevelVars = $clientSettings
      ->getVarsRegistry()
      ->getDocLevelVars()
      ->buildBySelectedClientSettings($clientSettings, $this->keyFormatter)
    ;

    return $this->varsNestedPresenter->presentDocLevelVars($docLevelVars, $templateOptions, $docData);
  }

  /**
   * @param DocData $docData
   * @param TemplateOptions $templateOptions
   * @param ClientSettings $clientSettings
   *
   * @return array
   */
  public function buildItemsData(DocData $docData, TemplateOptions $templateOptions, ClientSettings $clientSettings): array {
    $itemLevelVars = $clientSettings
      ->getVarsRegistry()
      ->getItemLevelVars()
      ->buildBySelectedClientSettings($clientSettings, $this->keyFormatter)
    ;

    $items_data = [];
    $items = $docData->getFlattedItems();
    foreach ($items as $item) {
      $items_data[] = $this->varsPresenter->presentItemLevelVars($itemLevelVars, $item, $templateOptions);
    }

    return $items_data;
  }
}
