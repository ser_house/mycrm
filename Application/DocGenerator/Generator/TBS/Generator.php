<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.02.2018
 * Time: 11:06
 */

namespace Application\DocGenerator\Generator\TBS;

use App\Model\Client\Client;
use App\Model\DocumentTemplate;
use Application\DocGenerator\Generator\DocData;
use clsTinyButStrong;
use Exception;
use Illuminate\Http\File as HttpFile;
use Storage;


class Generator {
  public function __construct(private readonly DataBuilder $dataBuilder) {

  }

  /**
   * @param DocumentTemplate $docTemplate
   * @param DocData $docData
   * @param array $date_formats
   * @param Client $client
   *
   * @return string temporary file path
   * @throws Exception
   */
  public function generate(DocumentTemplate $docTemplate, DocData $docData, array $date_formats, Client $client): string {
    $templateOptions = $docTemplate->getOptions($date_formats);
    $clientSettings = $client->getSettings($date_formats);

    $doc_data = $this->dataBuilder->buildDocData($docData, $templateOptions, $clientSettings);
    $items_data = $this->dataBuilder->buildItemsData($docData, $templateOptions, $clientSettings);

    $template_path = $this->templateFilePath($docTemplate->file->uri);

    $TBS = new clsTinyButStrong();
    $TBS->Plugin(TBS_INSTALL, 'clsOpenTBS');

    $TBS->SetOption('noerr', true);

    $TBS->LoadTemplate($template_path, OPENTBS_ALREADY_UTF8);

    $TBS->MergeBlock('doc', $doc_data);
    $TBS->MergeBlock('item', $items_data);

    $tmp_file_path = Storage::path('generated_');

    $TBS->Show(OPENTBS_FILE, $tmp_file_path);

    return $tmp_file_path;
  }

  /**
   * @param string $template_uri
   *
   * @return string
   */
  private function templateFilePath(string $template_uri): string {
    $file = new HttpFile("storage/$template_uri");
    return $file->getRealPath();
  }
}
