<?php

namespace Application\DocGenerator;

class TaskInput {

  public function __construct(public readonly int $task_id, public readonly array $sub_task_ids) {
  }

  /**
   * @return bool
   */
  public function hasSubTaskIds(): bool {
    return !empty($this->sub_task_ids);
  }
}
