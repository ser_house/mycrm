<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.05.2018
 * Time: 21:46
 */

namespace Application\Placeholder;


use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use DateTimeImmutable;

class Month extends PlaceholderBase implements IPlaceholder {
  public const PLACEHOLDER = 'month';
  public const NAME = 'месяц';

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function prev(DateTimeImmutable $dateTime): DateTimeImmutable {
    $month = (int)$dateTime->format('m');
    $year = (int)$dateTime->format('Y');
    $month--;
    if ($month === 0) {
      $month = 12;
      $year--;
    }

    return new DateTimeImmutable("$year-$month-1 00:00:00");
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function next(DateTimeImmutable $dateTime): DateTimeImmutable {
    $month = (int)$dateTime->format('m');
    $year = (int)$dateTime->format('Y');
    $month++;
    if ($month === 13) {
      $month = 1;
      $year++;
    }

    return new DateTimeImmutable("$year-$month-1 00:00:00");
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function default(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return $formatter->month($dateTime);
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function short(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return $formatter->monthShort($dateTime);
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function long(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return $formatter->month($dateTime);
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function declined(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return $formatter->monthDeclesion($dateTime);
  }
}
