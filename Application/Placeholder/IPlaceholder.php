<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.05.2018
 * Time: 11:11
 */

namespace Application\Placeholder;


use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use DateTimeImmutable;

interface IPlaceholder {
  /**
   * @return string
   */
  public function getPlaceholder(): string;

  /**
   * @return string
   */
  public function getName(): string;

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function prev(DateTimeImmutable $dateTime): DateTimeImmutable;

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function next(DateTimeImmutable $dateTime): DateTimeImmutable;

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function default(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string;

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function short(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string;

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function long(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string;

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function declined(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string;
}
