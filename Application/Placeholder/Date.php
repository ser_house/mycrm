<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.05.2018
 * Time: 08:50
 */

namespace Application\Placeholder;


use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use DateInterval;
use DateTimeImmutable;

class Date extends PlaceholderBase implements IPlaceholder {
  public const PLACEHOLDER = 'date';
  public const NAME = 'дата';

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function prev(DateTimeImmutable $dateTime): DateTimeImmutable {
    $interval = new DateInterval('P1D');
    return $dateTime->sub($interval);
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function next(DateTimeImmutable $dateTime): DateTimeImmutable {
    $interval = new DateInterval('P1D');
    return $dateTime->add($interval);
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function default(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return $this->long($dateTime, $formatter);
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function short(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return $formatter->dateShort($dateTime);
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function long(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return $formatter->dateLong($dateTime);
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function declined(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return $this->short($dateTime, $formatter);
  }
}
