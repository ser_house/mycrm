<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.05.2018
 * Time: 10:54
 */

namespace Application\Placeholder;


class PlaceholderBase {
  public const PLACEHOLDER = '';
  public const NAME = '';

  /**
   * @return string
   */
  public function getPlaceholder(): string {
    return static::PLACEHOLDER;
  }

  /**
   * @return string
   */
  public function getName(): string {
    return static::NAME;
  }
}
