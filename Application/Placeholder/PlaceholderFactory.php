<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.05.2018
 * Time: 12:13
 */

namespace Application\Placeholder;


use InvalidArgumentException;

class PlaceholderFactory {

  /**
   * @param string $placeholder_str
   *
   * @return IPlaceholder
   */
  public static function get(string $placeholder_str): IPlaceholder {
    return match ($placeholder_str) {
      Date::PLACEHOLDER => new Date(),
      Month::PLACEHOLDER => new Month(),
      Year::PLACEHOLDER => new Year(),
      MonthYear::PLACEHOLDER => new MonthYear(),
      default => throw new InvalidArgumentException("Для заменителя $placeholder_str не найден класс."),
    };
  }
}
