<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.05.2018
 * Time: 09:20
 */

namespace Application\Placeholder;


use Application\Placeholder\Modifier;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use DateTimeImmutable;

class Help {
  private const PLACEHOLDERS_PATH = __DIR__;
  private const MODIFIERS_PATH = __DIR__ . '/Modifier';

  /** @var IPlaceholder[] $placeholders */
  private array $placeholders = [];

  /** @var Modifier\IModifier[] $modifiers */
  private array $modifiers = [];

  public function __construct(private readonly DateTimeFormatter $dateTimeFormatter) {
    $placeholder_classes = load_implements_classes_from_dir(self::PLACEHOLDERS_PATH, static function ($class) {
      if (!class_exists($class)) {
        return false;
      }
      $interfaces = class_implements($class);
      if (isset($interfaces[IPlaceholder::class])) {
        return !empty($class::PLACEHOLDER);
      }

      return false;
    });

    foreach ($placeholder_classes as $class => $path) {
      $this->placeholders[] = new $class();
    }

    $modifier_classes = load_implements_classes_from_dir(self::MODIFIERS_PATH, static function ($class) {
      if (!class_exists($class)) {
        return false;
      }
      $interfaces = class_implements($class);
      if (isset($interfaces[Modifier\IModifier::class])) {
        return !empty($class::CODE);
      }

      return false;
    });

    foreach ($modifier_classes as $class => $path) {
      $this->modifiers[] = new $class(new EmptyPlaceholder());
    }
  }

  /**
   * @return array|array[]
   */
  public function help(): array {
    // могут потребоваться:
    // месяц
    // месяц с годом
    // месяц склоненный
    // месяц склоненный с годом
    // год
    // год из 2-х цифр
    // дата

    // d - склонение
    // u - первый символ заглавный
    // p - предыдущий
    // s - краткий формат
    // примеры:
    //   {month::dup} - Феврале
    //   {month year::dp} - феврале 2018


    $placeholders = [];
    foreach ($this->placeholders as $placeholder) {
      $code = self::formatPlaceholder($placeholder);
      $placeholders[$code] = $placeholder->getName();
    }

    $codes = [];
    foreach ($this->modifiers as $modifier) {
      $codes[$modifier->getCode()] = $modifier->getName();
    }


    // @todo: {month year} - коротким может быть и месяц (фев) и год (18).
    // При этом не получится обходится отдельными модификаторами,
    // потому что следующий может зависеть от предыдущего (месяц декабрь + 1).
    // Но можно попробовать прикрутить плейсхолдеры набором (двусвязным списком) так,
    // чтобы проверять и обрабатывать за раз весь такой набор.


    $processor = new Processor($this->dateTimeFormatter);
    $help = [
      'placeholders' => $placeholders,
      'modifiers' => $codes,
    ];

    $examples = [
      '{month::u}',
      '{month::dp}',
      '{month::n}',
      '{date}',
      '{date::n}',
      '{date::s}',
      '{month year}',
      '{month year::dp}',
    ];

    $dateTime = new DateTimeImmutable();

    foreach ($examples as $example) {
      $help['examples'][$example] = $processor->process($dateTime, $example);
    }

    return $help;
  }

  /**
   * @param IPlaceholder $placeholder
   *
   * @return string
   */
  public static function formatPlaceholder(IPlaceholder $placeholder): string {
    $format = '{%s}';

    return sprintf($format, $placeholder->getPlaceholder());
  }
}
