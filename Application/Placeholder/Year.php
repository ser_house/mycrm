<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.05.2018
 * Time: 21:46
 */

namespace Application\Placeholder;


use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use DateInterval;
use DateTimeImmutable;

class Year extends PlaceholderBase implements IPlaceholder {
	public const PLACEHOLDER = 'year';
	public const NAME = 'год';

  public function prev(DateTimeImmutable $dateTime): DateTimeImmutable {
    $interval = new DateInterval('P1Y');
    return $dateTime->sub($interval);
  }

  public function next(DateTimeImmutable $dateTime): DateTimeImmutable {
    $interval = new DateInterval('P1Y');
    return $dateTime->add($interval);
  }

	public function default(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
		return $formatter->year($dateTime);
	}

	public function short(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
		return $formatter->year($dateTime);
	}

	public function long(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
		return $dateTime->format('Y');
	}

	public function declined(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
		return $this->long($dateTime, $formatter);
	}
}
