<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.05.2018
 * Time: 12:28
 */

namespace Application\Placeholder;


use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Placeholder\Modifier\Modifiers;
use DateTimeImmutable;


class Processor {
  // @todo: надо, пожалуй, рассмотреть обработку вариантов вида {month:s year:p}.
  private const REGEX = '/\{(\w*\s*\w*:*\w*)\}/';
  private const DELIMITER = '::';

  public function __construct(protected readonly DateTimeFormatter $formatter) {

  }


  /**
   * @param DateTimeImmutable $dateTime
   * @param string $str
   *
   * @return string
   */
  public function process(DateTimeImmutable $dateTime, string $str): string {
    $matches = [];

    preg_match_all(static::REGEX, $str, $matches);

    if (empty($matches[0])) {
      return $str;
    }

    $raw_items = $matches[0];
    $items = $matches[1];

    foreach ($items as $i => $item) {

      $modifiers_str = '';

      if (str_contains($item, static::DELIMITER)) {
        [$placeholder_str, $modifiers_str] = explode(static::DELIMITER, $item);
      }
      else {
        $placeholder_str = $item;
      }

      $placeholder = PlaceholderFactory::get($placeholder_str);
      $modifiers = new Modifiers($modifiers_str, $placeholder);

      $preparedDateTime = $this->pre($modifiers, $dateTime);
      $result = $this->modify($modifiers, $preparedDateTime);
      $result = $this->post($modifiers, $result);

      if (!empty($result)) {
        $str = str_replace($raw_items[$i], $result, $str);
      }
    }

    return $str;
  }

  /**
   * @param Modifiers $modifiers
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  private function pre(Modifiers $modifiers, DateTimeImmutable $dateTime): DateTimeImmutable {
    // Модификаторы должны использовать одну и ту же дату,
    // чтобы иметь возможность влиять на неё.
    $preparedDateTime = null;

    foreach ($modifiers->getPreModifiers() as $modifier) {
      // Если модификаторы дату еще не меняли - меняем.
      if (null === $preparedDateTime) {
        $preparedDateTime = $modifier->pre($dateTime);
      }
      // В противном случае работаем с уже измененной датой.
      else {
        $preparedDateTime = $modifier->pre($preparedDateTime);
      }
    }

    if (null == $preparedDateTime) {
      $preparedDateTime = $dateTime;
    }

    return $preparedDateTime;
  }

  /**
   * @param Modifiers $modifiers
   * @param DateTimeImmutable $dateTime
   *
   * @return string
   */
  private function modify(Modifiers $modifiers, DateTimeImmutable $dateTime): string {
    $result = '';
    foreach ($modifiers->getProcessModifiers() as $modifier) {
      $result = $modifier->modify($dateTime, $this->formatter);
    }

    return $result;
  }

  /**
   * @param Modifiers $modifiers
   * @param string $result
   *
   * @return string
   */
  private function post(Modifiers $modifiers, string $result): string {
    foreach ($modifiers->getPostModifiers() as $modifier) {
      $result = $modifier->post($result);
    }

    return $result;
  }
}
