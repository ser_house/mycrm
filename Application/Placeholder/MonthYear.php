<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.05.2018
 * Time: 11:58
 */

namespace Application\Placeholder;


use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use DateTimeImmutable;

class MonthYear extends PlaceholderBase implements IPlaceholder {
  public const PLACEHOLDER = 'month year';
  public const NAME = 'месяц с годом';

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function prev(DateTimeImmutable $dateTime): DateTimeImmutable {
    $month = (int)$dateTime->format('m');
    $year = (int)$dateTime->format('Y');
    $month--;
    if ($month === 0) {
      $month = 12;
      $year--;
    }

    return new DateTimeImmutable("$year-$month-1 00:00:00");
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function next(DateTimeImmutable $dateTime): DateTimeImmutable {
    $month = (int)$dateTime->format('m');
    $year = (int)$dateTime->format('Y');
    $month++;
    if ($month === 13) {
      $month = 1;
      $year++;
    }

    return new DateTimeImmutable("$year-$month-1 00:00:00");
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function default(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    $result = $this->long($dateTime, $formatter);
    return mb_strtolower($result);
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function short(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return $dateTime->format('m.Y');
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function long(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    $month = $formatter->month($dateTime);
    return mb_strtolower($month) . ' ' . $formatter->year($dateTime);
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function declined(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    $month = $formatter->monthDeclesion($dateTime);
    return mb_strtolower($month) . ' ' . $formatter->year($dateTime);
  }
}
