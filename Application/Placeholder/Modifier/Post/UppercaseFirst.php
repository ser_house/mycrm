<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.05.2018
 * Time: 21:48
 */

namespace Application\Placeholder\Modifier\Post;

use Application\Placeholder\Modifier\ModifierBase;

class UppercaseFirst extends ModifierBase {
  public const CODE = 'u';
  public const NAME = 'первый символ заглавный';

  /**
   * @param string $result
   *
   * @return string
   */
  public function post(string $result): string {
    $first_symbol = mb_strtoupper(mb_substr($result, 0, 1));
    return $first_symbol . mb_substr($result, 1);
  }
}
