<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.05.2018
 * Time: 21:52
 */

namespace Application\Placeholder\Modifier;


use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use DateTimeImmutable;

class Short extends ModifierBase {
  public const CODE = 's';
  public const NAME = 'краткий формат';

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function modify(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return $this->placeholder->short($dateTime, $formatter);
  }
}
