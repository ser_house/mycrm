<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.05.2018
 * Time: 21:49
 */

namespace Application\Placeholder\Modifier\Pre;

use Application\Placeholder\Modifier\ModifierBase;
use DateTimeImmutable;

class Prev extends ModifierBase {
  public const CODE = 'p';
  public const NAME = 'предыдущий';

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function pre(DateTimeImmutable $dateTime): DateTimeImmutable {
    return $this->placeholder->prev($dateTime);
  }
}
