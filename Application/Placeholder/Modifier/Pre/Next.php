<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.05.2018
 * Time: 21:49
 */

namespace Application\Placeholder\Modifier\Pre;

use Application\Placeholder\Modifier\ModifierBase;
use DateTimeImmutable;

class Next extends ModifierBase {
  public const CODE = 'n';
  public const NAME = 'следующий';

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function pre(DateTimeImmutable $dateTime): DateTimeImmutable {
    return $this->placeholder->next($dateTime);
  }

}
