<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.05.2018
 * Time: 09:23
 */

namespace Application\Placeholder\Modifier;


use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use DateTimeImmutable;

interface IModifier {
  /**
   * @return string
   */
  public function getCode(): string;

  /**
   * @return string
   */
  public function getName(): string;

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function pre(DateTimeImmutable $dateTime): DateTimeImmutable;

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function modify(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string;

  /**
   * @param string $result
   *
   * @return string
   */
  public function post(string $result): string;
}
