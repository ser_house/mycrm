<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.05.2018
 * Time: 21:57
 */

namespace Application\Placeholder\Modifier;


use Application\Placeholder\IPlaceholder;
use Application\Placeholder\Modifier\Post\UppercaseFirst;
use Application\Placeholder\Modifier\Pre\Next;
use Application\Placeholder\Modifier\Pre\Prev;

class Modifiers {

  public const STEP_PRE = 0;
  public const STEP_MODIFY = 1;
  public const STEP_POST = 2;

  private array $modifiers;


  /**
   * Modifiers constructor.
   *
   * @param string $str
   * @param IPlaceholder $placeholder
   */
  public function __construct(string $str, IPlaceholder $placeholder) {

    $codes = str_split($str);

    // Есть три типа модификаторов:
    // 1. подготавливающий (меняющий дату до непосредственной замены на неё)
    // 2. обрабатывающий (формирует строку по заданному формату, например - "краткий")
    // 3. модифицирующий итоговую строку (регистр символов, например)
    //
    // Если у нас нет обрабатывающего модификатора, то плейсхолдер сам решает
    // как форматировать (для этого у него есть метод "по умолчанию").
    // Но плейсхолдер должен сначала узнать, что ему надо использовать дефолтный метод.
    // Об этом ему скажет специальный модификатор Nothing, который мы включим в цепочку обработки.

    $codes = array_combine($codes, $codes);

    // Определенная последовательность и без использования весов.
    $this->modifiers = [
      self::STEP_PRE => $this->buildPre($placeholder, $codes),
      self::STEP_MODIFY => $this->buildModify($placeholder, $codes),
      self::STEP_POST => $this->buildPost($placeholder, $codes),
    ];
  }

  /**
   * @param IPlaceholder $placeholder
   * @param array $codes
   *
   * @return array
   */
  private function buildPre(IPlaceholder $placeholder, array $codes): array {
    $modifiers = [];
    if (isset($codes[Prev::CODE])) {
      $modifiers[] = new Prev($placeholder);
    }
    if (isset($codes[Next::CODE])) {
      $modifiers[] = new Next($placeholder);
    }

    return $modifiers;
  }

  /**
   * @param IPlaceholder $placeholder
   * @param array $codes
   *
   * @return array
   */
  private function buildModify(IPlaceholder $placeholder, array $codes): array {
    $modifiers = [];
    if (!isset($codes[Short::CODE]) && !isset($codes[Decline::CODE])) {
      $modifiers[] = new ModifierBase($placeholder);
    }
    else {
      if (isset($codes[Short::CODE])) {
        $modifiers[] = new Short($placeholder);
      }
      if (isset($codes[Decline::CODE])) {
        $modifiers[] = new Decline($placeholder);
      }
    }

    return $modifiers;
  }

  /**
   * @param IPlaceholder $placeholder
   * @param array $codes
   *
   * @return array
   */
  private function buildPost(IPlaceholder $placeholder, array $codes): array {
    $modifiers = [];
    if (isset($codes[UppercaseFirst::CODE])) {
      $modifiers[] = new UppercaseFirst($placeholder);
    }

    return $modifiers;
  }

  /**
   * @return IModifier[]
   */
  public function getPreModifiers(): array {
    return $this->modifiers[self::STEP_PRE];
  }

  /**
   * @return IModifier[]
   */
  public function getProcessModifiers(): array {
    return $this->modifiers[self::STEP_MODIFY];
  }

  /**
   * @return IModifier[]
   */
  public function getPostModifiers(): array {
    return $this->modifiers[self::STEP_POST];
  }
}
