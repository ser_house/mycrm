<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.05.2018
 * Time: 21:42
 */

namespace Application\Placeholder\Modifier;


use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use DateTimeImmutable;

class Decline extends ModifierBase {
  public const CODE = 'd';
  public const NAME = 'склонение';

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function modify(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return $this->placeholder->declined($dateTime, $formatter);
  }
}
