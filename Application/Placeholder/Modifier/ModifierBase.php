<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.05.2018
 * Time: 21:47
 */

namespace Application\Placeholder\Modifier;


use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Placeholder\IPlaceholder;
use DateTimeImmutable;

class ModifierBase implements IModifier {
  public const CODE = '';
  public const NAME = '';

  /** @var IPlaceholder $placeholder */
  protected IPlaceholder $placeholder;


  public function __construct(IPlaceholder $placeholder) {
    $this->placeholder = $placeholder;
  }

  /**
   * @return string
   */
  public function getCode(): string {
    return static::CODE;
  }

  /**
   * @return string
   */
  public function getName(): string {
    return static::NAME;
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function pre(DateTimeImmutable $dateTime): DateTimeImmutable {
    return $dateTime;
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function modify(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return $this->placeholder->default($dateTime, $formatter);
  }

  /**
   * @param string $result
   *
   * @return string
   */
  public function post(string $result): string {
    return $result;
  }
}
