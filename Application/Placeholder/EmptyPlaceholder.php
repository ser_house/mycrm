<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.05.2018
 * Time: 10:10
 */

namespace Application\Placeholder;


use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use DateTimeImmutable;

class EmptyPlaceholder extends PlaceholderBase implements IPlaceholder {

  /**
   * @return string
   */
  public function getPlaceholder(): string {
    return '';
  }

  /**
   * @return string
   */
  public function getName(): string {
    return '';
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function prev(DateTimeImmutable $dateTime): DateTimeImmutable {
    return $dateTime;
  }

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable
   */
  public function next(DateTimeImmutable $dateTime): DateTimeImmutable {
    return $dateTime;
  }


  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function default(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return '';
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function short(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return '';
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function long(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return '';
  }

  /**
   * @param DateTimeImmutable $dateTime
   * @param DateTimeFormatter $formatter
   *
   * @return string
   */
  public function declined(DateTimeImmutable $dateTime, DateTimeFormatter $formatter): string {
    return '';
  }
}
