<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.04.2020
 * Time: 2:54
 */


namespace Application\Incoming\View;


class Item {
  public function __construct(
    public readonly string $title = '',
    public readonly string $value = '',
    public readonly float $raw = 0.0
  ) {

  }
}
