<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.04.2020
 * Time: 2:52
 */


namespace Application\Incoming\View;


class View {

  public Item $header;
  /** @var Item[] */
  public array $items;
  public Item $footer;

  /**
   * View constructor.
   *
   * @param Item[] $items
   */
  public function __construct(array $items = []) {
    $this->items = $items;
  }

  /**
   *
   * @return Item
   */
  public function getMaxValueItem(): Item {
    $index = 0;
    $max = 0.0;
    foreach($this->items as $i => $item) {
      if ($item->raw > $max) {
        $max = $item->raw;
        $index = $i;
      }
    }
    return $this->items[$index];
  }
}
