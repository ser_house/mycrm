<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.04.2020
 * Time: 2:50
 */


namespace Application\Incoming\View;


use Application\Money\Amount;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Infrastructure\Formatter\Money as MoneyFormatter;
use Application\Incoming\Data;
use Application\Incoming\Item as IncomingItem;
use Application\Incoming\View\Item as ItemView;

/**
 * Class Presenter
 *
 * @package Application\Incoming\View
 */
class Presenter {
  public function __construct(
    protected readonly DateTimeFormatter $dateTimeFormatter,
    protected readonly MoneyFormatter $moneyFormatter
  ) {

  }


  /**
   * @param Data $data
   *
   * @return array
   */
  public function presentByYears(Data $data): array {
    $values = [];
    $labels_x = [];
    foreach ($data->getItems() as $item) {
      $label_x = $this->dateTimeFormatter->year($item->dateTime);
      $labels_x[$label_x] = $label_x;
      $values[] = $item->amount;
    }

    return [
      'labels_x' => array_values($labels_x),
      'values' => $values,
    ];
  }

  /**
   * @param Data $data
   *
   * @return array
   */
  public function presentByMonths(Data $data): array {
    $values = [];
    $labels_x = [];
    foreach ($data->getItems() as $item) {
      $label_x = $this->dateTimeFormatter->month($item->dateTime);
      $labels_x[$label_x] = $label_x;
      $values[] = $item->amount;
    }

    return [
      'labels_x' => array_values($labels_x),
      'values' => $values,
    ];
  }

  /**
   * @param Data $data
   * @param string $header_label
   *
   * @return View
   */
  private function basePresentView(Data $data, string $header_label): View {
    $view = new View();

    $view->header = new ItemView($header_label, 'Сумма');
    $view->footer = new ItemView('Итого', $this->moneyFormatter->format(new Amount($data->getTotal())));

    return $view;
  }

  /**
   * @param IncomingItem $item
   *
   * @return Item
   */
  private function basePresentItemView(IncomingItem $item): ItemView {
    return new ItemView(
      '',
      $this->moneyFormatter->format(new Amount($item->amount)),
      $item->amount
    );
  }
}
