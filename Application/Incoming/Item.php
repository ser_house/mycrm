<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.05.2018
 * Time: 17:45
 */

namespace Application\Incoming;


use DateTimeImmutable;

class Item {
  public function __construct(
    public readonly DateTimeImmutable $dateTime,
    public readonly float $amount
  ) {

  }
}
