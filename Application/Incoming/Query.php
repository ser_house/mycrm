<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.05.2018
 * Time: 17:00
 */

namespace Application\Incoming;

use DB;

/**
 * Class Query
 *
 * @package Application\Incoming
 */
class Query {

  private string $sql = '';
  private array $params = [];

  /**
   * @param int $user_id
   * @param int $year
   * @param int $client_id
   *
   * @return $this
   */
	public function build(int $user_id, int $year, int $client_id): self {
		$this->sql = 'SELECT ';

		$fields = [];

		// Если указан год, то группируем по месяцам.
		if (!empty($year)) {
			$fields[] = "DATE_FORMAT(created_at, '%Y/%m') AS month";
		}
		else {
			$fields[] = "DATE_FORMAT(created_at, '%Y') AS year";
		}

		$fields[] = 'MAX(created_at) AS raw_date';
		$fields[] = 'SUM(amount*rate) AS amount_in_currency';

		$this->sql .= implode(',', $fields);

		$this->sql .= ' FROM payment
    INNER JOIN task ON task.id = payment.task_id';

    $conditions = [
      'task.user_id = :user_id'
    ];
		$this->params = [
      'user_id' => $user_id,
    ];

		if (!empty($client_id)) {
			$conditions[] = 'task.client_id = :client_id';
      $this->params['client_id'] = $client_id;
		}

		if (!empty($year)) {
			$conditions[] = "DATE_FORMAT(created_at, '%Y') = :year";
      $this->params['year'] = $year;
		}

    $conditions_str = implode(' AND ', $conditions);
    $this->sql .= " WHERE $conditions_str";

		if (!empty($year)) {
			$this->sql .= ' GROUP BY month';
		}
		else {
			$this->sql .= ' GROUP BY year';
		}

		$this->sql .= ' ORDER BY created_at ASC';

		return $this;
	}

  /**
   *
   * @return Data|null
   */
	public function getData(): ?Data {
    $rows = DB::select($this->sql, $this->params);
    return $rows ? new Data($rows) : null;
  }
}
