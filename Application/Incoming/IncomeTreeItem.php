<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2018
 * Time: 14:35
 */

namespace Application\Incoming;


use Application\Money\Amount;

class IncomeTreeItem {
  public Amount $amount;
  /** @var IncomeTreeItem[] */
  public array $children;

  public function __construct(public readonly int $id, public readonly string $label) {
    $this->children = [];
    $this->amount = new Amount(0.00);
  }

  public function addChild(IncomeTreeItem $item): self {
    $this->children[$item->id] = $item;
    return $this;
  }

  public function hasChildId($id): bool {
    return isset($this->children[$id]);
  }

  /**
   * @param Amount $amount
   *
   * @return IncomeTreeItem
   */
  public function setAmount(Amount $amount): IncomeTreeItem {
    $this->amount = $amount;

    return $this;
  }

  public function getChild($id): ?IncomeTreeItem {
    return $this->children[$id] ?? null;
  }

  public function childrenCount(): int {
    return count($this->children);
  }

  public function calcTotal(): void {
    $totalAmount = $this->calcChildrenTotal($this->children);
    $this->setAmount($totalAmount);
  }

  /**
   * @param IncomeTreeItem[] $children
   *
   * @return Amount
   */
  private function calcChildrenTotal(array $children): Amount {
    $total = 0.00;
    foreach ($children as $child) {
      if ($child->childrenCount() > 0) {
        $amount = $this->calcChildrenTotal($child->children);
        $child->setAmount($amount);
        $raw_amount = $amount->amount;
        $total += $raw_amount;
      }
      else {
        $raw_amount = $child->amount->amount;
        $total += $raw_amount;
      }
    }

    return new Amount($total);
  }
}
