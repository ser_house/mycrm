<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.05.2020
 * Time: 10:44
 */


namespace Application\Incoming;

use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Money\Amount;
use DB;
use Cache;

/**
 * Class Service
 *
 * @package Application\Incoming
 */
class Service {

  private const CACHE_PREFIX = 'incoming';

  public function __construct(private readonly DateTimeFormatter $dateTimeFormatter) {

  }

  /**
   * @param int $user_id
   *
   * @return Data|null
   */
  public function userCurrentYearByMonths(int $user_id): ?Data {
    return $this->data($user_id, (int)date('Y'), 0);
  }

  /**
   * @param int $user_id
   * @param int $client_id
   *
   * @return Data|null
   */
  public function userCurrentYearClientByMonth(int $user_id, int $client_id): ?Data {
    return $this->data($user_id, (int)date('Y'), $client_id);
  }

  /**
   * @param int $user_id
   *
   * @return Data|null
   */
  public function userByYears(int $user_id): ?Data {
    return $this->data($user_id, 0, 0);
  }

  /**
   * @param int $user_id
   * @param int $year
   * @param int $client_id
   */
  public function resetCache(int $user_id, int $year, int $client_id): void {
    $key = self::CACHE_PREFIX . "_{$user_id}_{$year}_{$client_id}";
    Cache::forget($key);
  }

  /**
   * @param int $user_id
   */
  public function resetUserCache(int $user_id): void {
    $key = self::CACHE_PREFIX . "_{$user_id}_0_0";
    Cache::forget($key);
  }

  /**
   * @param int $user_id
   * @param int $client_id
   */
  public function resetClientCache(int $user_id, int $client_id): void {
    $key = self::CACHE_PREFIX . "_{$user_id}_0_{$client_id}";
    Cache::forget($key);
  }

  /**
   * @param int $user_id
   * @param int $year
   */
  public function resetUserYearCache(int $user_id, int $year): void {
    $key = self::CACHE_PREFIX . "_{$user_id}_{$year}_0";
    Cache::forget($key);
  }

  /**
   * @param int $user_id
   * @param int $year
   * @param int $client_id
   *
   * @return Data|null
   */
  private function data(int $user_id, int $year, int $client_id): ?Data {
    $key = self::CACHE_PREFIX . "_{$user_id}_{$year}_{$client_id}";

    if (Cache::has($key)) {
      $data = Cache::get($key);
    }
    else {
      $query = new Query();
      $query->build($user_id, $year, $client_id);
      $data = $query->getData();

      $expiresAt = now()->addMonth();

      Cache::put($key, $data, $expiresAt);
    }

    return $data;
  }

  /**
   * @param int $user_id
   *
   * @return IncomeTreeItem[]|array
   */
  public function getUserIncomingDetailsAsTree(int $user_id): array {
    $data = $this->getUserIncomingDataDetail($user_id);
    return $this->buildTree($data);
  }

  /**
   * @param int $user_id
   *
   * @return array
   */
  private function getUserIncomingDataDetail(int $user_id): array {
    $sql = "SELECT 
				c.id AS client_id,
    		c.name AS client_name,
    		t.id AS task_id, 
    		t.title AS task_name, 
    		DATE_FORMAT(p.created_at, '%Y') AS year,
    		DATE_FORMAT(p.created_at, '%m') AS month, 
    		SUM(p.amount * p.rate) AS amount
			FROM payment AS p
			INNER JOIN task AS t ON t.id = p.task_id
			INNER JOIN client AS c ON c.id = t.client_id
			WHERE c.user_id = :user_id
			GROUP BY c.id, c.name, t.id, t.title, year, month
			ORDER BY p.created_at";
    return DB::select($sql, ['user_id' => $user_id]);
  }

  /**
   * @param array $data
   *
   * @return IncomeTreeItem[]|array
   */
  private function buildTree(array $data): array {
    $months = $this->dateTimeFormatter->months();

    /** @var IncomeTreeItem[] $items */
    $items = [];
    foreach ($data as $datum) {
      $year = $datum->year;

      if (!isset($items[$year])) {
        $items[$year] = new IncomeTreeItem($year, $year);
      }

      $yearItem = $items[$year];

      $month = (int)$datum->month;
      $month_name = $months[$month];
      if (!$yearItem->hasChildId($month)) {
        $monthItem = new IncomeTreeItem($month, $month_name);
        $yearItem->addChild($monthItem);
      }
      else {
        $monthItem = $yearItem->getChild($month);
      }

      $client_id = $datum->client_id;
      if (!$monthItem->hasChildId($client_id)) {
        $clientItem = new IncomeTreeItem($client_id, $datum->client_name);
        $monthItem->addChild($clientItem);
      }
      else {
        $clientItem = $monthItem->getChild($client_id);
      }

      $task_id = $datum->task_id;
      if (!$clientItem->hasChildId($task_id)) {
        $taskItem = new IncomeTreeItem($task_id, $datum->task_name);
        $taskItem->setAmount(new Amount($datum->amount));
        $clientItem->addChild($taskItem);
      }
    }

    foreach ($items as $item) {
      $item->calcTotal();
    }

    return $items;
  }
}
