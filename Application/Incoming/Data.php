<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.05.2018
 * Time: 17:41
 */

namespace Application\Incoming;


use DateTimeImmutable;

class Data {

	/** @var Item[] $items */
  private array $items = [];

	/** @var float $total */
  private float $total = 0.0;


	/**
	 * Data constructor.
	 *
	 * @param array $db_rows
	 */
	public function __construct(array $db_rows) {
		foreach ($db_rows as $db_row) {
			$this->items[] = new Item(new DateTimeImmutable($db_row->raw_date), $db_row->amount_in_currency);
			$this->total += $db_row->amount_in_currency;
		}
	}

	/**
	 * @return Item[]
	 */
	public function getItems(): array {
		return $this->items;
	}

	/**
	 * @return float
	 */
	public function getTotal(): float {
		return $this->total;
	}
}
