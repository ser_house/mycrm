<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.04.2018
 * Time: 21:09
 */

namespace Application\Calendar;


class Month {

  private array $weeks = [];

  public function __construct(public readonly string $name) {

  }

  /**
   * @param Day $day
   *
   * @return $this
   */
  public function setDay(Day $day): self {
    $week = $day->week;
    $week_day = $day->week_day;

    if (!isset($this->weeks[$week])) {
      $this->weeks[$week] = array_fill(1, 7, null);
    }

    $this->weeks[$week][$week_day] = $day;
    return $this;
  }

  /**
   * @return array
   */
  public function getWeeks(): array {
    return $this->weeks;
  }

  /**
   * @return string[]
   */
  public function getWeekdayNames(): array {
    return [
      'пн',
      'вт',
      'ср',
      'чт',
      'пт',
      'сб',
      'вс',
    ];
  }
}
