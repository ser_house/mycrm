<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.04.2020
 * Time: 14:52
 */


namespace Application\Calendar;


use Application\Money\Amount;

class AmountItem {

  public function __construct(
    public readonly string $title,
    public readonly Amount $amount
  ) {
  }
}
