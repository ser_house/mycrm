<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.04.2018
 * Time: 16:03
 */

namespace Application\Calendar;

use Application\Money\Amount;
use App\Model\Client\Client;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use App\Model\Holiday;
use DateInterval;
use DatePeriod;
use DateTimeImmutable;
use DB;
use Exception;
use Illuminate\Support\Collection;
use ReflectionException;
use Yasumi\Yasumi;


class CalendarService {
  public function __construct(private readonly DateTimeFormatter $dateTimeFormatter) {

  }

  /**
   * @param int $client_id
   *
   * @return array
   */
  public function getClientPaymentYears(int $client_id): array {
    $sql = "SELECT 
		DISTINCT DATE_FORMAT(p.created_at, '%Y') AS year
		FROM payment AS p
		INNER JOIN task AS t ON t.id = p.task_id
		WHERE t.client_id = :client_id
		ORDER BY DATE_FORMAT(p.created_at, '%Y') ASC";
    $data = DB::select($sql, ['client_id' => $client_id]);

    return $data ? array_column($data, 'year') : [];
  }

  /**
   *
   * @return array
   */
  public function getPaymentYears(): array {
    $sql = "SELECT 
		DISTINCT DATE_FORMAT(p.created_at, '%Y') AS year
		FROM payment AS p
		INNER JOIN task AS t ON t.id = p.task_id
		ORDER BY DATE_FORMAT(p.created_at, '%Y') ASC";
    $data = DB::select($sql);

    return $data ? array_column($data, 'year') : [];
  }

  /**
   * @param int|null $year
   *
   * @return array
   */
  public function getYearDates(int $year = null): array {
    if (null === $year) {
      $year = (int)date('Y');
    }

    $start_date = new DateTimeImmutable("$year-01-01");
    $end_date = new DateTimeImmutable("$year-12-31");

    $dates = [];

    $dayInterval = new DateInterval('P1D');

    $period = new DatePeriod($start_date, $dayInterval, $end_date->add($dayInterval));
    foreach ($period as $date) {
      $dates[] = new Day($date);
    }

    return $dates;
  }

  /**
   * @param Client $client
   * @param int $year
   * @param Day[] $days
   *
   * @return array
   */
  public function buildClientCalendarData(Client $client, int $year, array $days): array {
    $sql = "SELECT
      DATE_FORMAT(payment.created_at, '%Y-%m-%d') AS date,
      task.currency AS currency,
      SUM(payment.amount) AS amount
    FROM payment
      INNER JOIN task ON task.id = payment.task_id
    WHERE task.client_id = :client_id AND DATE_FORMAT(payment.created_at, '%Y') = :year
    GROUP BY DATE_FORMAT(payment.created_at, '%Y-%m-%d')
    ORDER BY payment.created_at ASC";
    $data = DB::select($sql, ['client_id' => $client->id, 'year' => $year]);
    $amounts = [];
    foreach ($data as $datum) {
      $amounts[$datum->date][] = new AmountItem($client->name, new Amount($datum->amount, $datum->currency));
    }

    return $this->buildMonths($days, $amounts);
  }

  /**
   * @param int $year
   * @param Day[] $days
   *
   * @return array
   */
  public function buildCalendarData(int $year, array $days): array {
    $sql = "SELECT
      DATE_FORMAT(payment.created_at, '%Y-%m-%d') AS date,
      task.currency AS currency,
      client.name AS client_name,
      SUM(payment.amount) AS amount
    FROM payment
      INNER JOIN task ON task.id = payment.task_id
      INNER JOIN client ON client.id = task.client_id
    WHERE DATE_FORMAT(payment.created_at, '%Y') = :year
    GROUP BY DATE_FORMAT(payment.created_at, '%Y-%m-%d'), client.id
    ORDER BY payment.created_at ASC";
    $data = DB::select($sql, ['year' => $year]);
    $amounts = [];
    foreach ($data as $datum) {
      $amounts[$datum->date][] = new AmountItem($datum->client_name, new Amount($datum->amount, $datum->currency));
    }

    return $this->buildMonths($days, $amounts);
  }

  /**
   * @param Day[] $days
   * @param array $amounts
   *
   * @return array
   */
  private function buildMonths(array $days, array $amounts): array {
    $months = [];

    $month_names = $this->dateTimeFormatter->months();

    foreach ($days as $day) {
      $month = $day->month;

      if (!isset($months[$month])) {
        $months[$month] = new Month($month_names[$month]);
      }

      $date_str = $day->getDateString();
      if (isset($amounts[$date_str])) {
        foreach ($amounts[$date_str] as $item) {
          $day->addAmountItem($item);
        }
      }
      $months[$month]->setDay($day);
    }

    return $months;
  }

  /**
   * @param int|null $year
   *
   * @return array
   * @throws ReflectionException
   */
  public function getHolidayYearDates(int $year = null): array {
    if (null === $year) {
      $year = (int)date('Y');
    }

    $start_date = new DateTimeImmutable("$year-01-01");
    $end_date = new DateTimeImmutable("$year-12-31");

    $date_format = 'd.m.Y';

    $holiday_dates = [];

    $holidays = Yasumi::create('Russia', $year);
    foreach ($holidays->getHolidayDates() as $holiday_date) {
      $date = DateTimeImmutable::createFromFormat('Y-m-d', $holiday_date);
      if ($date->format('N') < 6) {
        $formatted = $date->format($date_format);
        $holiday_dates[$formatted] = $formatted;
      }
    }

    $dates = [];

    $dayInterval = new DateInterval('P1D');

    $period = new DatePeriod($start_date, $dayInterval, $end_date->add($dayInterval));
    foreach ($period as $date) {
      $day = new Day($date);

      $formatted = $date->format($date_format);

      if (!$day->isWeekend() && isset($holiday_dates[$formatted])) {
        $day->setIsOfficialHoliday();
      }

      $dates[] = $day;
    }

    return $dates;
  }

  /**
   * @param Day[] $days
   * @param array $holidays
   */
  public function setHolidaysToDates(array $days, array $holidays): void {
    foreach ($days as $day) {
      $date = $day->getDateString();
      if (isset($holidays[$date])) {
        $day->setIsHoliday();
      }
    }
  }

  /**
   * @param $holidays
   *
   * @return Day[]
   * @throws Exception
   */
  public function holidaysToDays($holidays): array {
    $days = [];

    foreach ($holidays as $holiday) {
      $days[] = new Day(new DateTimeImmutable($holiday));
    }

    return $days;
  }

  /**
   * @param int|null $year
   * @param int|null $client_id
   *
   * @return array
   */
  private function buildWhere(int $year = null, int $client_id = null): array {
    if (null === $year) {
      $year = (int)date('Y');
    }

    $where_client = $client_id ? ['client_id', $client_id] : ['client_id', null];
    return [['year', $year], $where_client];
  }

  /**
   * @param int|null $year
   * @param int|null $client_id
   *
   * @return Collection
   */
  public function getHolidays(int $year = null, int $client_id = null): Collection {
    $where = $this->buildWhere($year, $client_id);
    return Holiday::where($where)->orderBy('date')->get();
  }

  /**
   * @param int|null $year
   * @param int|null $client_id
   *
   * @return array
   */
  public function getHolidaysAsDatesList(int $year = null, int $client_id = null): array {
    $holidays = $this->getHolidays($year, $client_id);

    $dates = [];

    foreach ($holidays as $holiday) {
      $dates[$holiday->date] = $holiday->date;
    }

    return $dates;
  }

  /**
   * @param array $dates
   * @param int $year
   * @param int|null $client_id
   *
   * @throws Exception
   */
  public function saveHolidays(array $dates, int $year, int $client_id = null): void {
    $holidays = $this->getHolidays($year, $client_id);
    $exists_dates = [];

    foreach ($holidays as $holiday) {
      $exists_dates[$holiday->date] = $holiday->date;
    }

    $where_basic = $this->buildWhere($year, $client_id);

    if (!empty($dates)) {
      $updated = [];
      $to_add = [];

      foreach ($dates as $date) {
        if (isset($exists_dates[$date])) {
          // Это существующие, пригодятся при определении удаленных.
          $updated[$date] = $date;
        }
        else {
          $to_add[] = ['client_id' => $client_id, 'year' => $year, 'date' => $date];
        }
      }

      // Удаление удаленных.
      foreach ($exists_dates as $exists_date) {
        if (!isset($updated[$exists_date])) {
          $where = $where_basic;
          $where[] = ['date', $exists_date];
          Holiday::where($where)->delete();
        }
      }

      // Добавление добавленных.
      if (!empty($to_add)) {
        DB::table('holiday')->insert($to_add);
      }
    }
    else {
      DB::table('holiday')->where($where_basic)->delete();
    }
  }
}
