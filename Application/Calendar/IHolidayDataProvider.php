<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.06.2020
 * Time: 4:05
 */


namespace Application\Calendar;


use DateTimeImmutable;

interface IHolidayDataProvider {

  /**
   * @param int $client_id
   * @param DateTimeImmutable $startDate
   * @param DateTimeImmutable $endDate
   *
   * @return array
   */
  public function getDates(int $client_id, DateTimeImmutable $startDate, DateTimeImmutable $endDate): array;
}
