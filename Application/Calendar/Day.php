<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.04.2018
 * Time: 16:49
 */

namespace Application\Calendar;

use DateTimeImmutable;

class Day {
  public readonly int $month;
  public readonly int $week;
  public readonly int $week_day;
  public readonly int $day;

  private bool $is_holiday = false;
  private bool $is_official_holiday = false;
  /** @var AmountItem[] */
  private array $amountItems = [];
  private static ?DateTimeImmutable $today = null;

  /**
   * Day constructor.
   *
   * @param DateTimeImmutable $date
   */
  public function __construct(public readonly DateTimeImmutable $date) {
    $this->month = $this->date->format('n');
    $this->week = $this->date->format('W');
    $this->week_day = $this->date->format('N');
    $this->day = $this->date->format('j');

    if (!self::$today) {
      self::$today = new DateTimeImmutable();
    }
  }

  /**
   * @param AmountItem $item
   *
   * @return Day
   */
  public function addAmountItem(AmountItem $item): Day {
    $this->amountItems[] = $item;

    return $this;
  }

  /**
   * @return array
   */
  public function getAmountItems(): array {
    return $this->amountItems;
  }

  /**
   * @return string
   */
  public function getDateString(): string {
    return $this->date->format('Y-m-d');
  }

  /**
   * @return bool
   */
  public function isWeekend(): bool {
    return ($this->week_day >= 6);
  }

  /**
   * @return bool
   */
  public function isHoliday(): bool {
    return $this->is_holiday;
  }

  /**
   *
   */
  public function setIsHoliday(): void {
    $this->is_holiday = true;
  }

  /**
   * @return bool
   */
  public function isOfficialHoliday(): bool {
    return $this->is_official_holiday;
  }

  /**
   *
   */
  public function setIsOfficialHoliday(): void {
    $this->is_official_holiday = true;
  }

  public function isPast(): bool {
    return (int)$this->date->format('Ymd') < (int)self::$today->format('Ymd');
  }

  public function isToday(): bool {
    return (int)$this->date->format('Ymd') === (int)self::$today->format('Ymd');
  }

  /**
   * @return string
   */
  public function cssClasses(): string {
    $classes = [];

    if ($this->isWeekend()) {
      $classes[] = 'weekend';
    }

    if ($this->isHoliday()) {
      $classes[] = 'holiday';
    }

    if ($this->isOfficialHoliday()) {
      $classes[] = 'official-holiday';
    }

    if ($this->isPast()) {
      $classes[] = 'past';
    }

    if ($this->isToday()) {
      $classes[] = 'today';
    }

    if ($this->amountItems) {
      $classes[] = 'has-amount';
    }

    return !empty($classes) ? (' ' . implode(' ', $classes)) : '';
  }
}
