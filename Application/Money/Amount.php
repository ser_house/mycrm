<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.08.2018
 * Time: 11:00
 */

namespace Application\Money;


use JsonSerializable;

class Amount implements JsonSerializable {
  public function __construct(public readonly float $amount, public readonly string $currency = 'RUB') {

  }

  /**
   *
   * @return bool
   */
  public function isEmpty(): bool {
    return empty($this->amount);
  }

  /**
   *
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    return [
      'amount' => $this->amount,
      'currency' => $this->currency,
    ];
  }
}
