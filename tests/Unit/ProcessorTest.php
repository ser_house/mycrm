<?php

namespace Tests\Unit;

use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Placeholder\Processor;


class ProcessorTest extends Base {
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testExample() {
		$dates = [
			'2018-01-01 00:00' => [
				'{month}' => 'Январь',
				'{month year::pd}' => 'декабре 2017',
				'{date}' => '1 января 2018 г.',
			],
			'2018-02-01 23:59' => [
				'{month}' => 'Февраль',
				'{month year::pd}' => 'январе 2018',
				'{date}' => '1 февраля 2018 г.',
			],
			'2018-03-01 23:59' => [
				'{month}' => 'Март',
				'{month year::pd}' => 'феврале 2018',
				'{date}' => '1 марта 2018 г.',
			],
			'2018-03-31 00:00' => [
				'{month}' => 'Март',
				'{month year::pd}' => 'феврале 2018',
				'{date}' => '31 марта 2018 г.',
			],
			'2018-04-04 00:00' => [
				'{month}' => 'Апрель',
				'{month year::pd}' => 'марте 2018',
				'{date}' => '4 апреля 2018 г.',
			],
		];

		$processor = new Processor(new DateTimeFormatter());

		print PHP_EOL;
		foreach ($dates as $date => $results) {
		  foreach($results as $template => $result) {
        $text = $processor->process(new \DateTimeImmutable($date), $template);
        if ($result !== $text) {
          print "date: '$date', template: '$template', text: '$text', wanted: '$result'" . PHP_EOL;
        }
        self::assertTrue($result == $text);
      }
		}

	}
}
