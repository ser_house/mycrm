<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.04.2018
 * Time: 05:22
 */

namespace Tests\Unit;


use Tests\TestCase;

class Base extends TestCase {
	protected function printEol($data, string $label = '') {
		$out = '';

		if (!empty($label)) {
			$out .= "$label:";
		}

		if (!is_string($data)) {
			$out .= PHP_EOL;
			if (is_object($data)) {
				$json = json_encode($data);
				$data = json_decode($json);
			}
			$out .= print_r($data, true);
		}
		else {
			$out .= " $data";
		}

		print PHP_EOL;
		print $out;
		print PHP_EOL;
	}
}
