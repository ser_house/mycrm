<?php

namespace Tests\Unit;

use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;


class FormatTest extends Base {
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testDateTimeFormatter() {
//	  self::assertTrue(true);

	  $dateTime = new \DateTimeImmutable('2018-03-31 10:22:30');

		$formatter = new DateTimeFormatter();
		$date_short = $formatter->dateShort($dateTime);
		self::assertTrue('31.03.2018' === $date_short, "date_short: $date_short" . PHP_EOL);

		$date_long = $formatter->dateLong($dateTime);
		self::assertTrue('31 марта 2018 г.' === $date_long, "date_long: $date_long" . PHP_EOL);

		$date_time_short = $formatter->dateTimeShort($dateTime);
		self::assertTrue('31.03.2018, 10:22' === $date_time_short, "date_time_short: $date_time_short" . PHP_EOL);


		$date_db = $formatter->dateToDb($dateTime);
		self::assertTrue('2018-03-31' === $date_db, "date_db: $date_db" . PHP_EOL);

		$month = $formatter->month($dateTime);
		self::assertTrue('Март' === $month, "month: $month" . PHP_EOL);

		$month_declesion = $formatter->monthDeclesion($dateTime);
		self::assertTrue('марте' === $month_declesion, "month_declesion: $month_declesion" . PHP_EOL);
	}
}
