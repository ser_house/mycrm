<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2020
 * Time: 9:38
 */

namespace Tests\Application\Client\Action;

use App\Model\Client\Client;
use App\Model\User;
use Tests\Feature\TestCase;

class UpdateTest extends TestCase {

  public function testUpdate() {
    $user = User::first();
    $this->be($user);

    $client = Client::where('name', '=', 'Client name')->first();

    $client_data = [
      'name' => 'Client name3',
      'note' => '',
      'contacts' => [
        ['name' => 'contact_name1', 'value' => 'contact_value1'],
        ['name' => 'contact_name2', 'value' => 'contact_value2'],
      ],
      'sites' => [],
      'is_selectable' => false,
    ];

    $response = $this->followingRedirects()->patch(route('client.update', ['client' => $client->id]), $client_data);
    $response->assertStatus(200);

    $client = Client::where('name', '=', $client_data['name'])->first();
    self::assertNotNull($client);
    self::assertEquals($client_data['name'], $client->name);

    self::assertCount(2, $client->contacts);
    self::assertEquals('contact_name1', $client->contacts[0]->first()->name);
    self::assertEquals('contact_value1', $client->contacts[0]->first()->value);
    self::assertEquals('contact_name2', $client->contacts[1]->name);
    self::assertEquals('contact_value2', $client->contacts[1]->value);

    self::assertCount(0, $client->sites);

    self::assertFalse($client->is_selectable);
  }
}
