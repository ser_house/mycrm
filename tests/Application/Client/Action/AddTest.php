<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2020
 * Time: 9:38
 */

namespace Tests\Application\Client\Action;

use App\Model\Client\Client;
use App\Model\User;
use Tests\Feature\TestCase;

class AddTest extends TestCase {

  public function testAdd() {
    $user = User::first();
    $this->be($user);

    $client_data = [
      'name' => 'Client name2',
      'note' => '',
      'contacts' => [
        ['name' => 'contact_name1', 'value' => 'contact_value1'],
      ],
      'sites' => [
        ['url' => 'http://example.com'],
      ],
    ];

    $response = $this->followingRedirects()->post(route('clients.add'), $client_data);
    $response->assertStatus(200);

    self::assertDatabaseHas('client', ['name' => $client_data['name']]);

    $client = Client::where('name', '=', $client_data['name'])->first();
    self::assertNotNull($client);

    self::assertCount(1, $client->contacts);
    self::assertEquals('contact_name1', $client->contacts->first()->name);
    self::assertEquals('contact_value1', $client->contacts->first()->value);

    self::assertCount(1, $client->sites);
    self::assertEquals('http://example.com', $client->sites->first()->url);
  }
}
