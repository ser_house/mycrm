<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.07.2020
 * Time: 12:24
 */


namespace Tests\Application\Task\Action;


use App\Model\Client\Client;
use App\Model\Task\Task;
use App\Model\User;
use Application\Task\State;
use Tests\Feature\TestCase;

class FinishTest extends TestCase {

  public function testRun() {
    $user = User::first();
    $this->be($user);

    $client = Client::first();
    $task = Task::first();

    $route = route('client.task.finish', ['client' => $client->id, 'task' => $task->id]);
    $response = $this->post($route);
    $response->assertStatus(200);
    $response->assertJson(['status' => 'success']);

    $task->refresh();
    self::assertEquals(State::Finished, $task->state);
  }
}
