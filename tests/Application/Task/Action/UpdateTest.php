<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.07.2020
 * Time: 9:52
 */

namespace Tests\Application\Task\Action;

use App\Model\Client\Client;
use App\Model\Task\Task;
use App\Model\User;
use Tests\Feature\TestCase;

class UpdateTest extends TestCase {

  public function testRun() {
    $user = User::first();
    $this->be($user);

    $client = Client::first();
    $task = Task::first();

    $task_data = [
      'title' => 'Task 2',
      'currency' => 'EUR',
    ];

    $route = route('client.task.update', ['client' => $client->id, 'task' => $task->id]);
    $response = $this->followingRedirects()->patch($route, $task_data);
    $response->assertStatus(200);

    $task = Task::find($task->id);
    self::assertEquals($task_data['title'], $task->title);
    self::assertEquals($task_data['currency'], $task->currency);
  }
}
