<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.07.2020
 * Time: 10:40
 */


namespace Tests\Application\Task\Action;


use App\Model\Account;
use App\Model\Client\Client;
use App\Model\Payment;
use App\Model\Task\Task;
use App\Model\User;
use Tests\Feature\TestCase;

class SavePaymentsTest extends TestCase {

  public function testRun() {
    $user = User::first();
    $this->be($user);

    $client = Client::first();
    $task = Task::first();

    $account = Account::first();

    $payment_data = [
      'account_id' => $account->id,
      'created_at' => date('Y-m-d'),
      'amount' => 100.00,
      'note' => 'Note',
    ];

    $route = route('client.task.payments.save', ['client' => $client->id, 'task' => $task->id]);
    $response = $this->followingRedirects()->post($route, [$payment_data]);
    $response->assertStatus(200);

    $payment = Payment::where('task_id', '=', $task->id)->first();
    self::assertNotNull($payment);
    self::assertEquals($payment_data['amount'], $payment->amount);
    self::assertEquals($payment_data['note'], $payment->note);
  }
}
