<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.07.2020
 * Time: 9:52
 */

namespace Tests\Application\Task\Action;

use App\Model\Client\Client;
use App\Model\Task\TaskTemplate;
use App\Model\User;
use Application\Task\State;
use Tests\Feature\TestCase;


class AddTest extends TestCase {

  public function testRun() {
    $user = User::first();
    $this->be($user);

    $client = Client::first();

    $task_data = [
      'user_id' => $user->id,
      'title' => '',
      'description' => '',
      'client_id' => $client->id,
      'client_site_id' => '',
      'currency' => 'RUB',
      'launched_at' => date('Y-m-d'),
      'completed_at' => '',
      'state' => State::New->value,
      'sub_tasks' => [
        [
          'id' => '',
          'title' => 'Основная',
          'task_id' => '',
          'budget' => '',
          'launched_at' => '',
          'completed_at' => '',
          'hours' => '',
        ],
      ],
    ];

    $route = route('client.task.store', ['client' => $client->id]);
    $response = $this->followingRedirects()->post($route, $task_data);
    $response->assertStatus(200);
  }

  public function testCreateByTemplate() {
    $user = User::first();
    $this->be($user);

    $client = Client::first();

    // TaskTemplateSeeder
    $template = TaskTemplate::first();

    $route = route('client.tasks.create_by_template', ['client' => $client->id, 'template_id' => $template->id]);
    $response = $this->get($route);

    $response->assertStatus(200);
    $response->assertJson(['status' => 'success']);
    $response->assertJson(['task' => [
      'title' => 'Task template',
      'description' => 'Task template description',
      'currency' => 'RUB',
      'sub_task' => [
        'budget' => 100.00,
        'hours' => 12,
      ],
    ]]);
  }
}
