<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.06.2020
 * Time: 18:23
 */

namespace Tests\Application\DocGenerator\Generator\TBS;

use App\Model\Client\ClientSettings;
use Application\Calendar\IHolidayDataProvider;
use Application\Client\Document\Template\TemplateOptions;
use Application\DocGenerator\Generator\DocData;
use Application\DocGenerator\Generator\SubTask;
use Application\DocGenerator\Generator\Task;
use Application\DocGenerator\Generator\TBS\KeyFormatter;
use Application\DocGenerator\Generator\Vars\DocLevelVars;
use Application\DocGenerator\Generator\Vars\ItemLevelVars;
use Application\Money\Amount;
use Application\DocGenerator\Generator\TBS\DataBuilder;
use Tests\Feature\Stub\HolidayDataProvider;
use Tests\Feature\TestCase;

class DataBuilderTest extends TestCase {
  private DataBuilder $dataBuilder;

  protected function setUp(): void {
    parent::setUp();

    $this->instance(IHolidayDataProvider::class, new HolidayDataProvider());

    $this->dataBuilder = $this->app->make(DataBuilder::class);
  }

  public function testDocVars() {
    $date_formats = [
      'd.m.Y',
      'd F Y г.',
    ];

    $clientSettings = new ClientSettings([
      'append_hours_suffix' => false,
      'human_amount_upfirst' => false,
      'human_amount_append_units_suffix' => false,
      'date_format' => key($date_formats),
      'document_file_name_pattern' => '',
      'doc:num' => true,
      'doc:date' => true,
      'doc:amount' => true,
      'doc:amount_human' => false,
      'item:num' => true,
      'item:title' => true,
      'item:hours' => true,
      'item:amount' => true,
    ], $date_formats);

    $docLevelVars = new DocLevelVars();
    $keyFormatter = new KeyFormatter();
    $docLevelVars = $docLevelVars->buildBySelectedClientSettings($clientSettings, $keyFormatter);
    $doc_keys = $docLevelVars->getCurrentKeys();

    self::assertCount(3, $doc_keys);
    self::assertContains('num', $doc_keys);
    self::assertContains('date', $doc_keys);
    self::assertContains('amount', $doc_keys);
  }

  public function testItemVars() {
    $date_formats = [
      'd.m.Y',
      'd F Y г.',
    ];

    $clientSettings = new ClientSettings([
      'append_hours_suffix' => false,
      'human_amount_upfirst' => false,
      'human_amount_append_units_suffix' => false,
      'date_format' => key($date_formats),
      'document_file_name_pattern' => '',
      'doc:num' => true,
      'doc:date' => true,
      'doc:amount' => true,
      'doc:amount_human' => true,
      'item:num' => true,
      'item:title' => true,
      'item:hours' => false,
      'item:amount' => true,
    ], $date_formats);

    $keyFormatter = new KeyFormatter();

    $itemLevelVars = new ItemLevelVars();
    $itemLevelVars = $itemLevelVars->buildBySelectedClientSettings($clientSettings, $keyFormatter);
    $item_keys = $itemLevelVars->getCurrentKeys();

    self::assertCount(3, $item_keys);
    self::assertContains('num', $item_keys);
    self::assertContains('title', $item_keys);
    self::assertContains('amount', $item_keys);
  }

  public function testBuildData() {
    $inputs = [
      'doc_number' => 1,
      'doc_date' => '01.06.2020',
    ];
    $client_id = 1;

    $currency = 'RUB';
    $tasks = [
      new Task(1, 'Title', 'Description', $currency,
        new \DateTimeImmutable('2020-05-01'), new \DateTimeImmutable('2020-05-31')),
      new Task(2, 'Title', 'Description', $currency,
        new \DateTimeImmutable('2020-05-01'), new \DateTimeImmutable('2020-05-31')),
    ];

    foreach ($tasks as $task) {
      $subTask = new SubTask("Subtask of task {$task->id}, 1000", new Amount(1000, $currency), 0);
      $task->addSubTask($subTask);
      $subTask = new SubTask("Subtask of task {$task->id}, 500", new Amount(500, $currency), 0);
      $task->addSubTask($subTask);
    }

    $docData = new DocData($client_id, $inputs['doc_number'], $inputs['doc_date'], $tasks);

    $date_formats = [
      'd.m.Y',
      'd F Y г.',
    ];
    $templateOptions = new TemplateOptions([
      'append_hours_suffix' => false,
      'human_amount_upfirst' => false,
      'human_amount_append_units_suffix' => false,
      'date_format' => $date_formats[1],
    ], $date_formats);

    $clientSettings = new ClientSettings([
      'append_hours_suffix' => false,
      'human_amount_upfirst' => false,
      'human_amount_append_units_suffix' => false,
      'date_format' => $date_formats[1],
      'document_file_name_pattern' => '',
    ], $date_formats);

    $doc_data = $this->dataBuilder->buildDocData($docData, $templateOptions, $clientSettings)[0];

    self::assertEquals(1, $doc_data['num']);
    self::assertEquals('1 июня 2020 г.', $doc_data['date']);
    self::assertEquals('1 мая 2020 г.', $doc_data['date_start']);
    self::assertEquals('31 мая 2020 г.', $doc_data['date_end']);
    self::assertEquals('3 000 р.', $this->clearAfterNumberFormatter($doc_data['amount']));
    self::assertEquals('три тысячи', $doc_data['amount_human']);
    self::assertEmpty($doc_data['hours']);
    self::assertEquals(28, $doc_data['days_count']);
    self::assertEquals('107,14 р.', $this->clearAfterNumberFormatter($doc_data['day_amount']));

    $items_data = $this->dataBuilder->buildItemsData($docData, $templateOptions, $clientSettings);

    self::assertCount(4, $items_data);

    $item = $items_data[0];
    self::assertEquals(1, $item['num']);
    self::assertEquals('Subtask of task 1, 1000', $item['title']);
    self::assertEmpty($item['hours']);
    self::assertEquals('1 000 р.', $this->clearAfterNumberFormatter($item['amount']));

    $item = $items_data[1];
    self::assertEquals(2, $item['num']);
    self::assertEquals('Subtask of task 1, 500', $item['title']);
    self::assertEmpty($item['hours']);
    self::assertEquals('500 р.', $this->clearAfterNumberFormatter($item['amount']));

    $item = $items_data[2];
    self::assertEquals(3, $item['num']);
    self::assertEquals('Subtask of task 2, 1000', $item['title']);
    self::assertEmpty($item['hours']);
    self::assertEquals('1 000 р.', $this->clearAfterNumberFormatter($item['amount']));

    $item = $items_data[3];
    self::assertEquals(4, $item['num']);
    self::assertEquals('Subtask of task 2, 500', $item['title']);
    self::assertEmpty($item['hours']);
    self::assertEquals('500 р.', $this->clearAfterNumberFormatter($item['amount']));
  }

  private function clearAfterNumberFormatter(string $formatted): string {
    // https://stackoverflow.com/questions/57812473/phpunit-failed-asserting-two-strings-are-equal
    return str_replace("\xc2\xa0", ' ', $formatted);
  }
}
