<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.07.2020
 * Time: 14:49
 */


namespace Tests\Application\Account;

use App\Model\Account;
use App\Model\AccountType;
use App\Model\User;
use Tests\Feature\TestCase;

class UpdateTest extends TestCase {

  public function testUpdate() {
    $user = User::first();
    $this->be($user);

    $accountType = AccountType::first();

    $accounts_data = [
      [
        'user_id' => $user->id,
        'type_id' => $accountType->id,
        'name' => 'Account 1 rub',
        'note' => '',
        'currency' => 'RUB',
        'is_active' => 1,
      ],
      [
        'user_id' => $user->id,
        'type_id' => $accountType->id,
        'name' => 'Account 2 eur',
        'note' => '',
        'currency' => 'EUR',
        'is_active' => 1,
      ]
    ];

    $response = $this->followingRedirects()->post(route('accounts.update'), ['accounts' => $accounts_data]);
    $response->assertStatus(200);

    $accounts = Account::all();
    self::assertCount(2, $accounts);

    self::assertEquals($accounts_data[0]['name'], $accounts[0]->name);
    self::assertEquals($accounts_data[0]['note'], $accounts[0]->note);
    self::assertEquals($accounts_data[0]['currency'], $accounts[0]->currency);

    self::assertEquals($accounts_data[1]['name'], $accounts[1]->name);
    self::assertEquals($accounts_data[1]['note'], $accounts[1]->note);
    self::assertEquals($accounts_data[1]['currency'], $accounts[1]->currency);

    $accounts_data = [
      [
        'user_id' => $user->id,
        'type_id' => $accountType->id,
        'name' => 'Account 2 eur',
        'note' => '',
        'currency' => 'EUR',
        'is_active' => 1,
      ]
    ];

    $response = $this->followingRedirects()->post(route('accounts.update'), ['accounts' => $accounts_data]);
    $response->assertStatus(200);

    $accounts = Account::all();
    self::assertCount(1, $accounts);

    self::assertEquals($accounts_data[0]['name'], $accounts[0]->name);
    self::assertEquals($accounts_data[0]['note'], $accounts[0]->note);
    self::assertEquals($accounts_data[0]['currency'], $accounts[0]->currency);

    $account_id = $accounts[0]->id;

    $accounts_data = [
      [
        'id' => $account_id,
        'currency' => 'USD',
      ]
    ];

    $response = $this->followingRedirects()->post(route('accounts.update'), ['accounts' => $accounts_data]);
    $response->assertStatus(200);

    $account = Account::find($account_id);
    self::assertEquals($accounts_data[0]['currency'], $account->currency);
  }
}
