<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.03.2020
 * Time: 12:37
 */


namespace Tests\Feature;

use Illuminate\Testing\TestResponse;

class CustomTestResponse extends TestResponse {

  /**
   * Get the assertion message for assertJson.
   *
   * @param  array  $data
   * @return string
   */
  protected function assertJsonMessage(array $data)
  {
    // Перекрываем, чтобы JSON_UNESCAPED_UNICODE
    $expected = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

    $actual = json_encode($this->decodeResponseJson(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

    return 'Unable to find JSON: '.PHP_EOL.PHP_EOL.
      "[{$expected}]".PHP_EOL.PHP_EOL.
      'within response JSON:'.PHP_EOL.PHP_EOL.
      "[{$actual}].".PHP_EOL.PHP_EOL;
  }
}
