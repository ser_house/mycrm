<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.06.2020
 * Time: 19:36
 */


namespace Tests\Feature\Stub;


use Application\Calendar\IHolidayDataProvider;

class HolidayDataProvider implements IHolidayDataProvider {
  /**
   * @inheritDoc
   */
  public function getDates(int $client_id, \DateTimeImmutable $startDate, \DateTimeImmutable $endDate): array {
    $items = [
      $startDate->format('Y-m-01'),
      $startDate->format('Y-m-02'),
    ];

    $holidays = [];

    foreach ($items as $item) {
      $holidays[$item] = $item;
    }

    return $holidays;
  }
}
