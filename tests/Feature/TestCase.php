<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.11.2019
 * Time: 11:34
 */


namespace Tests\Feature;


use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\CreatesApplication;


class TestCase extends BaseTestCase {
  use CreatesApplication;
  use DatabaseMigrations;
  use RefreshDatabase;

  protected function setUp(): void {
    parent::setUp();

    $this->artisan('db:seed');
  }

  // Для случая, когда мы хотим посмотреть тестовую базу после тестов.
//	use RefreshDatabase; не использовать
  /**
   * Define hooks to migrate the database before and after each test.
   *
   * @return void
   */
//	public function runDatabaseMigrations() {
//
//		$this->artisan('migrate:fresh');
//	}

  /**
   * Сортирует результат аналогично серверной сортировке результатов поиска.
   *
   * @param array $items
   */
  protected function sort(array &$items): void {

    usort($items, function (array $left, array $right) {
      return $left['title'] <=> $right['title'];
    });
  }

  protected function createTestResponse($response) {
    return CustomTestResponse::fromBaseResponse($response);
  }

  protected function refreshTestDatabase(): void {
    if (!RefreshDatabaseState::$migrated) {
      $this->artisan('migrate:fresh', $this->migrateFreshUsing());

      $this->app[Kernel::class]->setArtisan(null);

      RefreshDatabaseState::$migrated = true;
    }
    // Иначе будет PDOException: There is no active transaction
//    $this->beginDatabaseTransaction();
  }
}
