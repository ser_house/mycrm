<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 29.04.2020
 * Time: 10:50
 */

namespace Tests\Feature\User;

use Tests\Feature\TestCase;

class UserTest extends TestCase {

  public function testRegistration() {
    $user = [
      'name' => 'Joe',
      'email' => 'joe_doe@example.com',
      'password' => 'password',
      'password_confirmation' => 'password'
    ];

    $response = $this->post('/register', $user);

    $response->assertRedirect('/');

    //Remove password and password_confirmation from array
    array_splice($user, 2);

    self::assertDatabaseHas('users', $user);
  }

  public function testLogin() {
    $user = [
      'email' => 'joe_doe@example.com',
      'password' => 'password',
    ];

    $response = $this->post('/login', $user);

    $response->assertRedirect('/');
  }
}
