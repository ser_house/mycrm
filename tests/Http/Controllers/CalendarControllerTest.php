<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.07.2020
 * Time: 13:22
 */

namespace Tests\Http\Controllers;

class CalendarControllerTest extends Base {

  public function testAuthorized() {
    $response = $this->get(route('calendar'));
    $response->assertStatus(200);
  }
}
