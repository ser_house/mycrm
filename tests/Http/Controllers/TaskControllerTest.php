<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.07.2020
 * Time: 13:23
 */

namespace Tests\Http\Controllers;

class TaskControllerTest extends Base {
  public function testAuthorized() {
    $response = $this->get(route('tasks'));
    $response->assertStatus(200);
  }
}
