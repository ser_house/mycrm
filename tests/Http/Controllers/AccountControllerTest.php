<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.07.2020
 * Time: 13:25
 */

namespace Tests\Http\Controllers;

class AccountControllerTest extends Base {
  public function testAuthorized() {
    $response = $this->get(route('accounts'));
    $response->assertStatus(200);
  }
}
