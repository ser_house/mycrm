<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.07.2020
 * Time: 13:24
 */

namespace Tests\Http\Controllers;

class ClientsControllerTest extends Base {
  public function testAuthorized() {
    $response = $this->get(route('clients.index'));
    $response->assertStatus(200);
  }
}
