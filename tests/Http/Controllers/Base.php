<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.07.2020
 * Time: 13:29
 */


namespace Tests\Http\Controllers;

use App\Model\User;
use Tests\Feature\TestCase;

class Base extends TestCase{
  protected function setUp(): void {
    parent::setUp();

    $user = User::first();
    $this->be($user);
  }
}
