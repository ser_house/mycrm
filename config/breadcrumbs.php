<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.07.2020
 * Time: 11:18
 */

return [
  'param_obj_label_keys' => [
    'user' => 'name',
    'client' => 'name',
    'task' => 'title',
  ],
  'segment_titles' => [
    'clients' => 'Клиенты',
    'accounts' => 'Счета',
    'tasks' => 'Задачи',
    'documents' => 'Документы',
    'templates' => 'Шаблоны',
    'task-templates' => 'Шаблоны задач',
    'settings' => 'Настройки',
    'generator' => 'Генератор',
    'profile' => 'Профиль',
    'incoming' => 'Доходы',
    'calendar' => 'Календарь',
    'holidays' => 'Выходные',
    'admin' => 'Администрирование',
    'cities' => 'Города',
    'styles' => 'Стили',
    'form' => 'Форма',
    'color' => 'Цвет',
    'icons' => 'Иконки',
    'edit' => 'редактирование',
    'create' => 'создание',
  ],
  'last_segment' => [
    'show' => true,
    'as_link' => false,
  ],
];
