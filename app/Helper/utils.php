<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.03.2018
 * Time: 08:34
 *
 * @param $data
 * @param string $label
 *
 * @return void
 */

function beauty_dump($data, string $label = ''): void {
  if (!empty($label)) {
    dump([$label => $data]);
  }
  else {
    dump($data);
  }
}

/**
 * Склоняем словоформу
 * @ author runcore
 *
 * @param $n
 * @param $f1
 * @param $f2
 * @param $f5
 *
 * @return mixed
 */
function morph($n, $f1, $f2, $f5) {
  $n = abs((int)$n) % 100;
  if ($n > 10 && $n < 20) {
    return $f5;
  }
  $n %= 10;
  if ($n > 1 && $n < 5) {
    return $f2;
  }
  if ($n == 1) {
    return $f1;
  }

  return $f5;
}

if (!function_exists('mb_ucfirst')) {
  function mb_ucfirst($string, $encoding = 'UTF-8') {
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
  }
}


/**
 * @param Iterator|string $dir
 *  The directory to search in or an iterator
 * @param callable $validator
 *  принимает название класса, должен вернуть bool - включать класс в результат или нет
 *
 * @return array
 */
function load_implements_classes_from_dir($dir, callable $validator): array {
  if (is_string($dir)) {
    $dir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir));
  }

  $map = [];

  foreach ($dir as $file) {
    if (!$file->isFile()) {
      continue;
    }

    $path = $file->getRealPath() ?: $file->getPathname();

    if ('php' !== pathinfo($path, PATHINFO_EXTENSION)) {
      continue;
    }

    $classes = get_classes_from_dir($path);

    foreach ($classes as $class) {
      if ($validator($class)) {
        $map[$class] = $path;
      }
    }
  }

  return $map;
}

/**
 * Extract the classes in the given file.
 *
 * @param string $path The file to check
 *
 * @return array The found classes
 */
function get_classes_from_dir($path) {
  $contents = file_get_contents($path);
  $tokens = token_get_all($contents);
  $classes = [];
  $namespace = '';

  for ($i = 0; isset($tokens[$i]); ++$i) {
    $token = $tokens[$i];
    if (!isset($token[1])) {
      continue;
    }

    $class = '';

    switch ($token[0]) {

      case T_NAMESPACE:
        $i++;
        while (empty($tokens[$i++][1])) {
        }
        $namespace = $tokens[$i][1] . '\\';
        break;

      case T_CLASS:
        $isClassConstant = false;
        for ($j = $i - 1; $j > 0; --$j) {
          if (!isset($tokens[$j][1])) {
            break;
          }

          if (T_DOUBLE_COLON === $tokens[$j][0]) {
            $isClassConstant = true;
            break;
          }
          if (!in_array($tokens[$j][0], [T_WHITESPACE, T_DOC_COMMENT, T_COMMENT])) {
            break;
          }
        }

        if ($isClassConstant) {
          break;
        }

        // Find the classname
        while (isset($tokens[++$i][1])) {
          $t = $tokens[$i];
          if (T_STRING === $t[0]) {
            $class .= $t[1];
          }
          elseif ('' !== $class && T_WHITESPACE === $t[0]) {
            break;
          }
        }
        $classes[] = ltrim($namespace . $class, '\\');
        break;

      default:
        break;
    }
  }

  return $classes;
}
