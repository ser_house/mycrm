<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.03.2018
 * Time: 21:05
 */

namespace App\ViewComposers;


use App\Model\Client\Client;
use App\Model\Task\Task;
use Auth;
use Illuminate\View\View;

class ClientsComposer {
  /**
   * Bind data to the view.
   *
   * @param View $view
   *
   * @return void
   */
  public function compose(View $view): void {
    if (Auth::user()) {
      $user_id = Auth::user()->id;
      $is_selectable = 1;

      $active_clients = Client::where([
        ['user_id', $user_id],
        ['is_selectable', $is_selectable],
      ])->orderBy('name', 'desc')->get();
      $view->with('active_clients', $active_clients->count() ? $active_clients : null);

      $client = $this->getClient();
      $action = $this->getClientAction();

      $view->with('client', $client);
      $view->with('action', $action);
    }
  }

  private function getClient(): ?Client {
    $route = request()?->route();
    $client = $route?->parameter('client');
    if (empty($client)) {
      $client = request()?->get('client');
      if (empty($client)) {
        $task_id = $route?->parameter('task_id');
        if (!empty($task_id)) {
          /** @var Task $task */
          $task = Task::findOrFail($task_id);
          $client = $task->client;
        }
      }
    }

    return $client;
  }

  /**
   * @return string
   */
  private function getClientAction(): string {
    $route = request()?->route();

    $client = $route->parameter('client');
    if (empty($client)) {
      return '';
    }

    $prefix = $route->getPrefix();
    $name = $route->getName();
    $action = $route->getActionMethod();

    $actions = [
      'client.show',
      'client.edit',
      'client.tasks',
      'client.task.create',
      'client.task_templates',
      'client.documents',
      'client.documents.templates',
      'client.documents.settings',
      'client.documents.generator',
      'client.calendar',
      'client.holidays',
    ];

    if (in_array($name, $actions)) {
      return $name;
    }

    if ('{client}' !== $prefix) {
      return '';
    }

    return $action;
  }
}
