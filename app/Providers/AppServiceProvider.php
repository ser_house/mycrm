<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Application\DocGenerator\Generator\IKeyFormatter;
use Application\DocGenerator\Generator\TBS\KeyFormatter;
use Application\Calendar\IHolidayDataProvider;
use Application\Infrastructure\Holiday\DataProvider;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider {
  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot(): void {
    Schema::defaultStringLength(191);

    Blade::directive('spaceless', static function () {
      return '<?php ob_start() ?>';
    });

    Blade::directive('endspaceless', static function () {
      return "<?php echo preg_replace('/>\\s+</', '><', ob_get_clean()); ?>";
    });

    Blade::directive('nl2br', static function ($expression) {
      return "<?php echo str_replace(PHP_EOL, '<br/>', e($expression)); ?>";
    });

    Blade::directive('field_error', static function ($expression) {
      $opened = "<span class='help-block error'><span class='icon icon-error'></span>";
      return '<?php if ($errors->has(' . $expression . ')) {
echo "' . $opened . '", $errors->first(' . $expression . '), "</span>";
}
?>';
    });
    Paginator::useBootstrap();
  }


  /**
   * Register any application services.
   *
   * @return void
   */
  public function register(): void {
    $this->app->bind(IKeyFormatter::class, KeyFormatter::class, true);
    $this->app->bind(IHolidayDataProvider::class, DataProvider::class, true);
  }
}
