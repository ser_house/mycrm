<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.03.2018
 * Time: 09:06
 */

namespace App\Providers;

use App\ViewComposers\ClientsComposer;
use View;
use Illuminate\Support\ServiceProvider;


class ViewServiceProvider extends ServiceProvider{
	public function boot(): void {
		View::composer('layouts.app', ClientsComposer::class);
	}
}
