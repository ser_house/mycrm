<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.04.2020
 * Time: 10:24
 */


namespace App\Policies;


use App\Model\Client\Client;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy {
  use HandlesAuthorization;

  /**
   * @param User $user
   * @param Client $client
   *
   * @return bool
   */
  public function client(User $user, Client $client): bool {
    return $user->id === $client->user_id;
  }
}
