<?php

namespace App\Policies;

use App\Model\File;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FilePolicy {
  use HandlesAuthorization;

  /**
   * @param User $user
   * @param \App\Model\File $file
   *
   * @return bool
   */
  public function delete(User $user, File $file): bool {
    return $user->id === $file->user_id;
  }

}
