<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Routing\Controller;
use Application\Task\DataProvider as TaskDataProvider;
use Application\Task\View\Presenter as TaskPresenter;
use Illuminate\View\View;

class TaskController extends Controller {

  /**
   * TaskController constructor.
   *
   * @param TaskDataProvider $taskDataProvider
   * @param TaskPresenter $taskPresenter
   */
  public function __construct(
    private readonly TaskDataProvider $taskDataProvider,
    private readonly TaskPresenter $taskPresenter
  ) {
  }

  /**
   * @return Factory|View|Application
   * @throws \Exception
   */
  public function index(): Factory|View|Application {
    $user_id = Auth::user()->id;

    $tasks = $this->taskDataProvider->getUserTasks($user_id);
    $vars = [
      'tasks' => $this->taskPresenter->shortList($tasks->items()),
      'pager' => $tasks->links(),
    ];

    return view('task.index', $vars);
  }
}
