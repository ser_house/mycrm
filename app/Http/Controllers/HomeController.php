<?php

namespace App\Http\Controllers;

use Application\Incoming\Service as IncomingService;
use Application\Incoming\View\Presenter as IncomingPresenter;
use Application\Task\DataProvider as TaskDataProvider;
use Application\Task\View\Presenter as TaskPresenter;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 */
class HomeController extends Controller {
  public function __construct(
    private readonly IncomingPresenter $incomingPresenter,
    private readonly IncomingService $incomingService,
    private readonly TaskDataProvider $taskDataProvider,
    private readonly TaskPresenter $taskPresenter
  ) {
  }

  /**
   *
   * @return Factory|View
   */
  public function index(): Factory|View {
    $data = [];
    if ($user = Auth::user()) {
      $user_id = $user->id;

      $tasks = $this->taskDataProvider->getUserActiveTasks($user_id);
      $task_views = [
        'tasks' => $this->taskPresenter->shortList($tasks->items()),
        'pager' => $tasks->links(),
      ];

      $data = [
        'active_tasks' => $task_views,
        'by_years' => null,
        'current_year' => null,
        'is_incoming_empty' => true,
      ];

      $userIncomingByYearsData = $this->incomingService->userByYears($user_id);
      if ($userIncomingByYearsData) {
        $data['by_years'] = $this->incomingPresenter->presentByYears($userIncomingByYearsData);
        $data['is_incoming_empty'] = false;
      }

      $currentYearIncomingByMonth = $this->incomingService->userCurrentYearByMonths($user_id);
      if ($currentYearIncomingByMonth) {
        $data['current_year'] = $this->incomingPresenter->presentByMonths($currentYearIncomingByMonth);
        $data['is_incoming_empty'] = false;
      }
    }

    return view('home', $data);
  }
}
