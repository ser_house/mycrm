<?php

namespace App\Http\Controllers;

use Application\Incoming\Service as IncomingService;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Routing\Controller;
use Illuminate\View\View;

/**
 * Class IncomingController
 *
 * @package App\Http\Controllers
 */
class IncomingController extends Controller {
  public function __construct(private readonly IncomingService $incomingService) {
  }

  /**
   *
   * @return Factory|Application|View
   */
  public function index(): Factory|Application|View {

    $user_id = Auth::user()->id;

    $items = $this->incomingService->getUserIncomingDetailsAsTree($user_id);

    return view('incoming', [
      'items' => $items,
    ]);
  }
}
