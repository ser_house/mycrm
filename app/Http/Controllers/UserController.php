<?php

namespace  App\Http\Controllers;

use Application\Money\Currency;
use App\Model\User;
use Config;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\View\View;


class UserController extends Controller {

  /**
   * @param User $user
   *
   * @return Factory|Application|View
   * @throws Exception
   */
  public function form(User $user): Application|Factory|View {
    $tz = $user->timezone ?: Config::get('app.timezone');
    $user_currencies = Currency::currenciesByCodes($user->getCurrencies());
    $currencies = Currency::currencies();

		return view('user.form', [
		  'id' => $user->id,
      'name' => $user->name,
      'email' => $user->email,
      'timezones' => $this->getTimezones(),
      'current_tz' => $tz,
      'user_currencies' => $user_currencies,
      'currencies' => $currencies,
    ]);
	}

  /**
   * @param Request $request
   * @param User $user
   *
   * @return Factory|Application|View
   * @throws Exception
   */
  public function save(Request $request, User $user): Application|Factory|View {
    $inputs = $request->all();

    $user->name = $inputs['name'];
    $user->email = $inputs['email'];
    $user->timezone = $inputs['tz'];
    $user->currencies = serialize(explode(',', $inputs['currencies']));
    $user->save();

    $user_currencies = Currency::currenciesByCodes($user->getCurrencies());
    $currencies = Currency::currencies();

    $request->session()->flash('success', 'Сохранено.');

    return view('user.form', [
      'id' => $user->id,
      'name' => $user->name,
      'email' => $user->email,
      'timezones' => $this->getTimezones(),
      'current_tz' => $user->timezone,
      'user_currencies' => $user_currencies,
      'currencies' => $currencies,
    ]);
  }

  /**
   *
   * @return array
   * @throws Exception
   */
  private function getTimezones(): array {
    $timezones = [];

    foreach (DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, 'RU') as $timezone) {
      $dateTimeZone = new DateTimeZone($timezone);
      $dateTime = new DateTimeImmutable('now', $dateTimeZone);

      $offset = $dateTime->format('P');
      $current = $dateTime->format('H:i');

      $parts = explode('/', $timezone);

      $name = __("cities.$parts[1]");
      $tz = "$current ($name, $offset)";

      $timezones[$timezone] = $tz;
    }

    asort($timezones);

    return $timezones;
  }
}
