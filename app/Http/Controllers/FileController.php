<?php

namespace App\Http\Controllers;

use App\Model\File;
use Application\Infrastructure\Response\Status\Error;
use Application\Infrastructure\Response\Status\Success;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Storage;

class FileController extends Controller {

  public function delete(File $file): \Illuminate\Http\JsonResponse {
    $this->authorize('delete', $file);

    try {
      $filename = $file->name;
      $filepath = $file->path;
      Storage::delete("public/$filepath/$filename");
      $file->delete();

      $response = new Success("Файл $filename удален");
      return response()->json($response, 200);
    }
    catch (ModelNotFoundException $e) {
      $response = new Error($e->getMessage());
      return response()->json($response, 404);
    }
  }
}
