<?php

namespace App\Http\Controllers;

use App\Model\Client\Client;
use App\Model\Document;
use App\Model\DocumentTemplate;
use Application\DocGenerator\DocInput;
use Application\DocGenerator\Service as DocGeneratorService;
use Application\Infrastructure\Formatter\Exception as ExceptionFormatter;
use Application\Task\State;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use Session;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Throwable;
use ZipArchive;


/**
 * Class DocGeneratorController
 *
 * @package App\Http\Controllers
 */
class DocGeneratorController extends Controller {

  public function __construct(
    private readonly DocGeneratorService $generatorService,
    private readonly ExceptionFormatter $exceptionFormatter,
  ) {

  }


  /**
   * @param Request $request
   * @param Client $client
   *
   * @return Factory|Application|View
   */
  public function generator(Request $request, Client $client): Application|Factory|View {
    if ($dbg = Session::get('dbg')) {
      dump($dbg);
      Session::remove('dbg');
    }

    $can_to_pdf = $this->commandExists('lowriter');
    $state = $request->get('state');

    if (!$state) {
      $has_templates = $this->generatorService->hasTemplatesForGenerator($client->id);
      $has_items = $this->generatorService->hasItemsForGenerateFinished($client->id);

      if (!$has_templates || !$has_items) {
        $vars = [
          'client' => $client,
          'missing' => [
            'templates' => !$has_templates,
            'tasks' => !$has_items,
          ],
        ];

        return view('client.documents.generator.impossible', $vars);
      }
    }

    $vars = [
      'client' => $client,
      'can_to_pdf' => $can_to_pdf,
      'only_closed' => !empty($state),
    ];
    return view('client.documents.generator.generator', $vars);
  }

  /**
   * @param $command_name
   *
   * @return bool
   */
  public function commandExists($command_name) {
    return (null === shell_exec("command -v $command_name")) ? false : true;
  }

  /**
   * @param Client $client
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function dataToGeneratorFinished(Client $client) {
    $templates = $this->generatorService->getTemplatesForGenerator($client);
    $tasks = $this->generatorService->getItemsForGeneratorDefault($client->id);

    if ($templates->isEmpty()) {
      return response()->json([
        'status' => 'fail',
        'msg' => 'Нет шаблонов документов.',
      ]);
    }
    if ($tasks->isEmpty()) {
      return response()->json([
        'status' => 'fail',
        'msg' => 'Нет задач в статусе ' . State::Finished->label() . '.',
      ]);
    }
    $new_number = $this->generatorService->getNextDocNum($client, $templates);
    $response = [
      'status' => 'success',
      'new_number' => $new_number,
      'templates' => $templates->toArray(),
      'tasks' => $tasks->getItems(),
    ];

    return response()->json($response);
  }

  /**
   * @param Client $client
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function dataToGeneratorClosed(Client $client) {
    $templates = $this->generatorService->getTemplatesForGenerator($client);
    $tasks = $this->generatorService->getItemsForGeneratorClosed($client->id);
    if ($templates->isEmpty()) {
      return response()->json([
        'status' => 'fail',
        'msg' => 'Нет шаблонов документов.',
      ]);
    }
    if ($tasks->isEmpty()) {
      return response()->json([
        'status' => 'fail',
        'msg' => 'Нет задач в статусе ' . State::Closed->label() . '.',
      ]);
    }
    $new_number = $this->generatorService->getNextDocNum($client, $templates);
    $response = [
      'status' => 'success',
      'new_number' => $new_number,
      'templates' => $templates->toArray(),
      'tasks' => $tasks->getItems(),
      'years' => $this->generatorService->getClientTaskYears($client->id),
    ];
    return response()->json($response);
  }

  /**
   * @param Request $request
   * @param Client $client
   *
   * @return RedirectResponse|BinaryFileResponse
   */
  public function generate(Request $request, Client $client) {
    $inputs = $request->all();


    $save_to_client = !empty($inputs['save_to_client_files']);
    $to_pdf = !empty($inputs['to_pdf']);
    $template_id = $inputs['template'];
    $template = DocumentTemplate::findOrFail($template_id);
    $documentTypes = Document::types();
    $documentType = $documentTypes[$template->type_id];

    $docs = $inputs['docs'];
    $doc_inputs = [];
    foreach ($docs as $doc) {
      $doc_inputs[] = DocInput::fromArray($doc);
    }
    $documents = $this->generatorService->buildDocumentsForGenerate($client->id, $doc_inputs);

    $as_archive = count($documents) > 1;

    if ($as_archive) {
      $download_file_name = $documentType->getTitleForMulti() . '.zip';
      $to_remove = [];
      try {
        $zip_file = $documentType->machine_name . 's.zip';
        $zip_file_path = public_path() . "/$zip_file";

        $zip = new ZipArchive();
        $opened = $zip->open($zip_file_path, ZipArchive::OVERWRITE | ZipArchive::CREATE);
        if (false === $opened) {
          throw new Exception("Невозможно открыть $zip_file_path");
        }

        foreach ($documents as $docData) {
          $generatedFile = $this->generatorService->generateFromTemplate(
            $docData,
            $client,
            $template,
            $save_to_client,
            $to_pdf
          );

          $file = basename($generatedFile->file_uri);
          $file_path = $save_to_client ? "$generatedFile->file_path/$file" : $generatedFile->file_path;
          if (!file_exists($file_path)) {
            throw new Exception("Файл не существует $file_path: " . '<pre>' . print_r($generatedFile, true) . '</pre>');
          }

          $ext = pathinfo($file_path, PATHINFO_EXTENSION);
          $filename = "$generatedFile->file_name.$ext";
          $zip->addFile($file_path, $filename);
          if (!$save_to_client) {
            $to_remove[] = $file_path;
          }
        }

        $zip->close();
        if (!empty($to_remove)) {
          foreach ($to_remove as $to_remove_file) {
            unlink($to_remove_file);
          }
        }
        return response()->download($zip_file_path, $download_file_name)->deleteFileAfterSend();
      }
      catch (Throwable $e) {
        $output = $this->exceptionFormatter->format($e);
        return redirect()->back()->with('error', $output)->withInput();
      }
    }
    else {
      try {
        $docData = $documents[0];
        $generatedFile = $this->generatorService->generateFromTemplate(
          $docData,
          $client,
          $template,
          $save_to_client,
          $to_pdf
        );
        $file = basename($generatedFile->file_uri);
        $file_path = $save_to_client ? "$generatedFile->file_path/$file" : $generatedFile->file_path;
        if (!file_exists($file_path)) {
          throw new Exception("Файл не существует $file_path: " . '<pre>' . print_r($generatedFile, true) . '</pre>');
        }
        $ext = pathinfo($file_path, PATHINFO_EXTENSION);
        $filename = "$generatedFile->file_name.$ext";

        if (!$save_to_client) {
          return response()->download($file_path, $filename)->deleteFileAfterSend();
        }
        return response()->download($file_path, $filename);
      }
      catch (Throwable $e) {
        $output = $this->exceptionFormatter->format($e);
        return redirect()->back()->with('error', $output)->withInput();
      }
    }
  }
}
