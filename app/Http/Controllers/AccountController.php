<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.01.2018
 * Time: 10:58
 */

namespace App\Http\Controllers;

use Application\Account\DataProvider;
use Application\Account\AccountSaver;
use Application\Account\View\Presenter;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;


/**
 * Class AccountController
 *
 * @package App\Http\Controllers
 */
class AccountController extends Controller {
  public function __construct(
    private readonly AccountSaver $accountSaver,
    private readonly DataProvider $dataProvider,
    private readonly Presenter $accountPresenter
  ) {

  }

  /**
   *
   * @return Factory|Application|View
   */
  public function index(): Application|Factory|View {
    $user_id = Auth::user()->id;

    $models = $this->dataProvider->getUserAccounts($user_id);
    $accounts = $this->accountPresenter->presentEditItems($models);
    $types = $this->dataProvider->getTypes();
    $currencies = $this->dataProvider->getUserCurrencies($user_id);

    $view_data = [
      'accounts' => $accounts,
      'types' => $types,
      'currencies' => $currencies,
    ];

    if (empty($currencies)) {
      $view_data['missing_user_currencies'] = true;
    }

    return view('account.index', $view_data);
  }

  /**
   * @param Request $request
   *
   * @return RedirectResponse
   * @throws \Exception
   * @throws \Exception
   */
  public function update(Request $request): RedirectResponse {
    $user_id = Auth::user()->id;
    $accounts = $request->get('accounts');

    $this->accountSaver->update($accounts, $user_id);

    return back()->with('success', 'Счета сохранены.');
  }
}
