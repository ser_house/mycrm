<?php

namespace App\Http\Controllers;

use Application\Infrastructure\Formatter\Money;
use App\Model\Client\Client;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Application\Calendar\CalendarService;
use Illuminate\View\View;
use ReflectionException;
use Throwable;

class CalendarController extends Controller {
  public function __construct(
    private readonly CalendarService $calendarService,
    private readonly Money $moneyFormatter
  ) {
  }

  /**
   * @param Request $request
   * @param Client $client
   *
   * @return Factory|Application|View
   */
  public function clientCalendar(Request $request, Client $client): Application|Factory|View {
    $years = $this->calendarService->getClientPaymentYears($client->id);
    $year = $request->get('year', date('Y'));
    if (!in_array($year, $years)) {
      $year = last($years);
    }

    // @todo: добавить презентер, чтобы форматер не передавать в представление.
    $days = $this->calendarService->getYearDates($year);
    $data = $this->calendarService->buildClientCalendarData($client, $year, $days);

    return view('calendar.client', [
      'data' => $data,
      'years' => $years,
      'client' => $client,
      'year' => $year,
      'moneyFormatter' => $this->moneyFormatter,
    ]);
  }

  /**
   * @param Request $request
   *
   * @return Factory|Application|View
   */
  public function calendar(Request $request): Application|Factory|View {
    $years = $this->calendarService->getPaymentYears();
    $year = $request->get('year', date('Y'));
    if (!in_array($year, $years)) {
      $year = last($years);
    }

    $days = $this->calendarService->getYearDates($year);
    $data = $this->calendarService->buildCalendarData($year, $days);

    return view('calendar.all', [
      'data' => $data,
      'years' => $years,
      'year' => $year,
      'moneyFormatter' => $this->moneyFormatter,
    ]);
  }

  /**
   * @param Request $request
   * @param Client $client
   *
   * @return Factory|Application|View
   * @throws ReflectionException
   */
  public function holidays(Request $request, Client $client): Application|Factory|View {
    $currentYear = (int)date('Y');

    $year = (int)$request->get('year', $currentYear);

    $years = [
      $currentYear,
      $currentYear + 1,
    ];

    $days = $this->calendarService->getHolidayYearDates($year);
    $holidays = $this->calendarService->getHolidaysAsDatesList($year, $client->id);
    $this->calendarService->setHolidaysToDates($days, $holidays);
    $data = $this->calendarService->buildCalendarData($year, $days);

    $holidays = $this->calendarService->holidaysToDays($holidays);

    $holidays_to_str = [];
    foreach ($holidays as $holiday) {
      $holidays_to_str[$holiday->getDateString()] = $holiday->date->format('d.m.Y');
    }
    $holidays = $holidays_to_str;

    return view('calendar.holidays', [
      'data' => $data,
      'holidays' => $holidays,
      'years' => $years,
      'client' => $client,
      'year' => $year,
    ]);
  }

  /**
   * @param Request $request
   * @param Client $client
   *
   * @return RedirectResponse|null
   */
  public function saveHolidays(Request $request, Client $client): ?RedirectResponse {
    try {
      $year = $request->get('year');
      $dates = array_keys($request->get('date', []));
      $this->calendarService->saveHolidays($dates, $year, $client->id);
      return redirect()->route('client.holidays', ['client' => $client->id])->with('success', 'Выходные сохранены.');
    }
    catch (Throwable $e) {
      return redirect()->back()->with('error', $e->getMessage());
    }
  }

}
