<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller as BaseController;
use App\Model\Client\Client;
use Application\Client\DataProvider;
use Application\Client\View\Presenter as ClientPresenter;
use Application\Infrastructure\Response\Status\Success;
use Auth;
use DB;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class Controller
 *
 * @package App\Http\Controllers\Client
 */
class Controller extends BaseController {
  private const DOCUMENTS_PER_PAGE = 24;

  public function __construct(
    private readonly DataProvider $dataProvider,
    private readonly ClientPresenter $clientPresenter
  ) {

  }

  /**
   * @param Request $request
   *
   * @return Factory|Application|View
   */
  public function index(Request $request): Application|Factory|View {
    $user_id = Auth::user()->id;
    $active = $request->get('active', 'active');
    $clients = $this->dataProvider->getUserClients($active, $user_id);

    return view('client.clients', [
      'clients' => $this->clientPresenter->shortViews($clients),
      'active' => $active,
    ]);
  }

  /**
   * @param Client $client
   *
   * @return Factory|Application|View
   */
  public function show(Client $client): Factory|Application|View {
    $documents = $this->dataProvider->getClientShowDocuments($client, self::DOCUMENTS_PER_PAGE);
    $items = $documents->items();
    $data = [
      'client' => $this->clientPresenter->full($client),
      'documents' => $this->clientPresenter->documents($items),
      'documents_pager' => $documents->links(),
    ];

    return view('client.show', $data);
  }

//  public function remove($client_id) {
//    try {
//      $client = client_load($client_id);
//      $client_name = $client->name;
//      $client->delete();
//      client_reset($client_id);
//
//      return back()->with('success', "Клиент $client_name удален");
//    }
//    catch (\Throwable $e) {
//      return back_with_throwable($e);
//    }
//  }

  /**
   * @param Request $request
   *
   * @return JsonResponse
   */
  public function filesWeightUpdate(Request $request): JsonResponse {
    // @todo: не используется, собственно, и пока даже и надобности нет (хотя была когда-то).
    $items = $request->get('items');
    $table = $request->get('type');

    foreach ($items as $item) {
      $params = [
        'id' => $item['id'],
        'weight' => $item['weight'],
      ];
      DB::update("UPDATE $table SET weight = :weight WHERE id = :id", $params);
    }

    $response = new Success("Вес обновлен");

    return response()->json($response);
  }
}
