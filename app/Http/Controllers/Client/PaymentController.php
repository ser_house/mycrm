<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.04.2020
 * Time: 19:06
 */

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Model\Client\Client;
use App\Model\Task\Task;
use Application\Infrastructure\Response\Status\Error;
use Application\Infrastructure\Response\Status\Success;
use Application\Task\Action\SavePayments as TaskSavePaymentsAction;
use Application\Task\Payment\View\Presenter as PaymentPresenter;
use Application\Task\State;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;
use Application\Account\DataProvider as AccountDataProvider;
use Application\Account\View\Presenter as AccountPresenter;

/**
 * Class PaymentController
 *
 * @package App\Http\Controllers\Client
 */
class PaymentController extends Controller {
  public function __construct(
    private readonly TaskSavePaymentsAction $taskSavePaymentsAction,
    private readonly PaymentPresenter $paymentPresenter,
    private readonly AccountDataProvider $accountDataProvider,
    private readonly AccountPresenter $accountPresenter
  ) {

  }

  /**
   * @param Client $client
   * @param Task $task
   *
   * @return JsonResponse
   */
  public function index(Client $client, Task $task): JsonResponse {
    $payments = $task->payments;

    $user_id = $client->user_id;
    $accounts = $this->accountDataProvider->getTypedActiveAccounts($user_id);
    $account_views = $this->accountPresenter->presentGroupedAccounts($accounts);
    $lastPaymentAccount = $this->accountDataProvider->getLastPaymentAccountForUser($user_id);

    $result = [
      'payments' => $payments,
      'accounts' => $account_views,
      'last_payment_account_id' => $lastPaymentAccount?->id,
    ];

    return response()->json(new Success('', $result));
  }

  /**
   * @param Request $request
   * @param Client $client
   * @param Task $task
   *
   * @return JsonResponse
   */
  public function savePayments(Request $request, Client $client, Task $task): JsonResponse {
    try {
      $data = $request->all();

      $this->taskSavePaymentsAction->run($task, $data);

      $payments = [];
      foreach ($task->payments as $payment) {
        $payments[] = $this->paymentPresenter->short($payment);
      }

      $is_closed = false;
      $msg = 'Платежи сохранены.';
      if ($task->state === State::Closed) {
        $msg .= ' Задача закрыта.';
        $is_closed = true;
      }
      $vars = [
        'is_closed' => $is_closed,
        'payments' => $payments,
      ];
      $response = new Success($msg, $vars);
    }
    catch (Throwable $e) {
      $response = new Error($e->getMessage());
    }

    return response()->json($response);
  }
}
