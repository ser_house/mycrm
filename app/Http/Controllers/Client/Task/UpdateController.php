<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.04.2020
 * Time: 19:06
 */

namespace App\Http\Controllers\Client\Task;

use App\Http\Controllers\Controller;
use App\Model\Client\Client;
use App\Model\Task\Task;
use Application\Account\DataProvider as AccountDataProvider;
use Application\Account\View\Presenter as AccountPresenter;
use Application\Client\DataProvider as ClientDataProvider;
use Application\Infrastructure\Formatter\Exception as ExceptionFormatter;
use Application\Task\Action\Update as TaskUpdateAction;
use Application\Task\View\Presenter as TaskPresenter;
use Application\Task\DataProvider as TaskDataProvider;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Throwable;

/**
 * Class UpdateController
 *
 * @package App\Http\Controllers\Client\Task
 */
class UpdateController extends Controller {
  public function __construct(
    private readonly TaskUpdateAction $taskUpdateAction,
    private readonly ExceptionFormatter $exceptionFormatter,
    private readonly AccountDataProvider $accountDataProvider,
    private readonly AccountPresenter $accountPresenter,
    private readonly ClientDataProvider $clientDataProvider,
    private readonly TaskPresenter $taskPresenter,
    private readonly TaskDataProvider $taskDataProvider
  ) {
  }


  /**
   * @param Client $client
   * @param Task $task
   *
   * @return Factory|Application|View
   */
  public function form(Client $client, Task $task): Application|Factory|View {
    $user_id = Auth::user()->id;

    $task = $this->taskDataProvider->getTaskWithClient($task->id);
    $sites = $this->clientDataProvider->getClientSites($client->id);

    $payments = $this->taskPresenter->taskPaymentsForEdit($task);
    $sub_tasks = $this->taskPresenter->taskSubTasksForEdit($task);

    $accounts = $this->accountDataProvider->getTypedActiveAccounts($user_id);
    $account_views = $this->accountPresenter->presentGroupedAccounts($accounts);
    $last_account_id = $this->accountDataProvider->getClientLastPaymentAccount($client->id);

    $view_data = [
      'task' => $task,
      'sub_tasks' => $sub_tasks,
      'payments' => $payments,
      'sites' => $sites,
      'accounts' => $account_views,
      'last_account_id' => $last_account_id ?? null,
    ];

    return view('task.form.edit', $view_data);
  }

  /**
   * @param Request $request
   * @param Client $client
   * @param Task $task
   *
   * @return RedirectResponse
   */
  public function update(Request $request, Client $client, Task $task): RedirectResponse {
    try {
      $data = $request->all();
      $this->taskUpdateAction->run($task->id, $data);

      $params = ['client' => $client->id, 'task' => $task->id];

      return redirect()->route('client.task.show', $params)->with('success', 'Задача сохранена.');
    }
    catch (Throwable $e) {
      $output = $this->exceptionFormatter->format($e);

      return redirect()->back()->with('error', $output)->withInput();
    }
  }

  /**
   * @param Request $request
   * @param Client $client
   * @param Task $task
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function updateDates(Request $request, Client $client, Task $task) {
    $task->launched_at = $request->post('launched_at');
    $task->completed_at = $request->post('completed_at');
    $task->save();

    $response = [
      'status' => 'success',
    ];
    return response()->json($response);
  }
}
