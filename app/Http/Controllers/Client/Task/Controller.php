<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.04.2020
 * Time: 19:06
 */

namespace App\Http\Controllers\Client\Task;

use App\Http\Controllers\Controller as BaseController;
use App\Model\Client\Client;
use App\Model\Task\Task;
use Application\Infrastructure\Response\Status\Error;
use Application\Infrastructure\Response\Status\Success;
use Application\Task\DataProvider as TaskDataProvider;
use Application\Task\View\Presenter as TaskPresenter;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;
use Throwable;

/**
 * Class Controller
 *
 * @package App\Http\Controllers\Client\Task
 */
class Controller extends BaseController {
  public function __construct(
    private readonly TaskDataProvider $taskDataProvider,
    private readonly TaskPresenter $taskPresenter
  ) {

  }


  /**
   * @param Client $client
   *
   * @return Factory|Application|View
   */
  public function index(Client $client): Application|Factory|View {
    $tasks = $this->taskDataProvider->getClientTasks($client->id);
    $vars = [
      'tasks' => $this->taskPresenter->shortList($tasks->items()),
      'pager' => $tasks->links(),
    ];
    $vars['client'] = $client;

    return view('client.tasks.index', $vars);
  }

  /**
   * @param Client $client
   * @param Task $task
   *
   * @return Factory|Application|View
   * @throws \Exception
   */
  public function show(Client $client, Task $task): Application|Factory|View {
    $task = $this->taskDataProvider->getTaskWithClient($task->id);
    $taskView = $this->taskPresenter->full($task);

    $status_history = $this->taskDataProvider->getTaskStatusHistory($task->id);
    $status_history_views = $this->taskPresenter->statusHistory($status_history);

    $vars = [
      'taskView' => $taskView,
      'status_history' => $status_history_views,
    ];

    return view('task.pages.show', $vars);
  }

  /**
   * @param Client $client
   * @param Task $task
   *
   * @return JsonResponse
   */
  public function view(Client $client, Task $task): JsonResponse {
    try {
      $task = $this->taskDataProvider->getTaskWithClient($task->id);
      $taskView = $this->taskPresenter->full($task);

      $vars = [
        'title' => $taskView->title,
        'html' => view('task.view', ['taskView' => $taskView])->render(),
      ];
      $response = new Success('', $vars);
    }
    catch (Throwable $e) {
      $response = new Error($e->getMessage());
    }

    return response()->json($response);
  }
}
