<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.04.2020
 * Time: 19:06
 */

namespace App\Http\Controllers\Client\Task;

use App\Http\Controllers\Controller;
use App\Model\Client\Client;
use App\Model\Task\Task;
use Application\Infrastructure\Response\Status\Error;
use Application\Infrastructure\Response\Status\Success;
use DateTimeImmutable;
use Illuminate\Http\JsonResponse;
use Throwable;
use Application\Task\Action\Finish as TaskFinishAction;

/**
 * Class FinishController
 *
 * @package App\Http\Controllers\Client\Task
 */
class FinishController extends Controller {

  public function __construct(private readonly TaskFinishAction $taskFinishAction) {

  }

  /**
   * @param Client $client
   * @param int $task_id
   *
   * @return JsonResponse
   */
  public function run(Client $client, int $task_id): JsonResponse {
    try {
      $task = Task::findOrFail($task_id);
      $task = $this->taskFinishAction->run($task);

      $doc_generator_link = route('client.documents.generator', ['client' => $task->client_id]);
      $completedAt = new DateTimeImmutable($task->completed_at);
      $vars = [
        'state' => $task->state->label(),
        'msg' => "Задача завершена.<br/>Теперь у Вас есть задачи для <a href=$doc_generator_link>генерации документов</a>.",
        'completed_at' => $completedAt->format('d.m.Y'),
      ];
      $response = new Success('', $vars);
    }
    catch (Throwable $e) {
      $response = new Error($e->getMessage());
    }

    return response()->json($response);
  }
}
