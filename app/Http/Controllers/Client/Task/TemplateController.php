<?php

namespace App\Http\Controllers\Client\Task;


use Application\Infrastructure\Response\Status\Error;
use Application\Infrastructure\Response\Status\Success;
use App\Model\Client\Client;
use Auth;
use App\Http\Controllers\Controller;
use Application\Placeholder\Help as PlaceholderHelp;
use App\Http\Requests\Task\StoreTaskTemplateRequest;
use Application\Task\Service as TaskService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;
use Throwable;


/**
 * Class TemplateController
 *
 * @package App\Http\Controllers\Client\Task
 */
class TemplateController extends Controller {

  public function __construct(
    private readonly TaskService $taskService,
    private readonly PlaceholderHelp $placeholderHelp
  ) {
  }


  /**
   * @param Client $client
   *
   * @return Factory|Application|View
   */
  public function index(Client $client): Application|Factory|View {
    $templates = $client->taskTemplates()->get();
    $placeholders = $this->placeholderHelp->help();

    return view('client.tasks.templates', [
      'client' => $client,
      'placeholders' => $placeholders,
      'templates' => $templates,
    ]);
  }

  /**
   * @param StoreTaskTemplateRequest $request
   * @param Client $client
   *
   * @return JsonResponse
   */
  public function add(StoreTaskTemplateRequest $request, Client $client): JsonResponse {

    $inputs = $request->all();

    try {
      $template = $this->taskService->addTaskTemplate(Auth::user()->id, $client->id, $inputs);

      $vars = [
        'template' => $template,
      ];
      $response = new Success('Шаблон добавлен.', $vars);
    }
    catch (Throwable $e) {
      $response = new Error($e->getMessage());
    }

    return response()->json($response);
  }

  /**
   * @param StoreTaskTemplateRequest $request
   * @param Client $client
   * @param int $template_id
   *
   * @return JsonResponse
   */
  public function update(StoreTaskTemplateRequest $request, Client $client, int $template_id): JsonResponse {

    $inputs = $request->all();

    try {
      $template = $this->taskService->updateTaskTemplate($template_id, $inputs);

      $vars = [
        'template' => $template,
      ];
      $response = new Success('Шаблон обновлен.', $vars);
    }
    catch (Throwable $e) {
      $response = new Error($e->getMessage());
    }

    return response()->json($response);
  }

  /**
   * @param Client $client
   * @param int $template_id
   *
   * @return JsonResponse
   */
  public function remove(Client $client, int $template_id): JsonResponse {

    try {
      $this->taskService->removeTaskTemplate($template_id);
      $response = new Success('Шаблон удален');
    }
    catch (Throwable $e) {
      $response = new Error($e->getMessage());
    }

    return response()->json($response);
  }
}
