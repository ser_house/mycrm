<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.04.2020
 * Time: 19:06
 */

namespace App\Http\Controllers\Client\Task;

use App\Http\Controllers\Controller;
use App\Model\Client\Client;
use Application\Infrastructure\Formatter\Exception as ExceptionFormatter;
use Application\Task\Action\Remove as TaskRemoveAction;
use Illuminate\Http\RedirectResponse;
use Throwable;

/**
 * Class RemoveController
 *
 * @package App\Http\Controllers\Client\Task
 */
class RemoveController extends Controller {

  public function __construct(
    private readonly TaskRemoveAction $taskRemoveAction,
    private readonly ExceptionFormatter $exceptionFormatter) {

  }

  /**
   * @param Client $client
   * @param $task_id
   *
   * @return RedirectResponse
   */
  public function __invoke(Client $client, $task_id): RedirectResponse {
    try {
      $this->taskRemoveAction->run($task_id);
      return redirect()->route('tasks')->with('success', 'Задача удалена.');
    }
    catch (Throwable $e) {
      $output = $this->exceptionFormatter->format($e);

      return redirect()->back()->with('error', $output)->withInput();
    }
  }
}
