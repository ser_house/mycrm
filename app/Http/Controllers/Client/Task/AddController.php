<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.04.2020
 * Time: 19:06
 */

namespace App\Http\Controllers\Client\Task;

use App\Http\Controllers\Controller;
use App\Http\Requests\Task\StoreTaskRequest;
use App\Model\Client\Client;
use App\Model\Task\TaskTemplate;
use Application\Account\DataProvider as AccountDataProvider;
use Application\Account\View\Presenter as AccountPresenter;
use Application\Client\DataProvider as ClientDataProvider;
use Application\Infrastructure\Formatter\Exception as ExceptionFormatter;
use Application\Infrastructure\Response\Status\Error;
use Application\Infrastructure\Response\Status\Success;
use Application\Task\Action\Add as TaskAddAction;
use Application\Task\State;
use Application\Task\View\Presenter as TaskPresenter;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Throwable;

/**
 * Class AddController
 *
 * @package App\Http\Controllers\Client\Task
 */
class AddController extends Controller {

  public function __construct(
    private readonly TaskAddAction $taskAddAction,
    private readonly ExceptionFormatter $exceptionFormatter,
    private readonly AccountDataProvider $accountDataProvider,
    private readonly AccountPresenter $accountPresenter,
    private readonly ClientDataProvider $clientDataProvider,

  ) {

  }

  /**
   * @param Client $client
   *
   * @return Factory|Application|View
   */
  public function form(Client $client): Application|Factory|View {
    $user_id = Auth::user()->id;

    $task = $this->taskAddAction->buildCreateTaskData($user_id, $client->id);
    $sub_task = $this->taskAddAction->buildNewSubtask();
    $sites = $this->clientDataProvider->getClientSites($client->id);

    $task_templates = $this->clientDataProvider->getClientTaskTemplates($client->id, TaskTemplate::STATE_ACTIVE);
    $accounts = $this->accountDataProvider->getTypedActiveAccounts($user_id);
    $account_views = $this->accountPresenter->presentGroupedAccounts($accounts);
    $last_account_id = $this->accountDataProvider->getClientLastPaymentAccount($client->id);

    $view_data = [
      'task' => $task,
      'sub_tasks' => [$sub_task],
      'sites' => $sites,
      'task_templates' => $task_templates ?? [],
      'accounts' => $account_views,
      'last_account_id' => $last_account_id ?? null,
    ];

    return view('task.form.create', $view_data);
  }

  /**
   * @param StoreTaskRequest $request
   * @param Client $client
   *
   * @return RedirectResponse|Redirector
   */
  public function add(StoreTaskRequest $request, Client $client): Redirector|RedirectResponse {
    $user_id = Auth::user()->id;
    $data = $request->all();

    try {
      $task = $this->taskAddAction->run($data, $user_id, $client->id);

      return redirect()->route('client.task.show', ['client' => $client->id, 'task' => $task->id])->with('success', 'Задача сохранена.');
    }
    catch (Throwable $e) {
      $output = $this->exceptionFormatter->format($e);

      return redirect()->back()->with('error', $output)->withInput();
    }
  }

  /**
   * @param Client $client
   * @param int $template_id
   *
   * @return JsonResponse
   */
  public function createByTemplate(Client $client, int $template_id): JsonResponse {
    try {
      $task = $this->taskAddAction->createTaskByTemplateId($template_id);
      $vars = [
        'task' => $task,
      ];
      $response = new Success('', $vars);
    }
    catch (Throwable $e) {
      $response = new Error($e->getMessage());
    }

    return response()->json($response);
  }
}
