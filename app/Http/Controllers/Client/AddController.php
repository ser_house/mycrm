<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Client\AddClientRequest;
use Application\Client\Action\Add as ClientAddAction;
use Application\Infrastructure\Formatter\Exception as ExceptionFormatter;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Throwable;

/**
 * Class ClientAddController
 *
 * @package App\Http\Controllers\Client
 */
class AddController extends Controller {
  public function __construct(
    private readonly ClientAddAction $clientAddAction,
    private readonly ExceptionFormatter $exceptionFormatter
  ) {
  }

  /**
   *
   * @return Factory|Application|View
   */
  public function form(): Application|Factory|View {
    return view('client.form.create');
  }

  /**
   * @param AddClientRequest $request
   *
   * @return RedirectResponse|Redirector
   */
  public function add(AddClientRequest $request): Redirector|RedirectResponse {
    $user_id = Auth::user()->id;

    try {
      $inputs = $request->all();

      $client = $this->clientAddAction->run($inputs, $user_id);

      $save = $request->get('save');

      $route = ('save_and_add' === $save) ? 'client.create' : 'client.show';

      return redirect()
        ->route($route, ['client' => $client->id])
        ->with('success', "Клиент \"$client->name\" добавлен.")
      ;
    }
    catch (Throwable $e) {
      $output = $this->exceptionFormatter->format($e);

      return redirect()->back()->with('error', $output)->withInput();
    }
  }
}
