<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Client\UpdateClientRequest;
use App\Model\Client\Client;
use Application\Client\Action\Update as ClientUpdateAction;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class UpdateController
 *
 * @package App\Http\Controllers\Client
 */
class UpdateController extends Controller {
  public function __construct(private readonly ClientUpdateAction $clientUpdateAction) {

  }

  /**
   * @param Client $client
   *
   * @return Factory|Application|View
   */
  public function form(Client $client): Factory|Application|View {
    return view('client.form.edit', [
      'client' => $client,
    ]);
  }

  /**
   * @param UpdateClientRequest $request
   * @param Client $client
   *
   * @return RedirectResponse
   */
  public function update(UpdateClientRequest $request, Client $client): RedirectResponse {
    $inputs = $request->all();
    $user_id = Auth::user()->id;
    $this->clientUpdateAction->run($inputs, $user_id, $client->id);

    return redirect()
      ->route('client.show', ['client' => $client->id])
      ->with('success', 'Клиент обновлен.')
    ;
  }
}
