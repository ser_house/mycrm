<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.04.2020
 * Time: 18:54
 */


namespace App\Http\Controllers\Client\Document;


use App\Http\Controllers\Controller;
use App\Model\Client\Client;
use App\Model\Document;
use Application\Client\Document\Template\DocumentTemplateService;
use Application\Client\Document\Template\TemplateOptions;
use Application\DocGenerator\Generator\Vars\Settings\SelectedBuilder;
use Application\DocGenerator\Generator\Vars\Settings\Presenter as VarsSettingsPresenter;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Infrastructure\Response\Status\Error;
use Application\Infrastructure\Response\Status\Success;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Throwable;

/**
 * Class TemplateController
 *
 * @package App\Http\Controllers\Client\Document
 */
class TemplateController extends Controller {
  public function __construct(
    private readonly DocumentTemplateService $documentTemplateService,
    private readonly DateTimeFormatter $dateTimeFormatter,
    private readonly VarsSettingsPresenter $varSettingsPresenter,
    private readonly SelectedBuilder $varSettingsSelectedBuilder
  ) {

  }

  /**
   * @param Client $client
   *
   * @return Factory|Application|View
   */
  public function index(Client $client): Factory|Application|View {
    $template_rules = $this->documentTemplateService->rules();
    $templates = $this->documentTemplateService->getClientDocumentTemplates($client->id);

    $date_formats = $this->dateTimeFormatter->getDateFormats();
    // @todo: template options пока есть и у клиента и у шаблона.
    // У клиента \App\Http\Controllers\Client\DocumentController::settings
    // Надо, чтобы клиентские были умолчальными для новых шаблонов.
    $templateOptions = new TemplateOptions([], $date_formats);
    $options_definitions = $templateOptions->definitions();

    $doc_types = Document::types();

    $settings = $client->getSettings($date_formats);
    $variables = $this->varSettingsPresenter->presentAllLevelsVariables($settings, $this->varSettingsSelectedBuilder);

    return view('client.documents.templates', [
      'client' => $client,
      'templates' => $templates,
      'template_rules' => $template_rules,
      'doc_types' => $doc_types,
      'variables' => $variables,
      'options_definitions' => $options_definitions,
      'date_formats' => $templateOptions->getDateFormats(),
    ]);
  }

  /**
   * @param Request $request
   * @param Client $client
   *
   * @return JsonResponse
   */
  public function upload(Request $request, Client $client): JsonResponse {
    $user_id = Auth::user()->id;
    $uploaded_files = $request->file("templates");
    $all = $request->all();
    $data_input = $request->get("templates");
    $response_files = $this->documentTemplateService->upload(
      $uploaded_files,
      $all,
      $data_input,
      $user_id,
      $client->id);

    return response()->json(['files' => $response_files]);
  }

  /**
   * @param Request $request
   * @param Client $client
   *
   * @return JsonResponse
   */
  public function update(Request $request, Client $client): JsonResponse {
    $inputs = $request->get('templates');
    $id = $request->get('id');

    try {
      $inputs = reset($inputs);
      $this->documentTemplateService->update($id, $inputs);
      $response = new Success('Шаблон сохранен.');
    }
    catch (Throwable $e) {
      $response = new Error($e->getMessage());
    }

    return response()->json($response);
  }
}
