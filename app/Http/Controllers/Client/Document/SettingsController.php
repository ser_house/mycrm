<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.04.2020
 * Time: 18:59
 */


namespace App\Http\Controllers\Client\Document;


use App\Http\Controllers\Controller;
use App\Model\Client\Client;
use Application\DocGenerator\Generator\Vars\Settings\Presenter as VarsSettingsPresenter;
use Application\Infrastructure\Formatter\DateTime as DateTimeFormatter;
use Application\Infrastructure\Formatter\Exception as ExceptionFormatter;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Throwable;

/**
 * Class SettingsController
 *
 * @package App\Http\Controllers\Client\Document
 */
class SettingsController extends Controller {
  public function __construct(
    private readonly ExceptionFormatter $exceptionFormatter,
    private readonly DateTimeFormatter $dateTimeFormatter,
    private readonly VarsSettingsPresenter $varsSettingsPresenter
  ) {
  }

  /**
   * @param Client $client
   *
   * @return Factory|Application|View
   */
  public function settings(Client $client): Application|Factory|View {
    $settings = $client->getSettings($this->dateTimeFormatter->getDateFormats());
    $data = $settings->definitions($this->varsSettingsPresenter);

    return view('client.documents.settings', [
      'client' => $client,
      'data' => $data,
    ]);
  }

  /**
   * @param Request $request
   * @param Client $client
   *
   * @return RedirectResponse
   */
  public function settingsSave(Request $request, Client $client): RedirectResponse {

    try {
      $settings = $request->all();
      unset($settings['_token']);
      $client->settings = serialize($settings);
      $client->save();

      return redirect()
        ->route('client.documents.settings', ['client' => $client->id])
        ->with('success', 'Настройки сохранены.')
      ;
    }
    catch (Throwable $e) {
      $output = $this->exceptionFormatter->format($e);

      return redirect()->back()->with('error', $output)->withInput();
    }
  }
}
