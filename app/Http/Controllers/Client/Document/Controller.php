<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.04.2020
 * Time: 18:59
 */


namespace App\Http\Controllers\Client\Document;


use App\Http\Controllers\Controller as BaseController;
use App\Model\Client\Client;
use App\Model\Document;
use Application\Client\Document\ClientDocumentService;
use Application\Infrastructure\Response\Status\Error;
use Application\Infrastructure\Response\Status\Success;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Throwable;
use Application\Client\Document\DataProvider;

/**
 * Class Controller
 *
 * @package App\Http\Controllers\Client\Document
 */
class Controller extends BaseController {

  public function __construct(
    private readonly ClientDocumentService $clientDocumentService,
    private readonly DataProvider $dataProvider
  ) {

  }

  /**
   * @param Client $client
   *
   * @return Factory|Application|View
   */
  public function index(Client $client): Application|Factory|View {
    $document_rules = $this->dataProvider->rules();

    $doc_types = Document::types();

    $documents = $this->dataProvider->getClientDocuments($client->id);

    return view('client.documents.index', [
      'client' => $client,
      'documents' => $documents,
      'document_rules' => $document_rules,
      'doc_types' => $doc_types,
    ]);
  }

  /**
   * @param Request $request
   * @param Client $client
   *
   * @return JsonResponse
   */
  public function upload(Request $request, Client $client): JsonResponse {
    $user_id = Auth::user()->id;
    $response_files = $this->clientDocumentService->upload(
      $request->file("documents"),
      $request->all(),
      $request->get("documents"),
      $user_id,
      $client->id);

    return response()->json(['files' => $response_files]);
  }

  /**
   * @param Request $request
   * @param Client $client
   *
   * @return JsonResponse
   */
  public function update(Request $request, Client $client): JsonResponse {
    $inputs = $request->get('documents');
    $id = $request->get('id');

    try {
      $inputs = reset($inputs);
      $this->clientDocumentService->update($id, $inputs);
      $response = new Success('Документ сохранен.');
    }
    catch (Throwable $e) {
      $response = new Error($e->getMessage());
    }

    return response()->json($response);
  }
}
