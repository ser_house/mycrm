<?php

namespace App\Http\Controllers;


use Application\Infrastructure\Formatter\Exception as ExceptionFormatter;
use App\Model\User;
use Application\Incoming\Service as IncomingService;
use DB;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Throwable;


/**
 * Class AdminController
 *
 * @package App\Http\Controllers
 */
class AdminController extends Controller {
  public function __construct(
    private readonly IncomingService $incomingService,
    private readonly ExceptionFormatter $exceptionFormatter
  ) {

  }

  /**
   *
   * @return Factory|Application|View
   */
  public function caches(): Application|Factory|View {
    $users = User::all();
    return view('admin.caches', [
      'users' => $users,
    ]);
  }

  /**
   * @param Request $request
   *
   * @return RedirectResponse|Redirector
   */
  public function resetCaches(Request $request): Redirector|RedirectResponse {
    $inputs = $request->all();

    try {
      if (empty($inputs['user_id'])) {
        $users = User::all();
        $user_ids = [];
        foreach ($users as $user) {
          $user_ids[] = $user->id;
        }
      }
      else {
        $user_ids = [$inputs['user_id']];
      }

      foreach ($user_ids as $user_id) {
        if (!empty($inputs['mode']['by_years'])) {
          // Сбрасываем кэш статистики по годам,
          $this->incomingService->resetUserCache($user_id);
        }

        if (!empty($inputs['mode']['by_current_year_months'])) {
          $this->incomingService->resetUserYearCache($user_id, (int)date('Y'));
        }
      }

      return redirect()->route('admin.caches')->with('success', 'Кэши сброшены.');
    }
    catch (Throwable $e) {
      $output = $this->exceptionFormatter->format($e);
      return redirect()->back()->with('error', $output)->withInput();
    }
  }

  /**
   *
   * @return Factory|Application|View
   */
  public function styles(): Application|Factory|View {
    return view('admin.styles.base');
  }

  /**
   *
   * @return Factory|Application|View
   */
  public function stylesForm(): Application|Factory|View {
    return view('admin.styles.form');
  }

  /**
   *
   * @return Factory|Application|View
   */
  public function stylesColor(): Application|Factory|View {
    return view('admin.styles.color');
  }

  /**
   *
   * @return Factory|Application|View
   */
  public function stylesIcons(): Factory|Application|View {
    $icon_classes = $this->getIconsClasses();
    return view('admin.styles.icons', [
      'icon_classes' => $icon_classes,
    ]);
  }

  private function getIconsClasses(): array {
    $path = resource_path() . '/fonts/icons/variables.scss';

    $content = file_get_contents($path);
    $classes = [];
    preg_match_all('/\$(icon-.*):/', $content, $classes);

    return $classes[1];
  }
}
