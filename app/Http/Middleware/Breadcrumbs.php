<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.07.2020
 * Time: 18:12
 */


namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class Breadcrumbs {
  /**
   * @param Request $request
   * @param Closure $next
   *
   * @return mixed
   */
  public function handle(Request $request, Closure $next) {
    $current = Route::current();
    $route_segments = explode('/', $current->uri());

    $breadcrumbs = [
      [
        'href' => '/',
        'title' => 'Главная',
      ],
    ];
    $href = '';

    $request_segments = $request->segments();

    foreach ($request_segments as $i => $request_segment) {
      $href .= '/' . $request_segment;
      $title = config("breadcrumbs.segment_titles.$request_segment", $request_segment);

      if (0 === strpos($route_segments[$i], '{')) {
        $param_name = trim($route_segments[$i], '{}');
        $paramObj = $current->parameter($param_name);

        if (null !== $paramObj) {
          $label_key = config("breadcrumbs.param_obj_label_keys.$param_name", $param_name);
          if (isset($paramObj->{$label_key})) {
            $title = $paramObj->{$label_key};
          }
        }
      }

      $breadcrumbs[] = [
        'href' => $href,
        'title' => $title,
      ];
    }

    if (!config('breadcrumbs.last_segment.show', false)) {
      array_pop($breadcrumbs);
    }
    else if (!config('breadcrumbs.last_segment.as_link', false)) {
      $last_index = count($breadcrumbs) - 1;
      $breadcrumbs[$last_index]['href'] = '';
    }

    if (count($breadcrumbs) > 1) {
      $request->attributes->add(['breadcrumbs' => $breadcrumbs]);
    }

    return $next($request);
  }
}
