<?php

namespace App\Http\Requests\Task;


use App\Rules\Money;
use Illuminate\Foundation\Http\FormRequest;

class StoreTaskTemplateRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(): bool {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(): array {
		return [
			'title' => 'required|max:255',
			'description' => 'sometimes',
			'currency' => 'required|string|max:3',
			'budget' => ['required', new Money()],
		];
	}
}
