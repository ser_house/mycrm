<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(): bool {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(): array {
		return [
			'title' => 'required|max:255',
			'description' => 'sometimes',
			'launched_at' => 'sometimes',
			'completed_at' => 'sometimes',
			'client_id' => 'numeric',
			'client_site_id' => 'sometimes',
			'currency' => 'string|max:3',
			'sub_tasks' => 'required|array',
			'sub_tasks.*' => 'required',
		];
	}
}
