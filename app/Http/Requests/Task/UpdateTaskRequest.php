<?php

namespace App\Http\Requests\Task;


class UpdateTaskRequest extends TaskRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(): bool {
		return false;
	}
}
