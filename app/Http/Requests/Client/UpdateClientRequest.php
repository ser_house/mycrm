<?php

namespace App\Http\Requests\Client;

use Illuminate\Validation\Rule;

class UpdateClientRequest extends ClientRequest {

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules(): array {
    $client_id = $this->route('client')->id;
    $rules = parent::rules();
    $rules['name'] = [
      'required',
      Rule::unique('client')->ignore($client_id),
      'max:255',
    ];

    return $rules;
  }
}
