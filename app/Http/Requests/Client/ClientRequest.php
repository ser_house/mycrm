<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.01.2018
 * Time: 17:40
 */

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(): array {
		return [
			'name' => 'required|unique:client|max:255',
			'note' => 'sometimes|max:4096',
			'is_selectable' => 'sometimes|boolean',
		];
	}
}
