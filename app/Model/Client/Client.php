<?php

namespace App\Model\Client;

use App\Model\Document;
use App\Model\DocumentTemplate;
use App\Model\Task\Task;
use App\Model\Task\TaskTemplate;
use DB;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


/**
 * App\Model\Client\Client
 *
 * @property-read Collection|ClientContact[] $contacts
 * @property-read Collection|\App\Model\DocumentTemplate[] $documentTemplates
 * @property-read Collection|\App\Model\Document[] $documents
 * @property string $city
 * @property-read Collection|ClientSite[] $sites
 * @mixin Eloquent
 * @property int $id
 * @property string $created_at
 * @property int $user_id
 * @property string $name
 * @property string|null $note
 * @property bool $is_selectable
 * @property mixed $attributes
 * @method static Builder|Client whereCreatedAt($value)
 * @method static Builder|Client whereId($value)
 * @method static Builder|Client whereIsSelectable($value)
 * @method static Builder|Client whereLocationId($value)
 * @method static Builder|Client whereName($value)
 * @method static Builder|Client whereNote($value)
 * @method static Builder|Client whereUserId($value)
 * @property-read Collection|Task[] $tasks
 * @property-read Collection|TaskTemplate[] $taskTemplates
 * @method static Builder|Client whereCityId($value)
 * @property mixed $settings
 * @method static Builder|Client whereSettings($value)
 * @property-read int|null $contacts_count
 * @property-read int|null $document_templates_count
 * @property-read int|null $documents_count
 * @property-read int|null $sites_count
 * @property-read int|null $task_templates_count
 * @property-read int|null $tasks_count
 * @method static Builder|Client newModelQuery()
 * @method static Builder|Client newQuery()
 * @method static Builder|Client query()
 */
class Client extends Model {
	protected $table = 'client';
	public $timestamps = false;
	protected $fillable = ['user_id', 'name', 'note', 'is_selectable', 'settings'];

	protected $casts = [
		'is_selectable' => 'boolean',
	];

	public const STATE_ACTIVE = 1;
	public const STATE_INACTIVE = 0;

	/**
	 *
	 * @return HasMany
	 */
	public function contacts(): HasMany {
		return $this->hasMany(ClientContact::class);
	}

	/**
	 *
	 * @return HasMany
	 */
	public function sites(): HasMany {
		return $this->hasMany(ClientSite::class);
	}

	/**
	 *
	 * @return HasMany
	 */
	public function documentTemplates(): HasMany {
		return $this->hasMany(DocumentTemplate::class);
	}

	/**
	 *
	 * @return HasMany
	 */
	public function documents(): HasMany {
		return $this->hasMany(Document::class);
	}

	/**
	 *
	 * @return HasMany
	 */
	public function tasks(): HasMany {
		return $this->hasMany(Task::class);
	}

	/**
	 *
	 * @return HasMany
	 */
	public function taskTemplates(): HasMany {
		return $this->hasMany(TaskTemplate::class);
	}

  /**
   * @param array $date_formats
   *
   * @return ClientSettings
   */
	public function getSettings(array $date_formats): ClientSettings {
	    $settings = [];
	    if ($this->settings) {
            $settings = unserialize($this->settings, ['allowed_classes' => false]);
            $settings = $settings ?: [];
        }

		return new ClientSettings($settings, $date_formats);
	}
}
