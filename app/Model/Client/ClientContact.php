<?php

namespace App\Model\Client;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Model\Client\ClientContact
 *
 * @property-read Client $client
 * @mixin Eloquent
 * @property int $id
 * @property int $client_id
 * @property string $name
 * @property string $value
 * @property string|null $note
 * @method static Builder|ClientContact whereClientId($value)
 * @method static Builder|ClientContact whereId($value)
 * @method static Builder|ClientContact whereName($value)
 * @method static Builder|ClientContact whereNote($value)
 * @method static Builder|ClientContact whereValue($value)
 * @method static Builder|ClientContact newModelQuery()
 * @method static Builder|ClientContact newQuery()
 * @method static Builder|ClientContact query()
 */
class ClientContact extends Model {
	protected $table = 'client_contact';
	public $timestamps = false;
	protected $fillable = ['client_id', 'name', 'value', 'note'];

	/**
	 *
	 * @return BelongsTo
	 */
	public function client(): BelongsTo {
		return $this->belongsTo(Client::class);
	}
}
