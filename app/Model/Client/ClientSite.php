<?php

namespace App\Model\Client;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Model\Client\ClientSite
 *
 * @property-read Client $client
 * @mixin Eloquent
 * @property int $id
 * @property int $client_id
 * @property string $url
 * @method static Builder|ClientSite whereClientId($value)
 * @method static Builder|ClientSite whereId($value)
 * @method static Builder|ClientSite whereUrl($value)
 * @method static Builder|ClientSite newModelQuery()
 * @method static Builder|ClientSite newQuery()
 * @method static Builder|ClientSite query()
 */
class ClientSite extends Model {
	protected $table = 'client_site';

	public $timestamps = false;
	protected $fillable = ['client_id', 'url'];

	/**
	 *
	 * @return BelongsTo
	 */
	public function client(): BelongsTo {
		return $this->belongsTo(Client::class);
	}
}
