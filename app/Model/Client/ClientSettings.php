<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.04.2018
 * Time: 13:57
 */

namespace App\Model\Client;

use Application\DocGenerator\Generator\Vars\Settings\Presenter as VarsSettingsPresenter;
use Application\DocGenerator\Generator\Vars\VarsRegistry;
use Application\DocGenerator\Generator\Vars\Settings\Builder;
use JsonSerializable;

class ClientSettings implements JsonSerializable {

	private $settings = [];
  private array $date_formats;
  private VarsRegistry $varsRegistry;

  /**
   * ClientSettings constructor.
   *
   * @param array $settings
   * @param array $date_formats
   */
	public function __construct(array $settings, array $date_formats) {
    $this->date_formats = $date_formats;

		$default_settings = [
			'append_hours_suffix' => false,
			'human_amount_upfirst' => false,
			'human_amount_append_units_suffix' => false,
			'date_format' => key($this->date_formats),
			'document_file_name_pattern' => '',
		];

		$this->varsRegistry = new VarsRegistry();

		$all_variables = $this->varsRegistry->allVariablesWithDefaultValues();

		$this->settings = array_merge($default_settings, $all_variables);

		foreach ($this->settings as $key => $default_value) {
			if (isset($settings[$key])) {
				$this->settings[$key] = $settings[$key];
			}
		}
	}

	/**
	 *
	 * @return mixed
	 */
	public function jsonSerialize() {
		return $this->settings;
	}

  /**
   * @return VarsRegistry
   */
  public function getVarsRegistry(): VarsRegistry {
    return $this->varsRegistry;
  }

	/**
	 * @param $key
	 *
	 * @return mixed
	 */
	public function get($key) {
		return $this->settings[$key] ?? null;
	}

  /**
   * @param VarsSettingsPresenter $presenter
   *
   * @return array
   */
	public function definitions(VarsSettingsPresenter $presenter): array {
		$template_options = [
			'append_hours_suffix' => [
				'title' => 'выводить ли "час", "часа" и проч. в строке услуги',
				'default_value' => $this->get('append_hours_suffix'),
			],
			'human_amount_upfirst' => [
				'title' => 'делать ли строчной первую букву суммы прописью',
				'default_value' => $this->get('human_amount_upfirst'),
			],
			'human_amount_append_units_suffix' => [
				'title' => 'выводить ли "рубль", "рублей" и проч. в сумме прописью',
				'default_value' => $this->get('human_amount_append_units_suffix'),
			],
			'date_format' => [
				'title' => 'Формат даты',
				'options' => $this->date_formats,
				'default_value' => $this->get('date_format'),
			],
			'document_file_name_pattern' => [
				'title' => 'Шаблон названия файла документа',
				'type' => 'string',
				'default_value' => $this->get('document_file_name_pattern'),
			],
		];

		$varSettingsBuilder = new Builder($presenter->getKeyFormatter());
		return [
			'template_options' => $template_options,
			'variables' => $presenter->presentAllLevelsVariables($this, $varSettingsBuilder),
		];
	}
}
