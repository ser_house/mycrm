<?php

namespace App\Model;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Model\AccountType
 *
 * @property int $id
 * @property string $name
 * @property-read Collection|Account[] $accounts
 * @method static Builder|AccountType whereId($value)
 * @method static Builder|AccountType whereName($value)
 * @mixin Eloquent
 * @property-read int|null $accounts_count
 * @method static Builder|AccountType newModelQuery()
 * @method static Builder|AccountType newQuery()
 * @method static Builder|AccountType query()
 */
class AccountType extends Model {
	protected $table = 'account_type';
	public $timestamps = false;
	protected $fillable = ['name'];

	public function accounts(): HasMany {
		return $this->hasMany(Account::class, 'id', 'type_id');
	}
}
