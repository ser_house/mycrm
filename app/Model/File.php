<?php

namespace App\Model;

use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\File
 *
 * @mixin Eloquent
 * @property int $id
 * @property int $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $path Внутренний, файловый, путь к файлу.
 * @property string $name Название файла вместе с расширением.
 * @property int $size Размер файла в байтах.
 * @property string $mime
 * @property string|null $uri uri, по которому файл может быть доступен публично. Может и не быть, если, например, файл не должен быть доступен публично. Это не то же самое, что дает asset для сохраненного файла.
 * @method static Builder|File whereCreatedAt($value)
 * @method static Builder|File whereId($value)
 * @method static Builder|File whereMime($value)
 * @method static Builder|File whereName($value)
 * @method static Builder|File wherePath($value)
 * @method static Builder|File whereSize($value)
 * @method static Builder|File whereUpdatedAt($value)
 * @method static Builder|File whereUri($value)
 * @method static Builder|File whereUserId($value)
 * @property string $orig_name
 * @method static Builder|File whereOrigName($value)
 * @method static Builder|File newModelQuery()
 * @method static Builder|File newQuery()
 * @method static Builder|File query()
 */
class File extends Model {
	protected $table = 'file';
	protected $fillable = ['user_id', 'created_at', 'updated_at', 'path', 'name', 'size', 'mime', 'uri'];
}
