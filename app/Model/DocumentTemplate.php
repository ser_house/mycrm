<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.01.2018
 * Time: 15:00
 */

namespace App\Model;

use App\Model\Client\Client;
use Application\Client\Document\Template\TemplateOptions;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Model\DocumentTemplate
 *
 * @property-read \App\Model\File $file
 * @mixin Eloquent
 * @property int $id
 * @property int $client_id
 * @property int $type_id
 * @property int $file_id
 * @property string|null $note
 * @method static Builder|DocumentTemplate whereClientId($value)
 * @method static Builder|DocumentTemplate whereFileId($value)
 * @method static Builder|DocumentTemplate whereId($value)
 * @method static Builder|DocumentTemplate whereNote($value)
 * @method static Builder|DocumentTemplate whereTypeId($value)
 * @property mixed|null $options
 * @property int $weight
 * @property-read Client $client
 * @method static Builder|DocumentTemplate whereOptions($value)
 * @method static Builder|DocumentTemplate whereWeight($value)
 * @method static Builder|DocumentTemplate newModelQuery()
 * @method static Builder|DocumentTemplate newQuery()
 * @method static Builder|DocumentTemplate query()
 */
class DocumentTemplate extends Model {
  public $timestamps = false;
  protected $table = 'document_template';
  protected $fillable = ['client_id', 'type_id', 'file_id', 'note', 'options', 'weight'];

  public function file(): BelongsTo {
    return $this->belongsTo(File::class);
  }

  public function client(): BelongsTo {
    return $this->belongsTo(Client::class);
  }

  public function getOptions(array $date_formats): TemplateOptions {
    $options = $this->options ? json_decode($this->options, true) : [];
    return new TemplateOptions($options, $date_formats);
  }
}
