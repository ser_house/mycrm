<?php

namespace App\Model;

use App\Model\Task\Task;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * App\Model\Payment
 *
 * @property-read Task $task
 * @mixin Eloquent
 * @property int $id
 * @property int $task_id
 * @property string $created_at
 * @property int $account_id
 * @property string $note
 * @property float $amount
 * @property float $rate
 * @method static Builder|Payment whereAccountId($value)
 * @method static Builder|Payment whereAmount($value)
 * @method static Builder|Payment whereCreatedAt($value)
 * @method static Builder|Payment whereId($value)
 * @method static Builder|Payment whereNote($value)
 * @method static Builder|Payment whereRate($value)
 * @method static Builder|Payment whereTaskId($value)
 * @method static Builder|Payment withAccount()
 * @property-read Account $account
 * @method static Builder|Payment user($user_id)
 * @method static Builder|Payment newModelQuery()
 * @method static Builder|Payment newQuery()
 * @method static Builder|Payment query()
 */
class Payment extends Model {
	protected $table = 'payment';

	public $timestamps = false;

	protected $fillable = ['task_id', 'created_at', 'account_id', 'note', 'amount', 'rate'];

	public function task(): BelongsTo {
		return $this->belongsTo(Task::class);
	}

	public function account(): BelongsTo {
		return $this->belongsTo(Account::class);
	}

  /**
   * Scope a query to only include user's accounts.
   *
   * @param Builder $query
   * @param int $user_id
   *
   * @return Builder
   */
  public function scopeUser($query, $user_id): Builder {
    return $query->where([['user_id', $user_id]]);
  }

  public function scopeWithAccount(Builder $query): Builder {
    return $query->with('account');
  }
}
