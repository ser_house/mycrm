<?php

namespace App\Model;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Model\Client\Client;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Model\Holiday
 *
 * @property int $user_id
 * @property int $year
 * @property string $date
 * @method static Builder|Holiday whereDate($value)
 * @method static Builder|Holiday whereUserId($value)
 * @method static Builder|Holiday whereYear($value)
 * @mixin Eloquent
 * @property int $client_id
 * @property-read Client $client
 * @method static Builder|Holiday whereClientId($value)
 * @method static Builder|Holiday newModelQuery()
 * @method static Builder|Holiday newQuery()
 * @method static Builder|Holiday query()
 */
class Holiday extends Model {
	protected $table = 'holiday';
	public $timestamps = false;
	protected $fillable = ['client_id', 'year', 'date'];

	/**
	 *
	 * @return BelongsTo
	 */
	public function client(): BelongsTo {
		return $this->belongsTo(Client::class);
	}
}
