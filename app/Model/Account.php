<?php

namespace App\Model;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Model\Account
 *
 * @mixin Eloquent
 * @property int $id
 * @property string $created_at
 * @property int $user_id
 * @property int $type_id
 * @property string $name
 * @property string|null $note
 * @property string $currency
 * @property bool $is_active
 * @method static Builder|Account whereCreatedAt($value)
 * @method static Builder|Account whereCurrency($value)
 * @method static Builder|Account whereId($value)
 * @method static Builder|Account whereIsActive($value)
 * @method static Builder|Account whereName($value)
 * @method static Builder|Account whereNote($value)
 * @method static Builder|Account whereTypeId($value)
 * @method static Builder|Account whereUserId($value)
 * @property-read AccountType $accountType
 * @method static Builder|Account userActive($user_id)
 * @method static Builder|Account user($user_id)
 * @method static Builder|Account newModelQuery()
 * @method static Builder|Account newQuery()
 * @method static Builder|Account query()
 */
class Account extends Model {
	protected $table = 'account';
	public $timestamps = false;
	protected $fillable = ['user_id', 'type_id', 'name', 'note', 'currency', 'is_active'];

	protected $casts = [
		'is_active' => 'boolean',
	];

	public function accountType(): BelongsTo {
		return $this->belongsTo(AccountType::class, 'type_id', 'id');
	}

	/**
	 * Scope a query to only include active accounts.
	 *
	 * @param Builder $query
	 * @param int $user_id
	 *
	 * @return Builder
	 */
	public function scopeUserActive($query, $user_id): Builder {
		return $query->where([['user_id', $user_id], ['is_active', 1]])->with('accountType');
	}

	/**
	 * Scope a query to only include user's accounts.
	 *
	 * @param Builder $query
	 * @param int $user_id
	 *
	 * @return Builder
	 */
	public function scopeUser($query, $user_id): Builder {
		return $query->where([['user_id', $user_id]])->with('accountType');
	}

  /**
   *
   * @return HasMany
   */
  public function payments(): HasMany {
    return $this->hasMany(Payment::class);
  }
}
