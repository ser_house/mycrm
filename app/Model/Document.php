<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.01.2018
 * Time: 11:54
 */

namespace App\Model;

use App\Model\Client\Client;
use Application\Client\Document\DocumentType;
use Carbon\Carbon;
use DB;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Model\Document
 *
 * @property-read \App\Model\File $file
 * @mixin Eloquent
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $client_id
 * @property int $type_id
 * @property int $file_id
 * @property string|null $date
 * @property int|null $number
 * @property string|null $title
 * @method static Builder|Document whereClientId($value)
 * @method static Builder|Document whereCreatedAt($value)
 * @method static Builder|Document whereDate($value)
 * @method static Builder|Document whereFileId($value)
 * @method static Builder|Document whereId($value)
 * @method static Builder|Document whereNumber($value)
 * @method static Builder|Document whereTitle($value)
 * @method static Builder|Document whereTypeId($value)
 * @method static Builder|Document whereUpdatedAt($value)
 * @property int $weight
 * @property int $page_num
 * @property-read Client $client
 * @method static Builder|Document wherePageNum($value)
 * @method static Builder|Document whereWeight($value)
 * @method static Builder|Document newModelQuery()
 * @method static Builder|Document newQuery()
 * @method static Builder|Document query()
 */
class Document extends Model {
	public const UNDEFINED_TYPE = 1;

	protected $table = 'document';
	protected $fillable = ['created_at', 'updated_at', 'client_id', 'type_id', 'file_id', 'date', 'number', 'page_num', 'title', 'weight'];

  private static array $types = [];

	public function file(): BelongsTo {
		return $this->belongsTo(File::class);
	}

	public function client(): BelongsTo {
		return $this->belongsTo(Client::class);
	}

	/**
	 *
	 * @return \Application\Client\Document\DocumentType[] array
	 */
	public static function types(): array {
		if (empty(self::$types)) {
			$doc_type_items = DB::select('SELECT * FROM document_type ORDER BY title');
			foreach($doc_type_items as $doc_type_item) {
				$id = $doc_type_item->id;
				self::$types[$id] = new DocumentType($id, $doc_type_item->machine_name, $doc_type_item->title);
			}
		}

		return self::$types;
	}
}
