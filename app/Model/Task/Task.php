<?php

namespace App\Model\Task;

use App\Model\Client\Client;
use App\Model\Client\ClientSite;
use App\Model\Payment;
use Application\Money\Amount;
use Application\Task\State;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Task.
 *
 * Задача - это в любом случае группирующая сущность.
 * Группирует она подзадачи, которые у отдельной задачи могут быть, а могут и не быть.
 * Если подзадач как таковых нет, то задача всё равно содержит в себе единственную подзадачу,
 * называющуюся "Основная".
 * Задача так же определяет валюту бюджета подзадач.
 *
 * @package App
 * @property-read Client $client
 * @property-read Collection|Payment[] $payments
 * @property-read ClientSite $site
 * @property-read Collection|SubTask[] $subTasks
 * @mixin Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $description
 * @property int $client_id
 * @property int $client_site_id
 * @property string $currency
 * @property string $launched_at
 * @property string $completed_at
 * @property State $state
 * @method static Builder|Task whereClientId($value)
 * @method static Builder|Task whereClientSiteId($value)
 * @method static Builder|Task whereCurrency($value)
 * @method static Builder|Task whereCurrentStatusId($value)
 * @method static Builder|Task whereDescription($value)
 * @method static Builder|Task whereId($value)
 * @method static Builder|Task whereTitle($value)
 * @method static Builder|Task whereUserId($value)
 * @property int|null $task_template_id
 * @method static Builder|Task whereCompletedAt($value)
 * @method static Builder|Task whereLaunchedAt($value)
 * @method static Builder|Task whereTaskTemplateId($value)
 * @method static Builder|Task withClient()
 * @method static Builder|Task withSubtasks()
 * @method static Builder|Task withPayments()
 * @property-read int|null $payments_count
 * @property-read int|null $sub_tasks_count
 * @method static Builder|Task newModelQuery()
 * @method static Builder|Task newQuery()
 * @method static Builder|Task query()
 */
class Task extends Model {
  protected $table = 'task';

  public $timestamps = false;
  protected $fillable = ['user_id', 'title', 'description', 'client_id', 'client_site_id', 'currency', 'launched_at', 'completed_at', 'task_template_id', 'state'];

  protected $casts = [
    'state' => State::class,
  ];

  /**
   *
   * @return BelongsTo
   */
  public function client(): BelongsTo {
    return $this->belongsTo(Client::class);
  }

  /**
   *
   * @return BelongsTo
   */
  public function site(): BelongsTo {
    return $this->belongsTo(ClientSite::class, 'client_site_id', 'id');
  }

  public function template(): BelongsTo {
    return $this->belongsTo(TaskTemplate::class);
  }

  /**
   *
   * @return HasMany
   */
  public function subTasks(): HasMany {
    return $this->hasMany(SubTask::class);
  }

  /**
   *
   * @return HasMany
   */
  public function payments(): HasMany {
    return $this->hasMany(Payment::class);
  }

  /**
   * Scope a query to task with her state code and client.
   *
   * @param Builder $query
   *
   * @return Builder
   */
  public function scopeWithClient(Builder $query): Builder {
    return $query->with('client');
  }

  public function scopeWithSubtasks(Builder $query): Builder {
    return $query->with('subTasks');
  }

  public function scopeWithPayments(Builder $query): Builder {
    return $query->with('payments');
  }

  public function getBudget(): Amount {
    $amount = 0.0;
    foreach ($this->subTasks as $subTask) {
      $amount += $subTask->budget;
    }

    return new Amount($amount, $this->currency);
  }

  public function getPayed(): Amount {
    $amount = 0.0;
    foreach ($this->payments as $payment) {
      $amount += $payment->amount;
    }

    return new Amount($amount, $this->currency);
  }

  public function getHours(): float {
    $hours = 0.0;
    foreach ($this->subTasks as $subTask) {
      $hours += (float)$subTask->hours;
    }

    return $hours;
  }
}
