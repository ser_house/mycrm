<?php

namespace App\Model\Task;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Model\Task\SubTask
 *
 * @property-read Task $task
 * @mixin Eloquent
 * @property int $id
 * @property string $title
 * @property int $task_id
 * @property float $budget
 * @property string|null $launched_at Реальная дата начала, для внутренних отчетов
 * @property string|null $completed_at Реальная дата завершения, для внутренних отчетов
 * @property int|null $hours Кол-во часов для актов-счетов
 * @method static Builder|SubTask whereBudget($value)
 * @method static Builder|SubTask whereCompletedAt($value)
 * @method static Builder|SubTask whereHours($value)
 * @method static Builder|SubTask whereId($value)
 * @method static Builder|SubTask whereLaunchedAt($value)
 * @method static Builder|SubTask whereTaskId($value)
 * @method static Builder|SubTask whereTitle($value)
 * @property string|null $note
 * @method static Builder|SubTask whereNote($value)
 * @method static Builder|SubTask newModelQuery()
 * @method static Builder|SubTask newQuery()
 * @method static Builder|SubTask query()
 */
class SubTask extends Model {
	protected $table = 'sub_task';

	public $timestamps = false;

	protected $fillable = ['title', 'note', 'task_id', 'budget', 'hours'];

	/**
	 *
	 * @return BelongsTo
	 */
	public function task(): BelongsTo {
		return $this->belongsTo(Task::class);
	}
}
