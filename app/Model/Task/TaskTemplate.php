<?php

namespace App\Model\Task;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Model\Client\ClientSite;
use App\Model\Client\Client;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Model\Task\TaskTemplate
 *
 * @property int $id
 * @property string $created_at
 * @property int $is_active
 * @property string $title
 * @property string|null $description
 * @property int $client_id
 * @property int|null $client_site_id
 * @property string $currency
 * @property float $budget
 * @property float|null $hours
 * @property-read Client $client
 * @property-read ClientSite|null $site
 * @method static Builder|TaskTemplate whereBudget($value)
 * @method static Builder|TaskTemplate whereClientId($value)
 * @method static Builder|TaskTemplate whereClientSiteId($value)
 * @method static Builder|TaskTemplate whereCreatedAt($value)
 * @method static Builder|TaskTemplate whereCurrency($value)
 * @method static Builder|TaskTemplate whereDescription($value)
 * @method static Builder|TaskTemplate whereHours($value)
 * @method static Builder|TaskTemplate whereId($value)
 * @method static Builder|TaskTemplate whereIsActive($value)
 * @method static Builder|TaskTemplate whereTitle($value)
 * @mixin Eloquent
 * @method static Builder|TaskTemplate newModelQuery()
 * @method static Builder|TaskTemplate newQuery()
 * @method static Builder|TaskTemplate query()
 */
class TaskTemplate extends Model {
	public const STATE_ACTIVE = 1;
	public const STATE_INACTIVE = 0;

	protected $table = 'task_template';

	public $timestamps = false;
	protected $fillable = ['created_at', 'is_active', 'title', 'description', 'client_id', 'client_site_id', 'currency', 'budget', 'hours'];

	public function client(): BelongsTo {
		return $this->belongsTo(Client::class);
	}

	public function site(): BelongsTo {
		return $this->belongsTo(ClientSite::class, 'client_site_id', 'id');
	}

}
