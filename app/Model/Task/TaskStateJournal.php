<?php

namespace App\Model\Task;

use Application\Task\State;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Model\Task\TaskStateJournal
 *
 * @property-read Task $task
 * @mixin Eloquent
 * @property int $id
 * @property string $inserted_at
 * @property int $task_id
 * @property State $state
 * @method static Builder|TaskStateJournal whereId($value)
 * @method static Builder|TaskStateJournal whereInsertedAt($value)
 * @method static Builder|TaskStateJournal whereStatusCode($value)
 * @method static Builder|TaskStateJournal whereTaskId($value)
 * @method static Builder|TaskStateJournal newModelQuery()
 * @method static Builder|TaskStateJournal newQuery()
 * @method static Builder|TaskStateJournal query()
 */
class TaskStateJournal extends Model {
  protected $table = 'task_state_journal';

  public $timestamps = false;

  protected $fillable = ['task_id', 'state_to'];

  protected $casts = [
    'state_to' => State::class,
  ];

  public function task(): BelongsTo {
    return $this->belongsTo(Task::class);
  }
}
