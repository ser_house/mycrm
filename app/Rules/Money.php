<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Money implements Rule {
	/**
	 * Create a new rule instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Determine if the validation rule passes.
	 *
	 * @param  string $attribute
	 * @param  mixed $value
	 *
	 * @return bool
	 */
	public function passes($attribute, $value): bool {
		$value = str_replace([' ', ','], ['', '.'], $value);

		return is_numeric($value) || is_float($value);
	}

	/**
	 * Get the validation error message.
	 *
	 * @return string
	 */
	public function message(): string {
		return 'The :attribute must be amount.';
	}
}
